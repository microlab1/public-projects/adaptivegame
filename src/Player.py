import numpy as np
import json


class RemotePlayerStrategy:
    def __init__(self, **kwargs):
        self.nextAction = "0"
        self.sendData = kwargs["sender"]
        self.getData = kwargs["getter"]
        self.name = kwargs["name"]

    def setObservations(self, ownObject, fieldDict):
        self.nextAction = "0"
        self.sendData(json.dumps({"type":"gameData", "payload":fieldDict}), ownObject.name)

    def getNextAction(self):
        #print("Get",self.name)
        newaction = self.getData(self.name)
        #print("Get FINISH", self.name)
        if newaction is None:
            return self.nextAction
        else:
            return newaction

    def reset(self):
        self.nextAction = "0"
        data = "something"
        #print("Reset", self.name)
        while data is not None:
            data = self.getData(self.name)
        #print("Reset FINISH",self.name)

class DummyStrategy:
    def __init__(self, **kwargs):
        self.nextAction = "0"

    def setObservations(self, ownObject, fieldDict):
        ownObject.active=False

    def getNextAction(self):
        return "0"

    def reset(self):
        pass

class RandBotStrategy:
    def __init__(self, **kwargs):
        self.nextAction = 0

    def setObservations(self, ownObject, fieldDict):
        pass

    def getNextAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action

    def reset(self):
        self.nextAction = "0"

class NaiveStrategy:
    def __init__(self, **kwargs):
        self.nextAction = "0"
        self.oldpos = None
        self.oldcounter = 0

    def getRandomAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action

    def setObservations(self, ownObject, fieldDict):
        if self.oldpos is not None:
            if tuple(self.oldpos) == tuple(ownObject.pos):
                self.oldcounter += 1

        self.oldpos = ownObject.pos.copy()

        values = np.array([field["value"] for field in fieldDict["vision"]])
        values[values > 3] = 0
        values[values < 0] = 0
        if np.max(values) == 0 or self.oldcounter >= 3:
            self.nextAction = self.getRandomAction()
            self.oldcounter = 0
        else:
            idx = np.argmax(values)
            actstring = ""
            for i in range(2):
                if fieldDict["vision"][idx]["relative_coord"][i] == 0:
                    actstring += "0"
                elif fieldDict["vision"][idx]["relative_coord"][i] > 0:
                    actstring += "+"
                elif fieldDict["vision"][idx]["relative_coord"][i] < 0:
                    actstring += "-"

            self.nextAction = actstring

    def getNextAction(self):
        return self.nextAction

    def reset(self):
        self.nextAction = "0"

class NaiveHunterStrategy:
    def __init__(self, **kwargs):
        self.nextAction = "0"
        self.oldpos = None
        self.oldcounter = 0

    def getRandomAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action

    def setObservations(self, ownObject, fieldDict):
        if self.oldpos is not None:
            if tuple(self.oldpos) == tuple(ownObject.pos):
                self.oldcounter += 1
            else:
                self.oldcounter = 0
        if ownObject.active:
            self.oldpos = ownObject.pos.copy()

        vals = []
        for field in fieldDict["vision"]:
            if field["player"] is not None:
                if tuple(field["relative_coord"]) == (0, 0):
                    if 0 < field["value"] <= 3:
                        vals.append(field["value"])
                    elif field["value"] == 9:
                        vals.append(-1)
                    else:
                        vals.append(0)
                elif field["player"]["size"] * 1.1 < ownObject.size:
                    vals.append(field["player"]["size"])
                else:
                    vals.append(-1)
            else:
                if 0 < field["value"] <= 3:
                    vals.append(field["value"])
                elif field["value"] == 9:
                    vals.append(-1)
                else:
                    vals.append(0)

        values = np.array(vals)
        # print(values, fieldDict["vision"][np.argmax(values)]["relative_coord"], values.max())
        if np.max(values) <= 0 or self.oldcounter >= 3:
            self.nextAction = self.getRandomAction()
            self.oldcounter = 0
        else:
            idx = np.argmax(values)
            actstring = ""
            for i in range(2):
                if fieldDict["vision"][idx]["relative_coord"][i] == 0:
                    actstring += "0"
                elif fieldDict["vision"][idx]["relative_coord"][i] > 0:
                    actstring += "+"
                elif fieldDict["vision"][idx]["relative_coord"][i] < 0:
                    actstring += "-"

            self.nextAction = actstring

    def getNextAction(self):
        return self.nextAction

    def reset(self):
        self.nextAction = "0"

class Player:
    strategies = {"randombot": RandBotStrategy, "naivebot": NaiveStrategy, "naivehunterbot": NaiveHunterStrategy,
                  "remoteplayer": RemotePlayerStrategy, "dummy":DummyStrategy}

    def __init__(self, name, playerType, startingSize, **kwargs):
        self.name = name
        self.playerType = playerType
        self.pos = np.zeros((2,))
        self.size = startingSize
        kwargs["name"] = name
        self.strategy = Player.strategies[playerType](**kwargs)
        self.active = True

    def die(self):
        self.active = False
        print(self.name + " died!")

    def reset(self):
        self.active = True

class RemotePlayerStrategy222:
    def __init__(self, **kwargs):
        self.nextAction = "0"
        self.sendData = kwargs["sender"]
        self.getData = kwargs["getter"]
        self.name = kwargs["name"]



    def setObservations(self, ownObject, fieldDict):
        BeX=np.zeros((1,82))

        BeX[0]=fieldDict["vision"][40]["player"]["size"]
        if fieldDict["vision"][45]["player"] is None:
            BeX[0][1] = fieldDict["vision"][45]["value"]
        else:
            BeX[0][1] = fieldDict["vision"][45]["player"]["size"]

        if fieldDict["vision"][16]["player"] is None:
            BeX[0][2] = fieldDict["vision"][16]["value"]
        else:
            BeX[0][2] = fieldDict["vision"][16]["player"]["size"]

        if fieldDict["vision"][25]["player"] is None:
            BeX[0][3] = fieldDict["vision"][25]["value"]
        else:
            BeX[0][3] = fieldDict["vision"][25]["player"]["size"]

        if fieldDict["vision"][34]["player"] is None:
            BeX[0][4] = fieldDict["vision"][34]["value"]
        else:
            BeX[0][4] = fieldDict["vision"][34]["player"]["size"]

        if fieldDict["vision"][44]["player"] is None:
            BeX[0][5] = fieldDict["vision"][44]["value"]
        else:
            BeX[0][5] = fieldDict["vision"][44]["player"]["size"]

        if fieldDict["vision"][54]["player"] is None:
            BeX[0][6] = fieldDict["vision"][54]["value"]
        else:
            BeX[0][6] = fieldDict["vision"][54]["player"]["size"]

        if fieldDict["vision"][63]["player"] is None:
            BeX[0][7] = fieldDict["vision"][63]["value"]
        else:
            BeX[0][7] = fieldDict["vision"][63]["player"]["size"]

        if fieldDict["vision"][72]["player"] is None:
            BeX[0][8] = fieldDict["vision"][72]["value"]
        else:
            BeX[0][8] = fieldDict["vision"][72]["player"]["size"]

        if fieldDict["vision"][7]["player"] is None:
            BeX[0][9] = fieldDict["vision"][7]["value"]
        else:
            BeX[0][9] = fieldDict["vision"][7]["player"]["size"]

        if fieldDict["vision"][15]["player"] is None:
            BeX[0][10] = fieldDict["vision"][15]["value"]
        else:
            BeX[0][10] = fieldDict["vision"][15]["player"]["size"]

        if fieldDict["vision"][24]["player"] is None:
            BeX[0][11] = fieldDict["vision"][24]["value"]
        else:
            BeX[0][11] = fieldDict["vision"][24]["player"]["size"]

        if fieldDict["vision"][33]["player"] is None:
            BeX[0][12] = fieldDict["vision"][33]["value"]
        else:
            BeX[0][12] = fieldDict["vision"][33]["player"]["size"]

        if fieldDict["vision"][43]["player"] is None:
            BeX[0][13] = fieldDict["vision"][43]["value"]
        else:
            BeX[0][13] = fieldDict["vision"][43]["player"]["size"]

        if fieldDict["vision"][53]["player"] is None:
            BeX[0][14] = fieldDict["vision"][53]["value"]
        else:
            BeX[0][14] = fieldDict["vision"][53]["player"]["size"]

        if fieldDict["vision"][62]["player"] is None:
            BeX[0][15] = fieldDict["vision"][62]["value"]
        else:
            BeX[0][15] = fieldDict["vision"][62]["player"]["size"]

        if fieldDict["vision"][71]["player"] is None:
            BeX[0][16] = fieldDict["vision"][71]["value"]
        else:
            BeX[0][16] = fieldDict["vision"][71]["player"]["size"]

        if fieldDict["vision"][79]["player"] is None:
            BeX[0][17] = fieldDict["vision"][79]["value"]
        else:
            BeX[0][17] = fieldDict["vision"][79]["player"]["size"]

        if fieldDict["vision"][6]["player"] is None:
            BeX[0][18] = fieldDict["vision"][6]["value"]
        else:
            BeX[0][18] = fieldDict["vision"][6]["player"]["size"]

        if fieldDict["vision"][14]["player"] is None:
            BeX[0][19] = fieldDict["vision"][14]["value"]
        else:
            BeX[0][19] = fieldDict["vision"][14]["player"]["size"]

        if fieldDict["vision"][23]["player"] is None:
            BeX[0][20] = fieldDict["vision"][23]["value"]
        else:
            BeX[0][20] = fieldDict["vision"][23]["player"]["size"]

        if fieldDict["vision"][32]["player"] is None:
            BeX[0][21] = fieldDict["vision"][32]["value"]
        else:
            BeX[0][21] = fieldDict["vision"][32]["player"]["size"]

        if fieldDict["vision"][42]["player"] is None:
            BeX[0][22] = fieldDict["vision"][42]["value"]
        else:
            BeX[0][22] = fieldDict["vision"][42]["player"]["size"]

        if fieldDict["vision"][52]["player"] is None:
            BeX[0][23] = fieldDict["vision"][52]["value"]
        else:
            BeX[0][23] = fieldDict["vision"][52]["player"]["size"]

        if fieldDict["vision"][61]["player"] is None:
            BeX[0][24] = fieldDict["vision"][61]["value"]
        else:
            BeX[0][24] = fieldDict["vision"][61]["player"]["size"]

        if fieldDict["vision"][70]["player"] is None:
            BeX[0][25] = fieldDict["vision"][70]["value"]
        else:
            BeX[0][25] = fieldDict["vision"][70]["player"]["size"]

        if fieldDict["vision"][78]["player"] is None:
            BeX[0][26] = fieldDict["vision"][78]["value"]
        else:
            BeX[0][26] = fieldDict["vision"][78]["player"]["size"]

        if fieldDict["vision"][5]["player"] is None:
            BeX[0][27] = fieldDict["vision"][5]["value"]
        else:
            BeX[0][27] = fieldDict["vision"][5]["player"]["size"]

        if fieldDict["vision"][13]["player"] is None:
            BeX[0][28] = fieldDict["vision"][13]["value"]
        else:
            BeX[0][28] = fieldDict["vision"][13]["player"]["size"]

        if fieldDict["vision"][22]["player"] is None:
            BeX[0][29] = fieldDict["vision"][22]["value"]
        else:
            BeX[0][29] = fieldDict["vision"][22]["player"]["size"]

        if fieldDict["vision"][31]["player"] is None:
            BeX[0][30] = fieldDict["vision"][31]["value"]
        else:
            BeX[0][30] = fieldDict["vision"][31]["player"]["size"]

        if fieldDict["vision"][41]["player"] is None:
            BeX[0][31] = fieldDict["vision"][41]["value"]
        else:
            BeX[0][31] = fieldDict["vision"][41]["player"]["size"]

        if fieldDict["vision"][51]["player"] is None:
            BeX[0][32] = fieldDict["vision"][51]["value"]
        else:
            BeX[0][32] = fieldDict["vision"][51]["player"]["size"]

        if fieldDict["vision"][60]["player"] is None:
            BeX[0][33] = fieldDict["vision"][60]["value"]
        else:
            BeX[0][33] = fieldDict["vision"][60]["player"]["size"]

        if fieldDict["vision"][69]["player"] is None:
            BeX[0][34] = fieldDict["vision"][69]["value"]
        else:
            BeX[0][34] = fieldDict["vision"][69]["player"]["size"]

        if fieldDict["vision"][77]["player"] is None:
            BeX[0][35] = fieldDict["vision"][77]["value"]
        else:
            BeX[0][35] = fieldDict["vision"][77]["player"]["size"]

        if fieldDict["vision"][0]["player"] is None:
            BeX[0][36] = fieldDict["vision"][0]["value"]
        else:
            BeX[0][36] = fieldDict["vision"][0]["player"]["size"]

        if fieldDict["vision"][4]["player"] is None:
            BeX[0][37] = fieldDict["vision"][4]["value"]
        else:
            BeX[0][37] = fieldDict["vision"][4]["player"]["size"]

        if fieldDict["vision"][12]["player"] is None:
            BeX[0][38] = fieldDict["vision"][12]["value"]
        else:
            BeX[0][38] = fieldDict["vision"][12]["player"]["size"]

        if fieldDict["vision"][21]["player"] is None:
            BeX[0][39] = fieldDict["vision"][21]["value"]
        else:
            BeX[0][39] = fieldDict["vision"][21]["player"]["size"]

        if fieldDict["vision"][30]["player"] is None:
            BeX[0][40] = fieldDict["vision"][30]["value"]
        else:
            BeX[0][40] = fieldDict["vision"][30]["player"]["size"]

        BeX[0][41] = 0


        if fieldDict["vision"][50]["player"] is None:
            BeX[0][42] = fieldDict["vision"][50]["value"]
        else:
            BeX[0][42] = fieldDict["vision"][50]["player"]["size"]

        if fieldDict["vision"][59]["player"] is None:
            BeX[0][43] = fieldDict["vision"][59]["value"]
        else:
            BeX[0][43] = fieldDict["vision"][59]["player"]["size"]

        if fieldDict["vision"][68]["player"] is None:
            BeX[0][44] = fieldDict["vision"][68]["value"]
        else:
            BeX[0][44] = fieldDict["vision"][68]["player"]["size"]

        if fieldDict["vision"][76]["player"] is None:
            BeX[0][45] = fieldDict["vision"][76]["value"]
        else:
            BeX[0][45] = fieldDict["vision"][76]["player"]["size"]

        if fieldDict["vision"][80]["player"] is None:
            BeX[0][46] = fieldDict["vision"][80]["value"]
        else:
            BeX[0][46] = fieldDict["vision"][80]["player"]["size"]

        if fieldDict["vision"][3]["player"] is None:
            BeX[0][47] = fieldDict["vision"][3]["value"]
        else:
            BeX[0][47] = fieldDict["vision"][3]["player"]["size"]

        if fieldDict["vision"][11]["player"] is None:
            BeX[0][48] = fieldDict["vision"][11]["value"]
        else:
            BeX[0][48] = fieldDict["vision"][11]["player"]["size"]

        if fieldDict["vision"][20]["player"] is None:
            BeX[0][49] = fieldDict["vision"][20]["value"]
        else:
            BeX[0][49] = fieldDict["vision"][20]["player"]["size"]

        if fieldDict["vision"][29]["player"] is None:
            BeX[0][50] = fieldDict["vision"][29]["value"]
        else:
            BeX[0][50] = fieldDict["vision"][29]["player"]["size"]

        if fieldDict["vision"][39]["player"] is None:
            BeX[0][51] = fieldDict["vision"][39]["value"]
        else:
            BeX[0][51] = fieldDict["vision"][39]["player"]["size"]

        if fieldDict["vision"][49]["player"] is None:
            BeX[0][52] = fieldDict["vision"][49]["value"]
        else:
            BeX[0][52] = fieldDict["vision"][49]["player"]["size"]

        if fieldDict["vision"][58]["player"] is None:
            BeX[0][53] = fieldDict["vision"][58]["value"]
        else:
            BeX[0][53] = fieldDict["vision"][58]["player"]["size"]

        if fieldDict["vision"][67]["player"] is None:
            BeX[0][54] = fieldDict["vision"][67]["value"]
        else:
            BeX[0][54] = fieldDict["vision"][67]["player"]["size"]

        if fieldDict["vision"][75]["player"] is None:
            BeX[0][55] = fieldDict["vision"][75]["value"]
        else:
            BeX[0][55] = fieldDict["vision"][75]["player"]["size"]

        if fieldDict["vision"][2]["player"] is None:
            BeX[0][56] = fieldDict["vision"][2]["value"]
        else:
            BeX[0][56] = fieldDict["vision"][2]["player"]["size"]

        if fieldDict["vision"][10]["player"] is None:
            BeX[0][57] = fieldDict["vision"][10]["value"]
        else:
            BeX[0][57] = fieldDict["vision"][10]["player"]["size"]

        if fieldDict["vision"][19]["player"] is None:
            BeX[0][58] = fieldDict["vision"][19]["value"]
        else:
            BeX[0][58] = fieldDict["vision"][19]["player"]["size"]

        if fieldDict["vision"][28]["player"] is None:
            BeX[0][59] = fieldDict["vision"][28]["value"]
        else:
            BeX[0][59] = fieldDict["vision"][28]["player"]["size"]

        if fieldDict["vision"][38]["player"] is None:
            BeX[0][60] = fieldDict["vision"][38]["value"]
        else:
            BeX[0][60] = fieldDict["vision"][38]["player"]["size"]

        if fieldDict["vision"][48]["player"] is None:
            BeX[0][61] = fieldDict["vision"][48]["value"]
        else:
            BeX[0][61] = fieldDict["vision"][48]["player"]["size"]

        if fieldDict["vision"][57]["player"] is None:
            BeX[0][62] = fieldDict["vision"][57]["value"]
        else:
            BeX[0][62] = fieldDict["vision"][57]["player"]["size"]

        if fieldDict["vision"][66]["player"] is None:
            BeX[0][63] = fieldDict["vision"][66]["value"]
        else:
            BeX[0][63] = fieldDict["vision"][66]["player"]["size"]

        if fieldDict["vision"][74]["player"] is None:
            BeX[0][64] = fieldDict["vision"][74]["value"]
        else:
            BeX[0][64] = fieldDict["vision"][74]["player"]["size"]

        if fieldDict["vision"][1]["player"] is None:
            BeX[0][65] = fieldDict["vision"][1]["value"]
        else:
            BeX[0][65] = fieldDict["vision"][1]["player"]["size"]

        if fieldDict["vision"][9]["player"] is None:
            BeX[0][66] = fieldDict["vision"][9]["value"]
        else:
            BeX[0][66] = fieldDict["vision"][9]["player"]["size"]

        if fieldDict["vision"][18]["player"] is None:
            BeX[0][67] = fieldDict["vision"][18]["value"]
        else:
            BeX[0][67] = fieldDict["vision"][18]["player"]["size"]

        if fieldDict["vision"][27]["player"] is None:
            BeX[0][68] = fieldDict["vision"][27]["value"]
        else:
            BeX[0][68] = fieldDict["vision"][27]["player"]["size"]

        if fieldDict["vision"][37]["player"] is None:
            BeX[0][69] = fieldDict["vision"][37]["value"]
        else:
            BeX[0][69] = fieldDict["vision"][37]["player"]["size"]

        if fieldDict["vision"][47]["player"] is None:
            BeX[0][70] = fieldDict["vision"][47]["value"]
        else:
            BeX[0][70] = fieldDict["vision"][47]["player"]["size"]

        if fieldDict["vision"][56]["player"] is None:
            BeX[0][71] = fieldDict["vision"][56]["value"]
        else:
            BeX[0][71] = fieldDict["vision"][56]["player"]["size"]

        if fieldDict["vision"][65]["player"] is None:
            BeX[0][72] = fieldDict["vision"][65]["value"]
        else:
            BeX[0][72] = fieldDict["vision"][65]["player"]["size"]

        if fieldDict["vision"][73]["player"] is None:
            BeX[0][73] = fieldDict["vision"][73]["value"]
        else:
            BeX[0][73] = fieldDict["vision"][73]["player"]["size"]

        if fieldDict["vision"][8]["player"] is None:
            BeX[0][74] = fieldDict["vision"][8]["value"]
        else:
            BeX[0][74] = fieldDict["vision"][8]["player"]["size"]

        if fieldDict["vision"][17]["player"] is None:
            BeX[0][75] = fieldDict["vision"][17]["value"]
        else:
            BeX[0][75] = fieldDict["vision"][17]["player"]["size"]

        if fieldDict["vision"][26]["player"] is None:
            BeX[0][76] = fieldDict["vision"][26]["value"]
        else:
            BeX[0][76] = fieldDict["vision"][26]["player"]["size"]

        if fieldDict["vision"][36]["player"] is None:
            BeX[0][77] = fieldDict["vision"][36]["value"]
        else:
            BeX[0][77] = fieldDict["vision"][36]["player"]["size"]

        if fieldDict["vision"][46]["player"] is None:
            BeX[0][78] = fieldDict["vision"][46]["value"]
        else:
            BeX[0][78] = fieldDict["vision"][46]["player"]["size"]

        if fieldDict["vision"][55]["player"] is None:
            BeX[0][79] = fieldDict["vision"][55]["value"]
        else:
            BeX[0][79] = fieldDict["vision"][55]["player"]["size"]

        if fieldDict["vision"][64]["player"] is None:
            BeX[0][80] = fieldDict["vision"][64]["value"]
        else:
            BeX[0][80] = fieldDict["vision"][64]["player"]["size"]

        if fieldDict["vision"][35]["player"] is None:
            BeX[0][81] = fieldDict["vision"][35]["value"]
        else:
            BeX[0][81] = fieldDict["vision"][35]["player"]["size"]


        prediction=model.predict([BeX])
        if prediction[0][0] <= -0.5:
            Xaction="-"
        elif prediction[0][0] >= 0.5:
            Xaction="+"
        else:
            Xaction = "0"

        if prediction[0][1] <= -0.5:
            Yaction="-"
        elif prediction[0][1] >= 0.5:
            Yaction="+"
        else:
            Yaction = "0"

        self.nextAction = Xaction+Yaction #model kimenet
        self.sendData(json.dumps({"type":"gameData", "payload":fieldDict}), ownObject.name)

  #  def getNextAction(self):

   #     newaction = self.getData(self.name)
    #    if newaction is None:
   #         return self.nextAction
   #     else:
   #         return newaction

  #  def reset(self):
  #      self.nextAction = "0"
  #      data = "something"
   #     while data is not None:
   #         data = self.getData(self.name)

    def getNextAction(self):
        return self.nextAction

    def reset(self):
        self.nextAction = "0"