#encoding: utf-8

#==============================================================
# Adds the src map to the path, so imports can work properly
#==============================================================
import pathlib
import sys

current_dir = pathlib.Path().resolve()
src_dir     = current_dir.parent.parent.joinpath("src")
path = str(src_dir).replace("/", "\\")
sys.path.append(path)
#==============================================================

import time
from Client import SocketClient
import json
import numpy as np
import tensorflow as tf

model=tf.keras.models.load_model("neurhalo2.model")

# NaiveHunter stratégia implementációja távoli eléréshez.
class RemoteNaiveHunterStrategy:

    def __init__(self):
        # Dinamikus viselkedéshez szükséges változók definíciója
        self.oldpos = None
        self.oldcounter = 0

    # Egyéb függvények...
    def getRandomAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action

    # Az egyetlen kötelező elem: A játékmestertől jövő információt feldolgozó és választ elküldő függvény
    def processObservation(self, fulljson, sendData):


        """
        :param fulljson: A játékmestertől érkező JSON dict-be konvertálva.
        Két kötelező kulccsal: 'type' (leaderBoard, readyToStart, started, gameData, serverClose) és 'payload' (az üzenet adatrésze).
        'leaderBoard' type a játék végét jelzi, a payload tartalma {'ticks': a játék hossza tickekben, 'players':[{'name': jáétékosnév, 'active': él-e a játékos?, 'maxSize': a legnagyobb elért méret a játék során},...]}
        'readyToStart' type esetén a szerver az indító üzenetre vár esetén, a payload üres (None)
        'started' type esetén a játék elindul, tickLength-enként kiküldés és akciófogadás várható payload {'tickLength': egy tick hossza }
        'gameData' type esetén az üzenet a játékos által elérhető információkat küldi, a payload:
                                    {"pos": abszolút pozíció a térképen, "tick": az aktuális tick sorszáma, "active": a saját életünk állapota,
                                    "size": saját méret,
                                    "leaderBoard": {'ticks': a játék hossza tickekben eddig, 'players':[{'name': jáétékosnév, 'active': él-e a játékos?, 'maxSize': a legnagyobb elért méret a játék során eddig},...]},
                                    "vision": [{"relative_coord": az adott megfigyelt mező relatív koordinátája,
                                                                    "value": az adott megfigyelt mező értéke (0-3,9),
                                                                    "player": None, ha nincs aktív játékos, vagy
                                                                            {name: a mezőn álló játékos neve, size: a mezőn álló játékos mérete}},...] }
        'serverClose' type esetén a játékmester szabályos, vagy hiba okozta bezáródásáról értesülünk, a payload üres (None)
        :param sendData: A kliens adatküldő függvénye, JSON formátumú str bemenetet vár, melyet a játékmester felé továbbít.
        Az elküldött adat struktúrája {"command": Parancs típusa, "name": A küldő azonosítója, "payload": az üzenet adatrésze}
        Elérhető parancsok:
        'SetName' A kliens felregisztrálja a saját nevét a szervernek, enélkül a nevünkhöz tartozó üzenetek nem térnek vissza.
                 Tiltott nevek: a configban megadott játékmester név és az 'all'.
        'SetAction' Ebben az esetben a payload az akció string, amely két karaktert tartalmaz az X és az Y koordináták (matematikai mátrix indexelés) menti elmozdulásra.
                a karakterek értékei '0': helybenmaradás az adott tengely mentén, '+' pozitív irányú lépés, '-' negatív irányú lépés lehet. Amennyiben egy tick ideje alatt
                nem külünk értéket az alapértelmezett '00' kerül végrehajtásra.
        'GameControl' üzeneteket csak a Config.py-ban megadott játékmester névvel lehet küldeni, ezek a játékmenetet befolyásoló üzenetek.
                A payload az üzenet típusát (type), valamint az ahhoz tartozó 'data' adatokat kell, hogy tartalmazza.
                    'start' type elindítja a játékot egy "readyToStart" üzenetet küldött játék esetén, 'data' mezője üres (None)
                    'reset' type egy játék után várakozó 'leaderBoard'-ot küldött játékot állít alaphelyzetbe. A 'data' mező
                            {'mapPath':None, vagy elérési útvonal, 'updateMapPath': None, vagy elérési útvonal} formátumú, ahol None
                            esetén az előző pálya és növekedési map kerül megtartásra, míg elérési útvonal megadása esetén új pálya kerül betöltésre annak megfelelően.
                    'interrupt' type esetén a 'data' mező üres (None), ez megszakítja a szerver futását és szabályosan leállítja azt.
        :return:
        """

        # Játék rendezéssel kapcsolatos üzenetek lekezelése
        if fulljson["type"] == "leaderBoard":
            print("Game finished after",fulljson["payload"]["ticks"],"ticks!")
            print("Leaderboard:")
            for score in fulljson["payload"]["players"]:
                print(score["name"],score["active"], score["maxSize"])

            time.sleep(50)
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))

        if fulljson["type"] == "readyToStart":
            print("Game is ready, starting in 5")
            time.sleep(5)
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "start", "data": None}}))

        if fulljson["type"] == "started":
            print("Startup message from server.")
            print("Ticks interval is:",fulljson["payload"]["tickLength"])


        # Akció előállítása bemenetek alapján (egyezik a NaiveHunterBot-okéval)
        elif fulljson["type"] == "gameData":
            jsonData = fulljson["payload"]
            #if "pos" in jsonData.keys() and "tick" in jsonData.keys() and "active" in jsonData.keys() and "size" in jsonData.keys() and "vision" in jsonData.keys():
                #if self.oldpos is not None:
                   # if tuple(self.oldpos) == tuple(jsonData["pos"]):
                   #     self.oldcounter += 1
                   # else:
                   #     self.oldcounter = 0
                #if jsonData["active"]:
                #    self.oldpos = jsonData["pos"].copy()

            BeX = np.zeros((1, 82))
            BeX[0] = jsonData["vision"][40]["player"]["size"]
            if jsonData["vision"][45]["player"] is None:
                BeX[0][1] = jsonData["vision"][45]["value"]
            else:
                BeX[0][1] = jsonData["vision"][45]["player"]["size"]

            if jsonData["vision"][16]["player"] is None:
                BeX[0][2] = jsonData["vision"][16]["value"]
            else:
                BeX[0][2] = jsonData["vision"][16]["player"]["size"]

            if jsonData["vision"][25]["player"] is None:
                BeX[0][3] = jsonData["vision"][25]["value"]
            else:
                BeX[0][3] = jsonData["vision"][25]["player"]["size"]

            if jsonData["vision"][34]["player"] is None:
                BeX[0][4] = jsonData["vision"][34]["value"]
            else:
                BeX[0][4] = jsonData["vision"][34]["player"]["size"]

            if jsonData["vision"][44]["player"] is None:
                BeX[0][5] = jsonData["vision"][44]["value"]
            else:
                BeX[0][5] = jsonData["vision"][44]["player"]["size"]

            if jsonData["vision"][54]["player"] is None:
                BeX[0][6] = jsonData["vision"][54]["value"]
            else:
                BeX[0][6] = jsonData["vision"][54]["player"]["size"]

            if jsonData["vision"][63]["player"] is None:
                BeX[0][7] = jsonData["vision"][63]["value"]
            else:
                BeX[0][7] = jsonData["vision"][63]["player"]["size"]

            if jsonData["vision"][72]["player"] is None:
                BeX[0][8] = jsonData["vision"][72]["value"]
            else:
                BeX[0][8] = jsonData["vision"][72]["player"]["size"]

            if jsonData["vision"][7]["player"] is None:
                BeX[0][9] = jsonData["vision"][7]["value"]
            else:
                BeX[0][9] = jsonData["vision"][7]["player"]["size"]

            if jsonData["vision"][15]["player"] is None:
                BeX[0][10] = jsonData["vision"][15]["value"]
            else:
                BeX[0][10] = jsonData["vision"][15]["player"]["size"]

            if jsonData["vision"][24]["player"] is None:
                BeX[0][11] = jsonData["vision"][24]["value"]
            else:
                BeX[0][11] = jsonData["vision"][24]["player"]["size"]

            if jsonData["vision"][33]["player"] is None:
                BeX[0][12] = jsonData["vision"][33]["value"]
            else:
                BeX[0][12] = jsonData["vision"][33]["player"]["size"]

            if jsonData["vision"][43]["player"] is None:
                BeX[0][13] = jsonData["vision"][43]["value"]
            else:
                BeX[0][13] = jsonData["vision"][43]["player"]["size"]

            if jsonData["vision"][53]["player"] is None:
                BeX[0][14] = jsonData["vision"][53]["value"]
            else:
                BeX[0][14] = jsonData["vision"][53]["player"]["size"]

            if jsonData["vision"][62]["player"] is None:
                BeX[0][15] = jsonData["vision"][62]["value"]
            else:
                BeX[0][15] = jsonData["vision"][62]["player"]["size"]

            if jsonData["vision"][71]["player"] is None:
                BeX[0][16] = jsonData["vision"][71]["value"]
            else:
                BeX[0][16] = jsonData["vision"][71]["player"]["size"]

            if jsonData["vision"][79]["player"] is None:
                BeX[0][17] = jsonData["vision"][79]["value"]
            else:
                BeX[0][17] = jsonData["vision"][79]["player"]["size"]

            if jsonData["vision"][6]["player"] is None:
                BeX[0][18] = jsonData["vision"][6]["value"]
            else:
                BeX[0][18] = jsonData["vision"][6]["player"]["size"]

            if jsonData["vision"][14]["player"] is None:
                BeX[0][19] = jsonData["vision"][14]["value"]
            else:
                BeX[0][19] = jsonData["vision"][14]["player"]["size"]

            if jsonData["vision"][23]["player"] is None:
                BeX[0][20] = jsonData["vision"][23]["value"]
            else:
                BeX[0][20] = jsonData["vision"][23]["player"]["size"]

            if jsonData["vision"][32]["player"] is None:
                BeX[0][21] = jsonData["vision"][32]["value"]
            else:
                BeX[0][21] = jsonData["vision"][32]["player"]["size"]

            if jsonData["vision"][42]["player"] is None:
                BeX[0][22] = jsonData["vision"][42]["value"]
            else:
                BeX[0][22] = jsonData["vision"][42]["player"]["size"]

            if jsonData["vision"][52]["player"] is None:
                BeX[0][23] = jsonData["vision"][52]["value"]
            else:
                BeX[0][23] = jsonData["vision"][52]["player"]["size"]

            if jsonData["vision"][61]["player"] is None:
                BeX[0][24] = jsonData["vision"][61]["value"]
            else:
                BeX[0][24] = jsonData["vision"][61]["player"]["size"]

            if jsonData["vision"][70]["player"] is None:
                BeX[0][25] = jsonData["vision"][70]["value"]
            else:
                BeX[0][25] = jsonData["vision"][70]["player"]["size"]

            if jsonData["vision"][78]["player"] is None:
                BeX[0][26] = jsonData["vision"][78]["value"]
            else:
                BeX[0][26] = jsonData["vision"][78]["player"]["size"]

            if jsonData["vision"][5]["player"] is None:
                BeX[0][27] = jsonData["vision"][5]["value"]
            else:
                BeX[0][27] = jsonData["vision"][5]["player"]["size"]

            if jsonData["vision"][13]["player"] is None:
                BeX[0][28] = jsonData["vision"][13]["value"]
            else:
                BeX[0][28] = jsonData["vision"][13]["player"]["size"]

            if jsonData["vision"][22]["player"] is None:
                BeX[0][29] = jsonData["vision"][22]["value"]
            else:
                BeX[0][29] = jsonData["vision"][22]["player"]["size"]

            if jsonData["vision"][31]["player"] is None:
                BeX[0][30] = jsonData["vision"][31]["value"]
            else:
                BeX[0][30] = jsonData["vision"][31]["player"]["size"]

            if jsonData["vision"][41]["player"] is None:
                BeX[0][31] = jsonData["vision"][41]["value"]
            else:
                BeX[0][31] = jsonData["vision"][41]["player"]["size"]

            if jsonData["vision"][51]["player"] is None:
                BeX[0][32] = jsonData["vision"][51]["value"]
            else:
                BeX[0][32] = jsonData["vision"][51]["player"]["size"]

            if jsonData["vision"][60]["player"] is None:
                BeX[0][33] = jsonData["vision"][60]["value"]
            else:
                BeX[0][33] = jsonData["vision"][60]["player"]["size"]

            if jsonData["vision"][69]["player"] is None:
                BeX[0][34] = jsonData["vision"][69]["value"]
            else:
                BeX[0][34] = jsonData["vision"][69]["player"]["size"]

            if jsonData["vision"][77]["player"] is None:
                BeX[0][35] = jsonData["vision"][77]["value"]
            else:
                BeX[0][35] = jsonData["vision"][77]["player"]["size"]

            if jsonData["vision"][0]["player"] is None:
                BeX[0][36] = jsonData["vision"][0]["value"]
            else:
                BeX[0][36] = jsonData["vision"][0]["player"]["size"]

            if jsonData["vision"][4]["player"] is None:
                BeX[0][37] = jsonData["vision"][4]["value"]
            else:
                BeX[0][37] = jsonData["vision"][4]["player"]["size"]

            if jsonData["vision"][12]["player"] is None:
                BeX[0][38] = jsonData["vision"][12]["value"]
            else:
                BeX[0][38] = jsonData["vision"][12]["player"]["size"]

            if jsonData["vision"][21]["player"] is None:
                BeX[0][39] = jsonData["vision"][21]["value"]
            else:
                BeX[0][39] = jsonData["vision"][21]["player"]["size"]

            if jsonData["vision"][30]["player"] is None:
                BeX[0][40] = jsonData["vision"][30]["value"]
            else:
                BeX[0][40] = jsonData["vision"][30]["player"]["size"]

            BeX[0][41] = 0

            if jsonData["vision"][50]["player"] is None:
                BeX[0][42] = jsonData["vision"][50]["value"]
            else:
                BeX[0][42] = jsonData["vision"][50]["player"]["size"]

            if jsonData["vision"][59]["player"] is None:
                BeX[0][43] = jsonData["vision"][59]["value"]
            else:
                BeX[0][43] = jsonData["vision"][59]["player"]["size"]

            if jsonData["vision"][68]["player"] is None:
                BeX[0][44] = jsonData["vision"][68]["value"]
            else:
                BeX[0][44] = jsonData["vision"][68]["player"]["size"]

            if jsonData["vision"][76]["player"] is None:
                BeX[0][45] = jsonData["vision"][76]["value"]
            else:
                BeX[0][45] = jsonData["vision"][76]["player"]["size"]

            if jsonData["vision"][80]["player"] is None:
                BeX[0][46] = jsonData["vision"][80]["value"]
            else:
                BeX[0][46] = jsonData["vision"][80]["player"]["size"]

            if jsonData["vision"][3]["player"] is None:
                BeX[0][47] = jsonData["vision"][3]["value"]
            else:
                BeX[0][47] = jsonData["vision"][3]["player"]["size"]

            if jsonData["vision"][11]["player"] is None:
                BeX[0][48] = jsonData["vision"][11]["value"]
            else:
                BeX[0][48] = jsonData["vision"][11]["player"]["size"]

            if jsonData["vision"][20]["player"] is None:
                BeX[0][49] = jsonData["vision"][20]["value"]
            else:
                BeX[0][49] = jsonData["vision"][20]["player"]["size"]

            if jsonData["vision"][29]["player"] is None:
                BeX[0][50] = jsonData["vision"][29]["value"]
            else:
                BeX[0][50] = jsonData["vision"][29]["player"]["size"]

            if jsonData["vision"][39]["player"] is None:
                BeX[0][51] = jsonData["vision"][39]["value"]
            else:
                BeX[0][51] = jsonData["vision"][39]["player"]["size"]

            if jsonData["vision"][49]["player"] is None:
                BeX[0][52] = jsonData["vision"][49]["value"]
            else:
                BeX[0][52] = jsonData["vision"][49]["player"]["size"]

            if jsonData["vision"][58]["player"] is None:
                BeX[0][53] = jsonData["vision"][58]["value"]
            else:
                BeX[0][53] = jsonData["vision"][58]["player"]["size"]

            if jsonData["vision"][67]["player"] is None:
                BeX[0][54] = jsonData["vision"][67]["value"]
            else:
                BeX[0][54] = jsonData["vision"][67]["player"]["size"]

            if jsonData["vision"][75]["player"] is None:
                BeX[0][55] = jsonData["vision"][75]["value"]
            else:
                BeX[0][55] = jsonData["vision"][75]["player"]["size"]

            if jsonData["vision"][2]["player"] is None:
                BeX[0][56] = jsonData["vision"][2]["value"]
            else:
                BeX[0][56] = jsonData["vision"][2]["player"]["size"]

            if jsonData["vision"][10]["player"] is None:
                BeX[0][57] = jsonData["vision"][10]["value"]
            else:
                BeX[0][57] = jsonData["vision"][10]["player"]["size"]

            if jsonData["vision"][19]["player"] is None:
                BeX[0][58] = jsonData["vision"][19]["value"]
            else:
                BeX[0][58] = jsonData["vision"][19]["player"]["size"]

            if jsonData["vision"][28]["player"] is None:
                BeX[0][59] = jsonData["vision"][28]["value"]
            else:
                BeX[0][59] = jsonData["vision"][28]["player"]["size"]

            if jsonData["vision"][38]["player"] is None:
                BeX[0][60] = jsonData["vision"][38]["value"]
            else:
                BeX[0][60] = jsonData["vision"][38]["player"]["size"]

            if jsonData["vision"][48]["player"] is None:
                BeX[0][61] = jsonData["vision"][48]["value"]
            else:
                BeX[0][61] = jsonData["vision"][48]["player"]["size"]

            if jsonData["vision"][57]["player"] is None:
                BeX[0][62] = jsonData["vision"][57]["value"]
            else:
                BeX[0][62] = jsonData["vision"][57]["player"]["size"]

            if jsonData["vision"][66]["player"] is None:
                BeX[0][63] = jsonData["vision"][66]["value"]
            else:
                BeX[0][63] = jsonData["vision"][66]["player"]["size"]

            if jsonData["vision"][74]["player"] is None:
                BeX[0][64] = jsonData["vision"][74]["value"]
            else:
                BeX[0][64] = jsonData["vision"][74]["player"]["size"]

            if jsonData["vision"][1]["player"] is None:
                BeX[0][65] = jsonData["vision"][1]["value"]
            else:
                BeX[0][65] = jsonData["vision"][1]["player"]["size"]

            if jsonData["vision"][9]["player"] is None:
                BeX[0][66] = jsonData["vision"][9]["value"]
            else:
                BeX[0][66] = jsonData["vision"][9]["player"]["size"]

            if jsonData["vision"][18]["player"] is None:
                BeX[0][67] = jsonData["vision"][18]["value"]
            else:
                BeX[0][67] = jsonData["vision"][18]["player"]["size"]

            if jsonData["vision"][27]["player"] is None:
                BeX[0][68] = jsonData["vision"][27]["value"]
            else:
                BeX[0][68] = jsonData["vision"][27]["player"]["size"]

            if jsonData["vision"][37]["player"] is None:
                BeX[0][69] = jsonData["vision"][37]["value"]
            else:
                BeX[0][69] = jsonData["vision"][37]["player"]["size"]

            if jsonData["vision"][47]["player"] is None:
                BeX[0][70] = jsonData["vision"][47]["value"]
            else:
                BeX[0][70] = jsonData["vision"][47]["player"]["size"]

            if jsonData["vision"][56]["player"] is None:
                BeX[0][71] = jsonData["vision"][56]["value"]
            else:
                BeX[0][71] = jsonData["vision"][56]["player"]["size"]

            if jsonData["vision"][65]["player"] is None:
                BeX[0][72] = jsonData["vision"][65]["value"]
            else:
                BeX[0][72] = jsonData["vision"][65]["player"]["size"]

            if jsonData["vision"][73]["player"] is None:
                BeX[0][73] = jsonData["vision"][73]["value"]
            else:
                BeX[0][73] = jsonData["vision"][73]["player"]["size"]

            if jsonData["vision"][8]["player"] is None:
                BeX[0][74] = jsonData["vision"][8]["value"]
            else:
                BeX[0][74] = jsonData["vision"][8]["player"]["size"]

            if jsonData["vision"][17]["player"] is None:
                BeX[0][75] = jsonData["vision"][17]["value"]
            else:
                BeX[0][75] = jsonData["vision"][17]["player"]["size"]

            if jsonData["vision"][26]["player"] is None:
                BeX[0][76] = jsonData["vision"][26]["value"]
            else:
                BeX[0][76] = jsonData["vision"][26]["player"]["size"]

            if jsonData["vision"][36]["player"] is None:
                BeX[0][77] = jsonData["vision"][36]["value"]
            else:
                BeX[0][77] = jsonData["vision"][36]["player"]["size"]

            if jsonData["vision"][46]["player"] is None:
                BeX[0][78] = jsonData["vision"][46]["value"]
            else:
                BeX[0][78] = jsonData["vision"][46]["player"]["size"]

            if jsonData["vision"][55]["player"] is None:
                BeX[0][79] = jsonData["vision"][55]["value"]
            else:
                BeX[0][79] = jsonData["vision"][55]["player"]["size"]

            if jsonData["vision"][64]["player"] is None:
                BeX[0][80] = jsonData["vision"][64]["value"]
            else:
                BeX[0][80] = jsonData["vision"][64]["player"]["size"]

            if jsonData["vision"][35]["player"] is None:
                BeX[0][81] = jsonData["vision"][35]["value"]
            else:
                BeX[0][81] = jsonData["vision"][35]["player"]["size"]

            prediction = model.predict([BeX])
            if prediction[0][0] <= -0.5:
                Xaction = "-"
            elif prediction[0][0] >= 0.5:
                Xaction = "+"
            else:
                Xaction = "0"

            if prediction[0][1] <= -0.5:
                Yaction = "-"
            elif prediction[0][1] >= 0.5:
                Yaction = "+"
            else:
                Yaction = "0"

            #self.nextAction = Xaction + Yaction  # model kimenet
            actstring=Xaction + Yaction
            # Akció JSON előállítása és elküldése
            sendData(json.dumps({"command": "SetAction", "name": "Pink", "payload": actstring}))



if __name__=="__main__":
    # Példányosított stratégia objektum
    hunter = RemoteNaiveHunterStrategy()

    # Socket kliens, melynek a szerver címét kell megadni (IP, port), illetve a callback függvényt, melynek szignatúrája a fenti
    # callback(fulljson, sendData)
    client = SocketClient("localhost", 42069, hunter.processObservation)

    # Kliens indítása
    client.start()
    # Kis szünet, hogy a kapcsolat felépülhessen, a start nem blockol, a kliens külső szálon fut
    time.sleep(0.1)
    # Regisztráció a megfelelő névvel
    client.sendData(json.dumps({"command": "SetName", "name": "Pink", "payload": None}))

    # Nincs blokkoló hívás, a főszál várakozó állapotba kerül, itt végrehajthatók egyéb műveletek a kliens automata működésétől függetlenül.