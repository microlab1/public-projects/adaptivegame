# **Adaptív Rendszerek Házi Feladat**

### A projekt tartalmaz három elemet, ami az eredetiben nem volt vagy más volt:
1. neur_tanitas.py
2. Client_ViDa.py
3. neurhalo.model

#### neur_tanitas.py
Ebben a fájlban található a neurális háló betanítása. 
Maga a hálózat a keras API-jal lett létrehozva. A hálózat teljesen kapcsolt, lineáris és előre csatolt.
Három rétegből áll: Az első rétegnek 82 bemenete van és 40(kimenet) neuronból áll, amiknek RELU aktivációs függvényük van.
A második réteg 20 neuronból áll és szintén RELU aktivációs függvényt tartalmaznak. Az utolsó harmadik réteg 2 neuront tartalmaz, amiknek tanh az aktivációs függvénye.
Erre azért van szükség, hogy a két kimenet tudjon negatív is lenni.<br />

A neurális háló bemenetei a saját ügynökünk mérete és a 81 pixel, amit lát. A kimenete pedig a lépés X és Y tengely mentén.

**A játékban megszabott látótér számozása:**
![jatek_vision](C:\Users\Darius\PycharmProjects\adaptivegame-main\src\kepek\jatek_vision.png)

**Saját látótér számozás:**
![sajat_vision](C:\Users\Darius\PycharmProjects\adaptivegame-main\src\kepek\sajat_vision.png)

**A kimenetek variációi:**<br />
![kimenetek](C:\Users\Darius\PycharmProjects\adaptivegame-main\src\kepek\kimenetek.png)

**A tanításhoz szükséges importok:**
```python
import numpy as np
import matplotlib.pyplot as plt

from keras.models import Sequential
from keras.layers.core import Dense, Activation
```
A betanításhoz létrehoztam egy bemeneti mátrixot X-et és hozzá egy Y kimeneti mátrixot.

**A keras model létrehozásának és tanításának kódja:**
```python
# building a linear stack of layers with the sequential model
model = Sequential()

#bemenetek: saját méret,látómező 81 pixele
model.add(Dense(40, input_dim=82, activation='relu'))
model.add(Dense(20, activation='relu'))
model.add(Dense(2, activation='tanh'))

# compiling the sequential model
model.compile(loss='mse', metrics=['accuracy'], optimizer='adam')

# training the model and saving metrics in history
history = model.fit(X, y, epochs=330, verbose = 2)
```
**A tanulás lefolyásának diagramja:**
![Figure_1](C:\Users\Darius\PycharmProjects\adaptivegame-main\src\kepek\Figure_1.png)

Tanulás mentése:
```python
model.save('neurhalo.model')
```
Ezután jelenik meg a mentett model ebben a mappában: _[neurhalo.model](neurhalo.model)_

#### Az Example_Client_Main.py

Először is a keras model futásához szükségünk van a tensorflowra és magára a kimentett modelre.
```python
import tensorflow as tf

model=tf.keras.models.load_model("neurhalo.model")
```
Ezután átírtam a processObservation-t "Akció előállítása bemenetek alapján" után.

**A model bemenetének képzése a látótér alapján:**
```python
 BeX = np.zeros((1, 82))
            BeX[0] = jsonData["vision"][40]["player"]["size"]
            if jsonData["vision"][45]["player"] is None:
                BeX[0][1] = jsonData["vision"][45]["value"]
            else:
                BeX[0][1] = jsonData["vision"][45]["player"]["size"]
```
Mivel az általam felvett látótér és a játék által közölt játéktér semennyire nem egyezett, ezért sajnos a fenti függvényeket 81szer meg kellett ismételni.

A bemenő vektorból létrejön egy becslés(prediction) és attól függően, hogy a neurális háló mit hoz ki a végére pluszt, minuszt vagy 0-át tesz játék üzenetbe X és Y helyére is. 
```python
prediction = model.predict([BeX])
            if prediction[0][0] <= -0.5:
                Xaction = "-"
            elif prediction[0][0] >= 0.5:
                Xaction = "+"
            else:
                Xaction = "0"

            if prediction[0][1] <= -0.5:
                Yaction = "-"
            elif prediction[0][1] >= 0.5:
                Yaction = "+"
            else:
                Yaction = "0"

            #self.nextAction = Xaction + Yaction  # model kimenet
            actstring=Xaction + Yaction
            # Akció JSON előállítása és elküldése
            sendData(json.dumps({"command": "SetAction", "name": "RemotePlayer", "payload": actstring}))
```


### **Szükséges Packages**

Js2Py	0.71	0.71<br />
Keras-Preprocessing	1.1.2	1.1.2<br />
Markdown	3.3.6	3.3.7<br />
Pillow	9.1.0	9.1.1<br />
PyPrind	2.11.3	2.11.3<br />
Werkzeug	2.1.2	2.1.2<br />
absl-py	1.0.0	1.0.0<br />
astunparse	1.6.3	1.6.3<br />
beautifulsoup4	4.10.0	4.11.1<br />
cachetools	5.0.0	5.1.0<br />
certifi	2021.10.8	2022.5.18.1<br />
charset-normalizer	2.0.7	2.0.12<br />
comtypes	1.1.10	1.1.11<br />
cycler	0.11.0	0.11.0<br />
docopt	0.6.2	0.6.2<br />
flatbuffers	2.0	2.0<br />
fonttools	4.33.3	4.33.3<br />
gast	0.5.3	0.5.3<br />
google-auth	2.6.6	2.6.6<br />
google-auth-oauthlib	0.4.6	0.5.1<br />
google-pasta	0.2.0	0.2.0<br />
grpcio	1.44.0	1.46.3<br />
h5py	3.6.0	3.7.0<br />
idna	3.3	3.3<br />
keras	2.8.0	2.9.0<br />
kiwisolver	1.4.2	1.4.2<br />
libclang	14.0.1	14.0.1<br />
matplotlib	3.5.2	3.5.2<br />
numpy	1.22.3	1.22.4<br />
oauthlib	3.2.0	3.2.0<br />
opt-einsum	3.3.0	3.3.0<br />
packaging	21.0	21.3<br />
pip	22.0.4	22.1.1<br />
pipwin	0.5.1	0.5.2<br />
protobuf	3.20.1	4.21.0<br />
pySmartDL	1.3.4	1.3.4<br />
pyasn1	0.4.8	0.4.8<br />
pyasn1-modules	0.2.8	0.2.8<br />
pybrot	0.0.7	0.0.7<br />
pygame	2.1.2	2.1.2<br />
pyjsparser	2.7.1	2.7.1<br />
pyparsing	2.4.7	3.0.9<br />
pypiwin32	223	223<br />
python-dateutil	2.8.2	2.8.2<br />
pywin32	302	304<br />
requests	2.26.0	2.27.1<br />
requests-oauthlib	1.3.1	1.3.1<br />
rsa	4.8	4.8<br />
scipy	1.8.0	1.8.1<br />
setuptools	58.2.0	62.3.2<br />
six	1.16.0	1.16.0<br />
soupsieve	2.2.1	2.3.2.post1<br />
tensorboard	2.8.0	2.9.0<br />
tensorboard-data-server	0.6.1	0.6.1<br />
tensorboard-plugin-wit	1.8.1	1.8.1<br />
tensorflow	2.8.0	2.9.1<br />
tensorflow-io-gcs-filesystem	0.25.0	0.26.0<br />
termcolor	1.1.0	1.1.0<br />
tf-estimator-nightly	2.8.0.dev2021122109	<br />
typing-extensions	4.2.0	4.2.0<br />
tzdata	2021.2.post0	2022.1<br />
tzlocal	3.0	4.2<br />
urllib3	1.26.7	1.26.9<br />
wheel	0.37.0	0.37.1<br />
wrapt	1.14.1	1.14.1<br />