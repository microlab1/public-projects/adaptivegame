#encoding: utf-8

#==============================================================
# Adds the src map to the path, so imports can work properly
#==============================================================
import pathlib
import sys

current_dir = pathlib.Path().resolve()
src_dir     = current_dir.parent.parent.joinpath("src")
path = str(src_dir).replace("/", "\\")
sys.path.append(path)
#==============================================================

from smtplib import OLDSTYLE_AUTH
import time
import Config
from Client import SocketClient
import json
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import os

import random
from collections import deque

from datetime import datetime

maps=["./maps/01_ring_empty.txt","./maps/05_Maze.txt" ,"./maps/04_mirror.txt","./maps/02_base.txt","./maps/03_blockade.txt"]

class Linear_QNet(nn.Module):
    def __init__(self, input_size, hidden_size1,hidden_size2, output_size):
        super().__init__()
        self.linear1 = nn.Linear(input_size, hidden_size1)
        #self.linear2 = nn.Linear(hidden_size1, hidden_size2)
        self.linear3 = nn.Linear(hidden_size1,output_size)

    def forward(self, x):
        x = F.relu(self.linear1(x))
        #x = F.relu(self.linear2(x))
        x = self.linear3(x)
        return x

    def save(self, file_name='model.pth'):
        model_folder_path = './model'
        if not os.path.exists(model_folder_path):
            os.makedirs(model_folder_path)

        file_name = os.path.join(model_folder_path, file_name)
        torch.save(self.state_dict(), file_name)


class QTrainer:
    def __init__(self, model, lr, gamma):
        self.lr = lr
        self.gamma = gamma
        self.model = model
        self.optimizer = optim.Adam(model.parameters(), lr=self.lr)
        self.criterion = nn.MSELoss()

    def train_step(self, state, action, reward, next_state, done):
        state = torch.tensor(state, dtype=torch.float)
        next_state = torch.tensor(next_state, dtype=torch.float)
        action = torch.tensor(action, dtype=torch.long)
        reward = torch.tensor(reward, dtype=torch.float)

        if len(state.shape) == 1:
            state = torch.unsqueeze(state, 0)
            next_state = torch.unsqueeze(next_state, 0)
            action = torch.unsqueeze(action, 0)
            reward = torch.unsqueeze(reward, 0)
            done = (done, )

        pred = self.model(state)

        target = pred.clone()
        for index in range(len(done)):
            Q_new = reward[index]
            if not done[index]:
                Q_new = reward[index] + self.gamma * torch.max(self.model(next_state[index]))

            target[index][torch.argmax(action[index]).item()] = Q_new

        self.optimizer.zero_grad()
        loss = self.criterion(target, pred)
        loss.backward()

        self.optimizer.step()

MAX_MEMORY = 50_00#50_000
BATCH_SIZE = 100
LR = 0.1

epochs=50
global isTeaching
possibility=5
hidden_size1=128
hidden_size2=32
gamma=0.9

ACTIONS = [
    '0+',
    '0-',
    '+0',
    '-0',
    '++',
    '--',
    '+-',
    '-+'
]

class RemoteAgentStrategy:

    def __init__(self):
        self.oldstate = None
        self.oldaction = None
        self.oldscore = None
        self.oldpos = None
        self.oldCounter=0
        self.game_fitness=0
        self.model = Linear_QNet(81, hidden_size1,hidden_size2, len(ACTIONS))
        if(os.path.exists('./model/model.pth')):
            self.model.load_state_dict(torch.load('./model/model.pth'))
            print("Model is loaded...")
        else:
            self.model = Linear_QNet(81, hidden_size1,hidden_size2, len(ACTIONS)) #input size, hidden size, output size
        self.trainer = QTrainer(self.model, lr=LR, gamma=gamma)
        self.memory = deque(maxlen=MAX_MEMORY)
        self.cnt=0
        self.teach_log_file="epochlog.txt"
        self.teach_start()
        self.pos_memory=[]

    #====================Tannitas logolasat segíto fgvek===========================
    def teach_start(self):
        with open(self.teach_log_file,"a") as log_f:
            teaching_start = "\n=========="+'Teaching :' + datetime.now().strftime('%Y_%m_%d_%H_%M_%S')+"========== \n"
            log_f.write(teaching_start)
        log_f.close()

    def teach_end(self):
        with open(self.teach_log_file,"a") as log_f:
            teaching_start = "=========="+'Teaching END'+"========== \n"
            log_f.write(teaching_start)
        log_f.close()

    # ===========================================================================

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def train_long_memory(self):
        if len(self.memory) > BATCH_SIZE:
            mini_sample = random.sample(self.memory, BATCH_SIZE) # list of tuples
        else:
            mini_sample = self.memory
        #print("Mini sample: ",mini_sample)
        states, actions, rewards, next_states, dones = zip(*mini_sample)
        self.trainer.train_step(states, actions, rewards, next_states, dones)

    def train_short_memory(self, state, action, reward, next_state, done):
        self.trainer.train_step(state, action, reward, next_state, done)

    def get_opposite_action(self,idx):
        final_move_vector = [0] * len(ACTIONS)

        if(idx==0):
            idx=1
            final_move_vector[idx]+=1
            return idx,final_move_vector
        elif(idx==1):
            idx = 2
            final_move_vector[idx] += 1
            return idx, final_move_vector
        elif(idx==2):
            idx = 3
            final_move_vector[idx] += 1
            return idx, final_move_vector
        elif (idx == 3):
            idx = 4
            final_move_vector[idx] += 1
            return idx, final_move_vector
        elif (idx == 4):
            idx = 5
            final_move_vector[idx] += 1
            return idx, final_move_vector
        elif (idx == 5):
            idx = 6
            final_move_vector[idx] += 1
            return idx, final_move_vector
        elif (idx == 6):
            idx = 7
            final_move_vector[idx] += 1
            return idx, final_move_vector
        elif (idx == 7):
            idx = 0
            final_move_vector[idx] += 1
            return idx, final_move_vector

    def get_action(self, state):

        if(isTeaching):
            # valoszinuseg szerint uj random lepesek kiprobalasa
            if random.randint(1, 100) < possibility:
                pred_action = np.random.randint(0, len(ACTIONS))

            else:
                #modell alapjan kovetkezo lepes prediktalasa:
                prediction = self.model(state)
                pred_action = torch.argmax(prediction).item()

        else:
            prediction = self.model(state)
            pred_action = torch.argmax(prediction).item()


        #kimeneti lepes jelzese 1-el, amelyik kimeneti neuron a legnagyobb valoszinusegu:
        final_move_vector = [0] * len(ACTIONS)
        final_move_vector[pred_action] += 1
        print("Final move: ",final_move_vector)

        return final_move_vector, pred_action

    # Az egyetlen kötelező elem: A játékmestertől jövő információt feldolgozó és választ elküldő függvény
    def processObservation(self, fulljson, sendData):
        """
                :param fulljson: A játékmestertől érkező JSON dict-be konvertálva.
                Két kötelező kulccsal: 'type' (leaderBoard, readyToStart, started, gameData, serverClose) és 'payload' (az üzenet adatrésze).
                'leaderBoard' type a játék végét jelzi, a payload tartalma {'ticks': a játék hossza tickekben, 'players':[{'name': jáétékosnév, 'active': él-e a játékos?, 'maxSize': a legnagyobb elért méret a játék során},...]}
                'readyToStart' type esetén a szerver az indító üzenetre vár esetén, a payload üres (None)
                'started' type esetén a játék elindul, tickLength-enként kiküldés és akciófogadás várható payload {'tickLength': egy tick hossza }
                'gameData' type esetén az üzenet a játékos által elérhető információkat küldi, a payload:
                                            {"pos": abszolút pozíció a térképen, "tick": az aktuális tick sorszáma, "active": a saját életünk állapota,
                                            "size": saját méret,
                                            "leaderBoard": {'ticks': a játék hossza tickekben eddig, 'players':[{'name': jáétékosnév, 'active': él-e a játékos?, 'maxSize': a legnagyobb elért méret a játék során eddig},...]},
                                            "vision": [{"relative_coord": az adott megfigyelt mező relatív koordinátája,
                                                                            "value": az adott megfigyelt mező értéke (0-3,9),
                                                                            "player": None, ha nincs aktív játékos, vagy
                                                                                    {name: a mezőn álló játékos neve, size: a mezőn álló játékos mérete}},...] }
                'serverClose' type esetén a játékmester szabályos, vagy hiba okozta bezáródásáról értesülünk, a payload üres (None)
                :param sendData: A kliens adatküldő függvénye, JSON formátumú str bemenetet vár, melyet a játékmester felé továbbít.
                Az elküldött adat struktúrája {"command": Parancs típusa, "name": A küldő azonosítója, "payload": az üzenet adatrésze}
                Elérhető parancsok:
                'SetName' A kliens felregisztrálja a saját nevét a szervernek, enélkül a nevünkhöz tartozó üzenetek nem térnek vissza.
                         Tiltott nevek: a configban megadott játékmester név és az 'all'.
                'SetAction' Ebben az esetben a payload az akció string, amely két karaktert tartalmaz az X és az Y koordináták (matematikai mátrix indexelés) menti elmozdulásra.
                        a karakterek értékei '0': helybenmaradás az adott tengely mentén, '+' pozitív irányú lépés, '-' negatív irányú lépés lehet. Amennyiben egy tick ideje alatt
                        nem külünk értéket az alapértelmezett '00' kerül végrehajtásra.
                'GameControl' üzeneteket csak a Config.py-ban megadott játékmester névvel lehet küldeni, ezek a játékmenetet befolyásoló üzenetek.
                        A payload az üzenet típusát (type), valamint az ahhoz tartozó 'data' adatokat kell, hogy tartalmazza.
                            'start' type elindítja a játékot egy "readyToStart" üzenetet küldött játék esetén, 'data' mezője üres (None)
                            'reset' type egy játék után várakozó 'leaderBoard'-ot küldött játékot állít alaphelyzetbe. A 'data' mező
                                    {'mapPath':None, vagy elérési útvonal, 'updateMapPath': None, vagy elérési útvonal} formátumú, ahol None
                                    esetén az előző pálya és növekedési map kerül megtartásra, míg elérési útvonal megadása esetén új pálya kerül betöltésre annak megfelelően.
                            'interrupt' type esetén a 'data' mező üres (None), ez megszakítja a szerver futását és szabályosan leállítja azt.
                :return:
                """


        # Játék rendezéssel kapcsolatos üzenetek lekezelése
        if fulljson["type"] == "leaderBoard":
            print("Game finished after",fulljson["payload"]["ticks"],"ticks!")
            print("Leaderboard:")

            #jatekok eredmenyeinek kimentese statisztikai celbol:
            with open("epochlog.txt","a") as log_out:
                for score in fulljson["payload"]["players"]:
                    print(score["name"], score["active"], score["maxSize"])
                    log=str(self.cnt)+". epoch: \t"+(score["name"])+"\t"+ str(score["active"])+"\t"+ str(score["maxSize"])+"\t"
                    log_out.write(log)
                log_out.write("\n")
            log_out.close()

            #hosszutavu memoria alapjan valo tanitas:
            self.train_long_memory()

            #time.sleep(5)
            self.cnt+=1
            #jatek vegi modell kimentese:
            self.model.save()
            print("model saved...")
            time.sleep(0.1)

            if (self.cnt < epochs and isTeaching):
                idx=self.cnt%5
                sendData(json.dumps({"command": "GameControl", "name": "master",
                                     "payload": {"type": "reset", "data": {"mapPath": maps[idx], "updateMapPath": maps[idx]}}}))
            else:
                    print("===Teaching is ready===")
                    self.teach_end()
                    #isTeaching=False
                    print("===Game is done===")

        if fulljson["type"] == "readyToStart":
            print("Game is ready, starting in 5")
            #time.sleep(5)
            self.model.load_state_dict(torch.load('./model/model.pth'))
            print("model loaded...")
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "start", "data": None}}))

        if fulljson["type"] == "started":
            print("Startup message from server.")
            print("Ticks interval is:",fulljson["payload"]["tickLength"])

        # Akció előállítása bemenetek alapján (egyezik a NaiveHunterBot-okéval)
        elif fulljson["type"] == "gameData":
            jsonData = fulljson["payload"]
            #valid json kulcsertekek ellenorzese
            if "pos" in jsonData.keys() and\
                    "tick" in jsonData.keys() and\
                    "active" in jsonData.keys() and\
                    "size" in jsonData.keys() and\
                    "vision" in jsonData.keys():
                #bemeneti allapotok definialasa
                self.pos_memory.append(jsonData["pos"])

                state = []
                for field in jsonData["vision"]:
                    state.append(field["value"])

                #for size in jsonData["leaderBoard"]["players"]:
                #    state.append(size["maxSize"])

                #model tensorainak letrehozasa a tanitas-kiertekeles vegett
                state_tensor = torch.tensor(state, dtype=torch.float)
                next_action, action_index = self.get_action(state_tensor)
                #kimeneti string letrehozasa:
                #if(self.oldpos==jsonData["pos"] and self.oldaction==next_action):
                #    print("before sort: ",pred_tensor)
                #    valami=torch.sort(pred_tensor)
                #    print("Valami: ",valami)
                #action_str = ACTIONS[action_index]
                #jutalmazas es tanitas implementalasa, ha mar van memoriaban korabbi allapot:
                if self.oldstate is not None:

                    State = self.oldstate
                    Action = self.oldaction

                    #Reward policy:
                    Reward = (jsonData["size"] - self.oldscore )*400
                    if(self.oldscore==jsonData["size"]):
                        Reward-=0#meretnovekedessel aranyos jutalmazas
                    if jsonData["pos"] == self.oldpos:
                        Reward -=  100
                    else:
                        Reward+=100   #torekves a mozgasra
                    if jsonData["active"]=="False":
                        Reward -= 400
                    else:
                        Reward+=0#ha meghal, nagyon buntetjuk
                    if self.oldaction == next_action and self.oldpos == jsonData["pos"]:
                        Reward-= 300
                        self.oldCounter+=1
                    else:
                        Reward+=200 #falnak menes buntetese, mivel allast nem engedunk
                        self.oldCounter=0
                    Reward-=self.oldCounter*100
                    #Reward-=self.to_center(jsonData["pos"])*50
                    #if(len(self.pos_memory)>5):
                    #   Reward+=30*self.dist(self.pos_memory[-1],self.pos_memory[-4])   #terkep felfedezesenek osztonzese

                    #Reward-=self.oldCounter*20
                    Next_state = state
                    done = False
                    #print("old action: ",self.oldaction)
                    #if(self.oldpos==jsonData["pos"] and self.oldaction==next_action):
                        #base_action=action_index
                    #    action_index,next_action=self.get_opposite_action(action_index)

                        #while(action_index==base_action):
                        #    action_index=np.random.randint(0,len(ACTIONS))
                        #    print("Base - action",base_action, action_index)
                        
                    #print("Act reward: ",Reward)'''

                    #reward, action, state alapjan modell tanitasa rovid tavu memoria alapjan:
                    self.train_short_memory(State, Action, Reward, Next_state, done)
                    #allapotok eltarolasa:
                    self.remember(State, Action, Reward, Next_state, done)
                    #print("Act reward: ", Reward)
                    #self.game_fitness=Reward

                action_str = ACTIONS[action_index]
                #Kovtkezo ciklushoz a jelenlegi allapotok elmentese:
                self.oldstate = state
                self.oldaction = next_action
                self.oldscore = jsonData["size"]
                self.oldpos = jsonData["pos"]

                # Akció JSON előállítása és elküldése
                sendData(json.dumps({"command": "SetAction", "name": "Pink", "payload": action_str}))
                #print("Game fitness act: ",self.game_fitness)

    def dist(self,pos1,pos2):
        print("Dist: ",(pos1[0]-pos2[0])**2+(pos1[1]-pos2[1])**2)
        return np.sqrt((pos1[0]-pos2[0])^2+(pos1[1]-pos2[1])^2)

    def direction(self,p1,p2):
        return p1[0]-p2[0],p1[1]-p2[1]

    def to_center(self,act_pos):
        x=act_pos[0]
        y=act_pos[1]
        x0=19
        y0=19
        return ((x0-x)**2+(y0-y)**2)

if __name__=="__main__":
    # Példányosított stratégia objektum
    hunter = RemoteAgentStrategy()
    isTeaching=True
    # Socket kliens, melynek a szerver címét kell megadni (IP, port), illetve a callback függvényt, melynek szignatúrája a fenti
    # callback(fulljson, sendData)

    client = SocketClient("localhost", 42069, hunter.processObservation)

    # Kliens indítása
    client.start()
    # Kis szünet, hogy a kapcsolat felépülhessen, a start nem blockol, a kliens külső szálon fut
    time.sleep(0.1)
    # Regisztráció a megfelelő névvel
    client.sendData(json.dumps({"command": "SetName", "Pink": "Yellow", "payload": None}))

    # Nincs blokkoló hívás, a főszál várakozó állapotba kerül, itt végrehajthatók egyéb műveletek a kliens automata működésétől függetlenül.
