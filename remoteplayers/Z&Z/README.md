<img src="/GPK_BME_MOGI.png">

# AdaptIO megoldás - Z&Z

Az AdaptIO házi feladat megoldása során több lehetséges algoritmust is kidolgoztunk, amelyke közül a Reinforcement Learning bizonyult a leghatékonyabbnak. (Emellett evolúciós és genetikus algoritmusokkal is kísérleteztünk, amelyek a házi feladat leadásának idejéig nem eredményeztek megfelelő pontosságot, ezért ezeket nem fejlesztettük tovább.)

Alapvetően a Reinforcement Learning általános lépéseit implementáltuk, amelyek a következők:
- Akciók (action-ok) definiálása
- Állapotok definiálása
- Jutalmazási rendszer (Reward Policy) definiálása
- Tanítási paraméterek és a neurális háló szerkezetének definiálása

A fenti esetek megvalósítását a Python környezeten belül a pytorch könyvtár segítségével implementáltuk, melynek eredményeképpen a következő struktúrát kaptuk:

**A tanulási paraméterek (kódrészlet):**
<br />
<img src="/tan_parameterek.png" width="300">
<br />
**A neurális háló modellje (kódrészlet):**
<br />
<img src="/neur_halo_modell.png" width="500">
<br />
**A DeepQ trainer (kódrészlet):**
<br />
<img src="/deepq_trainer.png" width="500">
<br />
**A modell Reward Policy-je (kódrészlet):**
<br />
<img src="/reward_policy.png" width="600">

## Tanítás menete

Definiálunk egy RemoteAgentStrategy osztályt, amely tartalmazza, hogy hányszor menjünk végig az adott pályákon az adott agentünkkel, illetve tartalmazza a neurális hálót, amelyet tanítani szeretnénk. Amennyiben ez a háló még nem létezik, akkor létrehozza azt, amennyiben nem létezik, akkor pedig a korábbi modellt tölti be.

Amikor elindítjuk a játékot az adott térképen, akkor a rövid- és hosszútávú memória segítségével tanítjuk az agentünket, hogy minél jobb pontszámot érjen el és minél tovább maradjon életben a pályán.

A tanítást befolyásoló paraméterek a következők:
- MAX_MEMORY: a globális memóriáját határozza meg a korábbi játékokból vett mintáknak
- BATCH_SIZE: a rövid távú tanítás memóriáját határozza meg
- lr: a tanulás konvergenciájának sebességét határozza meg (learning rate)
- epochs: a tanító játékmenetek számát határozza meg
- possibility: a random lépésekkel feltérképezett események generálását segíti elő a gyorsabb tanulás elérése érdekében
- hidden_size: a neurális háló egy rejtett rétegének méretét adja meg
- gamma: a Reinforcement Learning-ben felhasznált Bellman-egyenlet paramétere

Az egyes játékok alatt a fent bemutatott paramétereket megválasztottuk és ezekkel tanítottuk be az adott agentünket minden egyes játék végén. A játék végén kimentettük a modellt és a későbbiekben ezt töltöttük be és ennek a tanítását folytattuk tovább addig, amíg az epochszámig el nem ért a számláló. 

Minden egyes játék egy másik térképen zajlik, ezzel növelve a stratégiánk összetettségét.

A Reward Policy megválasztása során az alábbi eseteket vettük figyelembe:
- Táplálkozás ösztönzése a méretnövekedéssel arányos pontszámmal
- Térkép felfedezése, folymatos mozgás ösztönzése
- Falnak ütközés büntetése
- Halál esetén súlyos büntetés

## Konklúzió

Azt tapasztaltuk, hogy amennyiben a neurális háló rejtett rétegét növeltük vagy több rejtett réteget használtunk, akkor a neurális háló komplexitása megnőtt, így a fenti paramétereket hosszas próbálgatások során állítottuk be, így a többi beállításhoz képest jobban tanult az agent.

Összességében, ha a játékosunk "beletanult", akkor 50-70 kaját evett meg nehezített pályán, és volt, hogy levadászott másokat is.

## Credits
Csuja Zoltán <br>
Dombi Zoltán
