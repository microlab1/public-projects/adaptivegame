#encoding: utf-8
#previous step :
#inner fitness
#posx
#posy
#expected fitness  0-3 
#current fitness
#static counter  If static counter > 2  -1 inner fitness
#dynamic counter if dynmaic counter > 5 -1 inner fitness

#==============================================================
# Adds the src map to the path, so imports can work properly
#==============================================================
import pathlib
import sys

current_dir = pathlib.Path().resolve()
src_dir     = current_dir.parent.parent.joinpath("src")
path = str(src_dir).replace("/", "\\")
sys.path.append(path)
#==============================================================

import time
from Client import SocketClient
import json
import numpy as np
import random
import matplotlib.pyplot as plt
import pickle 

from My_Agent_Model3 import MyBacteriumAgent #a stratégia motorja


#saját stratégia definíciója
class MyStrategy:

    def __init__(self, my_agent):
        #belső változók definíciója
        self.my_agent = my_agent #a stratégia motorjának egy példánya
        self.act_string_dict = { #dictionary, hogy az adott integer kimeneteket igazi lépésre váltsuk át
            0: '00',
            1: '0+',
            2: '++',
            3: '+0',
            4: '+-',
            5: '0-',
            6: '--',
            7: '-0',
            8: '-+'
        }
        self.train = True #train vagy play 

        #böntető függvényhez szükséges változók
        self.previous_location = [0,0] #előző pozíció
        self.previous_dynamic_location = [0,0] #előző olyan pozíció aminek az egyes környezetéből nem jött még ki az agent
        self.location = [0,0] #jelenlegi pozíció
        self.static_location_counter = 0 #pozíció nem változásának mérése
        self.dynamic_location_counter = 0 #dinamikus pozíció nem változásának mérése

        self.previous_size = 5 #előző méret
        self.size = 5 #jelenlegi méret
        self.expected_growth = 0 #elvárt méret növekedés
        self.fitness = 5 #fitnesz érték
    

    def action_define(self, output):
        '''az egyes lépés lehetőségek alapján kiválasztja, hogy mi legyen a lépés'''
        if self.my_agent.training:# ha atnítani szertnénk
            if np.max(output) > 0.5: #ha van olyan lépés aminek a háló nagyobb mint 50 a valószínűsége akkor az lesz
                max = np.argmax(output, axis=0)
                return int(max)
            else: #ha nincs 50-nél nagyobb valószínűség random módon lesz eldöntve
                rand = random.random() #0 és 1 között random
                for i in range(9):
                    max = np.argmax(output, axis=0)
                    rand = rand - output[max] #kivonjuk belőle mindig a legnagyobb százalékú elem százalékát
                    if rand <= 0: #hogyha kisebb lesz mint nulla -> az a lépés lesz aminek a százalékát kivontuk belőle
                        return int(max)
                    else: #ha nem akkor megyünk a következő legnaygobb százalékú elemre
                        output[max] = -1

                return 0
        else:# ha játék megy
            maxes = []
            for i in range(5):
                maxes.append(int(np.argmax(output[i], axis=0))) #az öt agent leginkáb preferált lépése
            for i in range(9):
                if maxes.count(i) > 4: # ha legalább 4 ugyaaaz akkor az a lépés lesz
                    return i
            new_output = (output[0] + output[1] + output[2] +  output[3] + output[4] )/5 #öt kimenet összegzése - innentől ugyanaz mint előtte
            if np.max(new_output) > 0.5: #ha van olyan lépés aminek a háló nagyobb mint 50 a valószínűsége akkor az lesz
                max = np.argmax(new_output, axis=0)
                return int(max)
            else: #ha nincs 50-nél nagyobb valószínűség random módon lesz eldöntve
                rand = random.random() #0 és 1 között random
                for i in range(9):
                    max = np.argmax(new_output, axis=0)
                    rand = rand - new_output[max] #kivonjuk belőle mindig a legnagyobb százalékú elem százalékát
                    if rand <= 0: #hogyha kisebb lesz mint nulla -> az a lépés lesz aminek a százalékát kivontuk belőle
                        return int(max)
                    else: #ha nem akkor megyünk a következő legnagyobb százalékú elemre
                        new_output[max] = -1

                return 0

    def setObservations(self, fieldDict):
        '''A látómező alapján visszaadja a lépéseket és a hozzájuk tartozó valószínűséget'''
        #minden mezőhöz 4 érték van hozzárendelve: relatív x, relatív y, mező értéke és ha van rajta másik agent annak a mérete egyébként 0
        vision = np.zeros((81,4))  
        for i in range(len(fieldDict["vision"])):
            if fieldDict["vision"][i]["player"] == None:
                vision[i,:] = np.array([   (fieldDict["vision"][i]["relative_coord"][0] + 4)/8, # 0 és 1 közé normálva
                                                        (fieldDict["vision"][i]["relative_coord"][1] + 4)/8, # 0 és 1 közé normálva
                                                        fieldDict["vision"][i]["value"] / 9, # 0 és 1 közé normálva
                                                        0
                                                        ])
            else:                                            
                vision[i,:] = np.array([   (fieldDict["vision"][i]["relative_coord"][0] + 4)/8,
                                                    (fieldDict["vision"][i]["relative_coord"][1] + 4)/8,
                                                    fieldDict["vision"][i]["value"] / 9,
                                                    fieldDict["vision"][i]["player"]["size"] / 50 # körülbelül 0 és 1 közé normálva
                                                    ])
                                                
        input = vision.flatten(order = 'C') #oszlpvektorrá összenyomva
        return input
    
    def reset_fitness_values(self):
        '''a fitnesz számításához szükséges változók resetelése'''
        # a játék újraindítása miatt szükséges
        self.previous_location = [0,0]
        self.previous_dynamic_location = [0,0]
        self.location = [0,0]
        self.static_location_counter = 0
        self.dynamic_location_counter = 0

        self.previous_size = 5
        self.size = 5
        self.expected_growth = 0
        self.fitness = 5

    # A játékmestertől jövő információt feldolgozó és választ elküldő függvény
    def processObservation(self, fulljson, sendData):
        """
        :param fulljson: A játékmestertől érkező JSON dict-be konvertálva.
        Két kötelező kulccsal: 'type' (leaderBoard, readyToStart, started, gameData, serverClose) és 'payload' (az üzenet adatrésze).
        'leaderBoard' type a játék végét jelzi, a payload tartalma {'ticks': a játék hossza tickekben, 'players':[{'name': jáétékosnév, 'active': él-e a játékos?, 'maxSize': a legnagyobb elért méret a játék során},...]}
        'readyToStart' type esetén a szerver az indító üzenetre vár esetén, a payload üres (None)
        'started' type esetén a játék elindul, tickLength-enként kiküldés és akciófogadás várható payload {'tickLength': egy tick hossza }
        'gameData' type esetén az üzenet a játékos által elérhető információkat küldi, a payload:
                                    {"pos": abszolút pozíció a térképen, "tick": az aktuális tick sorszáma, "active": a saját életünk állapota,
                                    "size": saját méret, "vision": [{"relative_coord": az adott megfigyelt mező relatív koordinátája,
                                                                    "value": az adott megfigyelt mező értéke (0-3,9),
                                                                    "player": None, ha nincs aktív játékos, vagy
                                                                            {name: a mezőn álló játékos neve, size: a mezőn álló játékos mérete}},...] }
        'serverClose' type esetén a játékmester szabályos, vagy hiba okozta bezáródásáról értesülünk, a payload üres (None)
        :param sendData: A kliens adatküldő függvénye, JSON formátumú str bemenetet vár, melyet a játékmester felé továbbít.
        Az elküldött adat struktúrája {"command": Parancs típusa, "name": A küldő azonosítója, "payload": az üzenet adatrésze}
        Elérhető parancsok:
        'SetName' A kliens felregisztrálja a saját nevét a szervernek, enélkül a nevünkhöz tartozó üzenetek nem térnek vissza.
                 Tiltott nevek: a configban megadott játékmester név és az 'all'.
        'SetAction' Ebben az esetben a payload az akció string, amely két karaktert tartalmaz az X és az Y koordináták (matematikai mátrix indexelés) menti elmozdulásra.
                a karakterek értékei '0': helybenmaradás az adott tengely mentén, '+' pozitív irányú lépés, '-' negatív irányú lépés lehet. Amennyiben egy tick ideje alatt
                nem külünk értéket az alapértelmezett '00' kerül végrehajtásra.
        'GameControl' üzeneteket csak a Config.py-ban megadott játékmester névvel lehet küldeni, ezek a játékmenetet befolyásoló üzenetek.
                A payload az üzenet típusát (type), valamint az ahhoz tartozó 'data' adatokat kell, hogy tartalmazza.
                    'start' type elindítja a játékot egy "readyToStart" üzenetet küldött játék esetén, 'data' mezője üres (None)
                    'reset' type egy játék után várakozó 'leaderBoard'-ot küldött játékot állít alaphelyzetbe. A 'data' mező
                            {'mapPath':None, vagy elérési útvonal, 'updateMapPath': None, vagy elérési útvonal} formátumú, ahol None
                            esetén az előző pálya és növekedési map kerül megtartásra, míg elérési útvonal megadása esetén új pálya kerül betöltésre annak megfelelően.
                    'interrupt' type esetén a 'data' mező üres (None), ez megszakítja a szerver futását és szabályosan leállítja azt.
        :return:
        """

        # Játék rendezéssel kapcsolatos üzenetek lekezelése
        if fulljson["type"] == "leaderBoard":  #vége a játéknak
            print("Game finished after",fulljson["payload"]["ticks"],"ticks!") #mennyi tick volt a játékban
            print("Leaderboard:")

            
            size_log = [0,0,0,0] #az adott játékosok méretének lementéséhez kell
            for score in fulljson["payload"]["players"]: 
                print(score["name"],score["active"], score["maxSize"]) #a játékosok mérete és hogy életben maradtake 
                
                if score["name"] == 'RemotePlayer':#az egyes elemek méretének elmentése, ha nem trainingen van akkor nem fontos
                    size_log[0] = score["maxSize"]
                if score["name"] == 'Teszt2':
                    size_log[1] = score["maxSize"]
                if score["name"] == 'Teszt3':
                    size_log[2] = score["maxSize"]
                if score["name"] == 'Teszt4':
                    size_log[3] = score["maxSize"]
                
                

            if self.my_agent.training: #ha tanítani szeretnénk az algoritmust
                self.my_agent.size_log.append(size_log)#begyűjtött méretek elmentése
                self.my_agent.mutated_agent_fitnesses[self.my_agent.current_mutated_agent] = self.fitness
                with open("teszt2.pkl", 'rb') as file:
                    myvar = pickle.load(file) #a másik 3 player által számolt fitnesz értékek beolvasása és elmentése
                self.my_agent.mutated_agent_fitnesses[self.my_agent.current_mutated_agent+1] = myvar["fitness"] 
                with open("teszt3.pkl", 'rb') as file:
                    myvar = pickle.load(file)
                self.my_agent.mutated_agent_fitnesses[self.my_agent.current_mutated_agent+2] = myvar["fitness"] 
                with open("teszt4.pkl", 'rb') as file:
                    myvar = pickle.load(file)
                self.my_agent.mutated_agent_fitnesses[self.my_agent.current_mutated_agent+3] = myvar["fitness"] 
                
                self.reset_fitness_values()
                

                if self.my_agent.current_mutated_agent == 4: #ez jelzi azt, hogy egy agentnek az összes mutáns verzióját megvizsgáltuk
                    print("current agent: " + str(self.my_agent.current_agent + 1)) #kiiratások
                    print("current mutated agent: " + str(self.my_agent.current_mutated_agent + 1))
                    print(self.my_agent.mutated_agent_fitnesses) #az egyes mutáns agentek fitnesz értékeinek a kiiratáse
                    self.my_agent.find_best_version()#a legjobb fitneszű mutáns kiválasztása

                    if self.my_agent.current_agent == 19:# hogyha már az utolsó agentnél vagyunk

                        

                        if self.my_agent.current_iteration+1 != self.my_agent.learning_iterations:#hogyha még nem vagyunk az utolsó tanulási lépésnél
                            print("NEW ITERATION: " + str(self.my_agent.current_iteration + 1))#jelenlegi tanulási lépés
                            print("fitnesses: ")
                            print(self.my_agent.agent_fitnesses)#az adott generáció fitnesz értékeinek kiiratása
                            self.my_agent.current_iteration += 1#a tanulási lépés léptetése
                            self.my_agent.make_fitness_order()#fitnesz sorba rendezés
                            self.my_agent.gene_injection()#gén injektálás
                            self.my_agent.current_agent = 0#visszatérés az első agenthez
                            self.my_agent.create_mutants()#mutánsok készítése az első agentnek
                            self.my_agent.save_model()#mentés
                            self.my_agent.save_model("dummy_model.pkl")#mentés készítése a többi agent számára
                            sendData(json.dumps({"command": "GameControl", "name": "master", #reset miután vége lett a játéknak
                                            "payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))
                        else:#hogyha az utolsó tanulási lépésnél vagyunk
                            print("ENDING TRAINING:")
                            print("fitnesses: ")
                            print(self.my_agent.agent_fitnesses)#jelenlegi generáció fitneszei
                            self.my_agent.make_fitness_order()#fitnesz sorrend
                            self.my_agent.gene_injection()#gén injektálás
                            self.my_agent.current_agent = 0#visszatérés az agenthez
                            self.my_agent.create_mutants()#mutánsok készítése
                            self.my_agent.save_model()#mentés
                            self.my_agent.save_model("dummy_model.pkl")#mentés készítése a többi agent számára

                            for i in range(len(self.my_agent.learning_log)): #az összes tanulási lépésnél afitneszek kiirása
                                print(self.my_agent.learning_log[i])
                                print('\n')
                            self.my_agent.savelogs()# a statisztikák kimentése

                            plt.plot(self.my_agent.fitness_log[0])
                            plt.plot(self.my_agent.fitness_log[1])
                            plt.savefig('pic.png')# a fitnesz alakulásának kimentése
                            pass

                    else:#ha még nem vagyunk az utolsó agentnél
                        self.my_agent.current_agent += 1#következő agentre lépés
                        self.my_agent.create_mutants()#mutánsok készítése
                        self.my_agent.current_mutated_agent = 0#visszalépés az első mutánsra
                        self.my_agent.save_model("dummy_model.pkl")# mentés készítés a több agentnek
                        sendData(json.dumps({"command": "GameControl", "name": "master", #reset miután vége lett a játéknak
                                        "payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))
                
                else: #ha egy agentnek még nem vizsgáltuk meg az összes mutáns verzióját
                    print("current agent: " + str(self.my_agent.current_agent + 1))#kiiratások
                    print("current mutated agent: " + str(self.my_agent.current_mutated_agent + 1))
                    self.my_agent.current_mutated_agent += 4 #mutáns agent továbbléptetés
                    sendData(json.dumps({"command": "GameControl", "name": "master", #reset miután vége lett a játéknak
                                        "payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))

            
        if fulljson["type"] == "readyToStart": # ha készen áll a játék az indításra
            print("Game is ready, starting in 5")
            time.sleep(5)
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "start", "data": None}})) # játék indítása üzenettel

        if fulljson["type"] == "started": # elndult a játék
            print("Startup message from server.")
            print("Ticks interval is:",fulljson["payload"]["tickLength"]) # tickhossz, ha sokat számol az algoritmus lehet hosszabbra kell


        # Akció előállítása bemenetek alapján
        elif fulljson["type"] == "gameData":
            jsonData = fulljson["payload"]
            if "pos" in jsonData.keys() and "tick" in jsonData.keys() and "active" in jsonData.keys() and "size" in jsonData.keys() and "vision" in jsonData.keys():
                if jsonData["active"]:
                    pass
                
                input = self.setObservations(jsonData)# inputvektor létrehozása
                input = np.reshape(input, (324,1))# pluszdimenzió hozzáadása, hogy menjen a mátrix szorzás
                
                if self.my_agent.training:#ha tanítunk akkor a mutáns agentekkel kell lépni
                    output = self.my_agent.forward_mutated_step(input)
                else:
                    output = self.my_agent.forward_play_step(input)

                if self.my_agent.training:#hogyha tanítunk akkor itt van a fitnesz számítás    
                    #fitnesz: =================================
                    self.size = jsonData['size'] #jelenlegi méret
                    self.pos = jsonData['pos']# jelenlegi pozíció

                    plus = (self.size - self.previous_size)*1.5 #az összeszedett kaját 1.5 szorzóval veszzük figyelembe
                    minus = 0
                    if plus < self.expected_growth:
                        minus += self.expected_growth - plus # ha kisebb a növekedés mint az elvárt, akkor különbség minuszként jelenik meg

                    self.location = jsonData['pos']# az egyhelyben maradás büntetése
                    if self.location[0] == self.previous_location[0] and self.location[1] == self.previous_location[1]:
                        self.static_location_counter += 1
                    else:
                        self.static_location_counter = 0
                    
                    if self.static_location_counter > 3:# ha 3 ticknél tovább egyhelyben marad -> büntetés
                        minus += 1

                    #a dinamukis egyhelyben maradás büntetése - hogyha egy adott régiót nem hagy el az agent
                    if  self.location[0] > self.previous_dynamic_location[0] - 3 and self.location[0] < self.previous_dynamic_location[0] + 3 and \
                        self.location[1] > self.previous_dynamic_location[1] - 3 and self.location[1] < self.previous_dynamic_location[1] + 3:
                        self.dynamic_location_counter += 1
                    else:
                        self.dynamic_location_counter = 0
                        self.previous_dynamic_location = self.location

                    if self.dynamic_location_counter > 20:# ha 20 tick után is egy régióban marad akkor büntetés
                        minus += 1

                    self.fitness = self.fitness + plus - minus# végső fitnesz érték számítása

                    self.previous_location = self.location#
                    self.previous_size = self.size#az idő léptetése - jelenlegi értékek korábbi értékké változtatása
                    num = 0
                    values = 0
                    for tile in jsonData["vision"]: #az elvárt növekeédes kiszámítása: az agnt környezetében található kaják átlagos értéke
                        if tile["relative_coord"][0] > -2 and tile["relative_coord"][0] < 2 and \
                            tile["relative_coord"][1] > -2 and tile["relative_coord"][1] < 2:
                            if tile["value"] == 1:
                                num += 1
                                values += 1
                            elif tile["value"] == 2:
                                num += 1
                                values += 2
                            elif tile["value"] == 3:
                                num += 1
                                values += 3
                    if num != 0:                                   
                        self.expected_growth = values / num
                    else:
                        self.expected_growth = 0
                #fitnesz számítás vége=========================================

                act = self.action_define(output) #a kimeneti array-ből szám készítés
                actstring = self.act_string_dict[act]# a számból üzenet konvertálás
                #actstring = ""
                
                # Akció JSON előállítása és elküldése
                #sendData(json.dumps({"command": "SetAction", "name": "Yellow", "payload": actstring}))
                sendData(json.dumps({"command": "SetAction", "name": "Pink", "payload": actstring}))
                


if __name__=="__main__":
    # Példányosított stratégia objektum
    
    random.seed(291) #random seed a tanításhoz
    my_agent = MyBacteriumAgent() #stratégia motor generálása
    load = True
    
    if load:#korábban tanított agent betöltése
        my_agent.load_model("model_2022-5-16__15-25-19.pkl")
    else:#ha nem betöltésről megy akkor kell mutánsokat generálni
        my_agent.create_mutants()
    

    if my_agent.training == False:
        my_agent.set_to_best()  # legjobb agent kiválasztása
        my_agent.set_best_five()# legjobb 5 agent kiválasztása

    papucs_allatka = MyStrategy(my_agent = my_agent)# stratégia objektum készítése
    

    # Socket kliens, melynek a szerver címét kell megadni (IP, port), illetve a callback függvényt, melynek szignatúrája a fenti
    # callback(fulljson, sendData)
    #client = SocketClient("10.0.0.113", 42069, papucs_allatka.processObservation)
    client = SocketClient("localhost", 42069, papucs_allatka.processObservation)

    # Kliens indítása
    client.start()
    # Kis szünet, hogy a kapcsolat felépülhessen, a start nem blockol, a kliens külső szálon fut
    time.sleep(0.1)
    # Regisztráció a megfelelő névvel
    #client.sendData(json.dumps({"command": "SetName", "name": "Yellow", "payload": None}))
    client.sendData(json.dumps({"command": "SetName", "name": "Pink", "payload": None}))
    #print('here')
    # Nincs blokkoló hívás, a főszál várakozó állapotba kerül, itt végrehajthatók egyéb műveletek a kliens automata működésétől függetlenül.