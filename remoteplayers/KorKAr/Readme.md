## Adaptív rendszerek modellezése 

Készítette: <br>
Kovács Richárd <br>
Károly Ármin <br>

### A program felépítése
Az általunk készített program három fő részre osztható. Az első rész az Example_Client_Main.py fájl kiegészítése a tanítás elvégzésére, a második a My_Agent_Model3.py fájl létrehozása, ami tartalmazza a tanítani kívánt rendszer elemeit, a harmadik, pedig a Player.py fájl kiegészítése, ami lehetővé teszi, hogy a program saját maga ellen játsszon, és így gyorsuljon a tanulás. A kódunk futtatásához a követketző kiegészítő csomagok szükségesek:
- Numpy
- Matplotlib

### My_Agent_Model3.py
Ez a fáj tartalmazza a modellünk alapvető felépítését és a tanítása során felhasznált függvényeket. A modell egy két rétegű neurális háló struktúra, aminek az optimalizálását bakteriális algoritmus szerint végezzük. A modellben definiálhatók az alapvető paraméterek, amivel meg lehet határozni a modell és a tanítás felépítését. Néhány fontos paraméter: egy generációban található egyedek száma, tanulási iterációk száma, mutálódási százalék, géninjektálási százalék, hogy a model éppen tanítva van vagy játszik stb.

A fájlban továbbá definiálva vannak a tanításhoz szükséges függvények, mint a model lementése, a model betöltése, a neurális háló definiálásához szükséges függvények, mutált mátrixok gyártása, a mutáns verziók közül a legjobb kiválasztása, fitnesz sorrend létrehozása, gén injektálás, tanítási adatok mentése, a jelenlegi egyed beállítása a legjobb fitnesszel rendelkező egyedre, valamint a legjobb 5 ilyen egyedre.
### Example_Client_Main.py
Ez a fájl a fő fájl, ahol maga a tanító függvények meghívása történik. Még a játék kezdete előtt itt van objektumosítva a modell, amibe lehetőség van betölteni egy már tanított modell mátrixait ekkor. 

Ezután indul a játék. A játék során minden lépésnél a bemenetek alapján létrehozásra kerül az éppen játszó agent számára készített bemenet, amit az agent megkap és meghatározza belőle a kimenetet, amiből a fő fájl a saját függvénye alapján elkészíti, hogy mi legyen az agent lépése. A lépés megadáson kívűl a játék során minden lépésnél fitnesz számítás is történik. A fitnesz értéke több elemből áll össze, az összeszedett kaják másfélszerese képezi az alapot, amihez hozzájönnek az egyes büntetések. Büntetés jár az agentnek, ha sokáig egy helyben áll, vagy egy régióban marad, valamint az is büntetésre kerül, ha a model nem szed össze kaját a közvetlen közelébe.
Ha egy játék végetért, akkor a tanítás állapotától függően többféle irányba mehet tovább a program, Azonban minden esetben megtörténik az agentek méretének és fitneszének mentése. 
- Ha az összes agent és ezek mutánsai is megvolt, és a legutolsó tanítási ciklusban van a program akkor at agent, valamint a futási adatok mentése után a program nem kér új restartot és a futás végetér.
- Hogyha még nem az utolsó tanítási ciklusban van a program, akkor a megtörténik a fitnesz sorrend készítés, a gén injektálás, majd a tanitási ciklus léptetése jön és a visszatérés az első agenthez, ahol mutánsokat generál a program. Végezetül a model mentése következik, majd a játék újraindul.
- Hogyha még nem ért le az utolsó agenthez a program, akkor az agent szám léptetése történik meg, majd az adott agenthez a mutánsok generálása. Végezetül a model mentése történik, majd újraindul a játék.
- Ha még a program az utolsó mutáns agenthez sem ért el, akkor csupán a mutáns agent számának léptetése történik meg mielőtt újraindul a játék. <br>

A sok mentésre azért van szükség, mert a Player.py program alapján generált játékosok így kaphatják meg a mutáns mátrixokat, amik alapján lépniük kell majd. Ezeket a mentéseket, amik a többi agentnek továbbítanak információt „dummy_model.pkl” névvel menti a program. A többi agent által számított saját fitneszeket is fájlokon keresztül tudja megkapni a fő program. Ezek a fitnesz fájlok „teszt2.pkl”, ”teszt3.pkl” és „teszt4.pkl” nevekkel vannak mentve.
### Player.py
A player.py programban augyanaz az modell felépítés van megvalósítva, mint a My_Agent_Model3.py programban, úgy, hogy ne legyen szükséges közvetlen kapcsolatra a My_Agent_Model3.py program függvényeivel. Erre azért volt szükség, hogy egy játékban az adott modell 4 egyedét is lehessen játszatni. Ezt úgy sikerült megoldani, hogy a főprogram folyamatosan update-el egy fájlt, amit a Player.py agentjei betöltenek. Ebben a fájlban találhatók a Player.py agent-jeinek a működéséhez szüksges mátrixok. Ezek az agent-ek pedig a saját játékuk során számolt fitnesz értékeket egy másik fájlba belementik, amit azután a főprogram tud megnyitni, hogy hozzáférjen ehhez az információhoz 
