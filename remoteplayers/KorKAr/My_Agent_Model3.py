import numpy as np
import random
import pickle
import time

class MyBacteriumAgent:

    def __init__(self):
        random.seed(281)#random seed a random számokhoz
        #belső változók
        self.agent_number = 20 #agent-ök száma
        self.mutated_agent_number = 8 #hány mutáns kászüljön egy agentről
        self.current_agent = 0 #hanyadik agent következik éppen
        self.current_mutated_agent = 0 #hanyadik mutáns aagent következik éppen

        self.input_num = 324 #bemenetek száma
        self.hidden_layer = 16 #rejtett paraméterek száma
        self.output_layer = 9 # kimeneti réteg

        self.agent_matrices = [] #az egyes agntek mátrixainak inicializálása
        for i in range(self.agent_number):
            self.agent_matrices.append([np.random.rand(self.hidden_layer,self.input_num)])
            self.agent_matrices[i].append(np.random.rand(self.output_layer,self.hidden_layer))
        self.mutated_matrices = [ [], [], [], [], [], [], [], []]#mutáns mátrixok helyének kijelölése

        self.learning_iterations = 10 #ennyi tanítási ciklus történik
        self.current_iteration = 0 #jelenlegi tanítási ciklus
        
        self.agent_fitnesses = [0] * self.agent_number #az agentek fitnesz értékei
        self.mutated_agent_fitnesses = [0] * self.mutated_agent_number #a mutáns agenteknek a fitnesz értékei
        self.fitness_order = [0] * self.agent_number #a fitneszek alapján a csöökenő sorrend
    

        self.mutation_percent = 10 #mutációs százalék mutánsok létrehozásakor
        self.injection_percent = 30 #génátadási százalék génátadáskor
        self.training = False #True #éppen tanítjuk az algoritmust vagy játszunk vele

        self.learning_log = [] #log adatok gyűjtésére
        self.fitness_log =[[], []] #log adatok gyűjtésére
        self.size_log = [] #méretek gyűjtésére

        self.best_five = [0,0,0,0,0] # a legjobb öt modell indexe
        
    
    def save_model(self, path=''):
        '''modell mentésére szolgáló függvény'''
        save_var = {'matrices' : self.agent_matrices,
                    'fitnesses' : self.agent_fitnesses,
                    'mutated_matrices' : self.mutated_matrices} #kimenteni kívánt változók
        
        if path == '':
            local_time = time.localtime(time.time())
            file_name = "model_" + str(local_time[0]) + '-' + str(local_time[1]) + '-' + str(local_time[2]) + '__' + str(local_time[3]) + '-' + str(local_time[4]) + '-' + str(local_time[5]) +'.pkl'
        else:
            file_name = path
        with open(file_name, 'wb') as file: #.pkl fájlba menti ki az adatokat
            pickle.dump(save_var, file)

    def load_model(self, path):
        '''modell adatainak betöltésére szolgáló függvény'''
        if path != '':
            with open(path, 'rb') as file:
                # Call load method to deserialze
                myvar = pickle.load(file)

        self.agent_matrices = myvar["matrices"]# a mentésben tárolt változók betöltése
        self.agent_fitnesses = myvar["fitnesses"]
        self.mutated_matrices = myvar["mutated_matrices"]
        #többi változó újra inicializálása
        self.current_iteration = 0
        self.current_agent = 0
        self.current_mutated_agent = 0
        self.learning_log = []
        self.fitness_log = [[], []]
  

    def sigmoid(self, x): 
        '''sigmoid aktivációs függvény a neurális hálóhoz'''
        z = np.exp(-x)
        sig = 1 / (1 + z)

        return sig

    def softmax(self, x):
        '''softmax aktiváció a neurális háló kimenetére'''
        f_x = np.exp(x) / np.sum(np.exp(x))
        return f_x

    def forward_step(self, input):
        '''bemenet beadása az aktuális indexű agentnek'''
        output = np.dot(self.agent_matrices[self.current_agent][0], input) #1. layer
        output = self.sigmoid(output)# 1. aktiváció
        output = np.dot(self.agent_matrices[self.current_agent][1] , output)#2. layer
        output = self.softmax(output)# 2. aktiváció
        return output

    def forward_mutated_step(self, input):
        '''bemenet beadása az aktuális indexű mutáns agentnek'''
        output = np.dot(self.mutated_matrices[self.current_mutated_agent][0], input)
        output = self.sigmoid(output)
        output = np.dot(self.mutated_matrices[self.current_mutated_agent][1] , output)
        output = self.softmax(output)
        return output

    def forward_play_step(self, input):
        '''játék esetén a bemenet beadása a legjobb öt agentnek - fitnesz aapján'''
        output_list = []
        for i in range(5):
            output = np.dot(self.agent_matrices[self.best_five[i]][0], input)
            output = self.sigmoid(output)
            output = np.dot(self.agent_matrices[self.best_five[i]][1] , output)
            output = self.softmax(output)
            output_list.append(output)
        return output_list #kimenet a kimenetek listába szedve

    
    def create_mutants(self,):
        '''mutánsok generálása az aktuális agenthez - összesen 7'''
        self.mutated_matrices = [ [], [], [], [], [], [], [], []]# tároló újra inicializálása
        self.mutated_matrices[0].append(self.agent_matrices[self.current_agent][0])#eredeti agent mátrixok hozzáadása
        self.mutated_matrices[0].append(self.agent_matrices[self.current_agent][1])

        # a mutácisó százelék alapján a mutálni kívánt változók száma
        variables = self.input_num * self.hidden_layer + self.hidden_layer * self.output_layer#összes változó
        mut_variables = int((self.mutation_percent/100) * variables)# a mutálni kívánt változók száma

        second_percent = (self.hidden_layer * self.output_layer)/ variables# a másosik réteg mérete az elsőhöz képest
        sec_mut_variables = int(mut_variables*second_percent)# a második layerben változtatni kívánt változók száma
        fir_mut_variables = mut_variables - sec_mut_variables# az első layerben változtatni kívánt változók száma
        if sec_mut_variables < 1:
            sec_mut_variables = 1

        # a hét mutáns mátrix legenerálása
        for i in range(7):
            tmp_matrix1 = np.copy(self.agent_matrices[self.current_agent][0]) #eredeti másolása
            tmp_matrix2 = np.copy(self.agent_matrices[self.current_agent][1])
            for j in range(fir_mut_variables):#első layer esetében
                first_index = random.randint(0,self.hidden_layer-1) #random lokáció
                second_index = random.randint(0,self.input_num-1)
                tmp_matrix1[first_index, second_index] = random.random()#adott lokáción az érték random változtatása
                
            for j in range(sec_mut_variables):#második layer esetében
                first_index = random.randint(0,self.output_layer-1)
                second_index = random.randint(0,self.hidden_layer-1)
                tmp_matrix2[first_index, second_index] = random.random()#(random.uniform(0.0, 2.0))**2 #random.random()
            
            self.mutated_matrices[i+1].append(tmp_matrix1)# amutáns mátrixok hozzáadása a tárolóhoz
            self.mutated_matrices[i+1].append(tmp_matrix2)

    def find_best_version(self):
        '''A mutáns mátrixok közül kiválasztja a legjobb fitnesszel rendelkezőt és az őrzi meg'''
        val_max = max(self.mutated_agent_fitnesses)
        max_index = self.mutated_agent_fitnesses.index(val_max)
        self.agent_matrices[self.current_agent][0] = np.copy(self.mutated_matrices[max_index][0]) #az eredeti mátrixok átírása a legjobbra
        self.agent_matrices[self.current_agent][1] = np.copy(self.mutated_matrices[max_index][1])
        
        self.mutated_matrices = [ [], [], [], [], [], [], [], []]
        self.agent_fitnesses[self.current_agent] = val_max
        self.current_mutated_agent = 0


    def make_fitness_order(self):
        '''az adott agent fitneszek alapján az agentek sorban elfoglalt helyének meghatározása'''
        tmp_list = self.agent_fitnesses.copy() #a fitneszek lemásolása
        for i in range(self.agent_number):
            max_val = max(tmp_list)#max érték megkeresése
            max_index = tmp_list.index(max_val)#a max érték indexének megkeresése
            self.fitness_order[i] = max_index #az index behelyezése a tárolóba
            tmp_list[max_index] = -10000 #a már kiválasztott érték kicsire állítása, hogy többet ne válasszák ki
        '''
        for i in range(self.agent_number):
            index = self.fitness_order[i] #index - > nagyság szerinti sorrend
            percent = self.agent_fitnesses[index]/sum(self.agent_fitnesses)
            self.fitness_percent[index] = percent #eredeti sorrendben vannak a percentek
        '''
    def gene_injection(self):
        ''' a gén injektálás megvalósítása'''
        good_matrices_indexes = [] #jó mátrixok
        bad_matrices_indexes = [] #rossz mátrixpk
        for i in range(self.agent_number):
            index = self.fitness_order[i]#csökkenő sorrend használata fitnesz alapján
            if i < 10:
                good_matrices_indexes.append(index)#első 10 jó 
            else:
                bad_matrices_indexes.append(index)# második 10 rossz

        # a gén injektálás százelék alapján a kicserélendő értékek mennyiséégnek kiszámítása
        variables = self.input_num * self.hidden_layer + self.hidden_layer * self.output_layer
        mut_variables = int((self.injection_percent/100) * variables)

        second_percent = (self.hidden_layer * self.output_layer)/ variables
        sec_mut_variables = int(mut_variables*second_percent) #második layerben kicserélendő értékek száma
        fir_mut_variables = mut_variables - sec_mut_variables # első mátrixban kicserélendő értékek száma
        if sec_mut_variables < 1:
            sec_mut_variables = 1

        for i in range(int(self.agent_number/2)): #10-szer

            for j in range(fir_mut_variables):# az első layernél
                first_index = random.randint(0,self.hidden_layer-1) #random indexek választása
                second_index = random.randint(0,self.input_num-1)
                #értékek átadása
                self.agent_matrices[bad_matrices_indexes[i]][0][first_index, second_index] = self.agent_matrices[good_matrices_indexes[i]][0][first_index, second_index]
            for j in range(sec_mut_variables):# amásodik layernél
                first_index = random.randint(0,self.output_layer-1)
                second_index = random.randint(0,self.hidden_layer-1)
                #értkek átadása
                self.agent_matrices[bad_matrices_indexes[i]][1][first_index, second_index] = self.agent_matrices[good_matrices_indexes[i]][1][first_index, second_index] 

        self.learning_log.append( #agent fitneszek elmentése string formában
            (  
                str(self.agent_fitnesses[0]) + '  ' +
                str(self.agent_fitnesses[1]) + '  ' +
                str(self.agent_fitnesses[2]) + '  ' +
                str(self.agent_fitnesses[3]) + '  ' +
                str(self.agent_fitnesses[4]) + '  ' + 
                str(self.agent_fitnesses[5]) + '  ' +
                str(self.agent_fitnesses[6]) + '  ' +
                str(self.agent_fitnesses[7]) + '  ' +
                str(self.agent_fitnesses[8]) + '  ' +
                str(self.agent_fitnesses[9]) + '  ' +
                str(self.agent_fitnesses[10]) + '  ' + '\n' +
                str(self.agent_fitnesses[11]) + '  ' + 
                str(self.agent_fitnesses[12]) + '  ' +
                str(self.agent_fitnesses[13]) + '  ' + 
                str(self.agent_fitnesses[14]) + '  ' +
                str(self.agent_fitnesses[15]) + '  ' +
                str(self.agent_fitnesses[16]) + '  ' +
                str(self.agent_fitnesses[17]) + '  ' +
                str(self.agent_fitnesses[18]) + '  ' +
                str(self.agent_fitnesses[19]))   
        )
        sum_fit1 = 0 #legjobb 10 fitnesz átlagának változóba mentése
        for i in range(10):
            index = self.fitness_order[i]
            sum_fit1 += self.agent_fitnesses[index]
        
        sum_fit2 = 0 #legjobb 5 fitnesz átlagának változóba mentése
        for i in range(5):
            index = self.fitness_order[i]
            sum_fit2 += self.agent_fitnesses[index]

        self.fitness_log[0].append(sum_fit1/10) #belső változóban tárolás
        self.fitness_log[1].append(sum_fit2/5)

    def reset(self):
        pass

    def savelogs(self):
        '''nehány adat lementése a tanításról'''
        save_var = {'sizes' : self.size_log,  #agent méretek
                    'fitnesses' : self.fitness_log, #agent fitneszek
                   }
        local_time = time.localtime(time.time())
        file_name = "logs_" + str(local_time[0]) + '-' + str(local_time[1]) + '-' + str(local_time[2]) + '__' + str(local_time[3]) + '-' + str(local_time[4]) + '-' + str(local_time[5]) +'.pkl'
        
        with open(file_name, 'wb') as file:
            pickle.dump(save_var, file) #.pkl fájlba lesznek mentve

    def set_to_best(self):
        '''a legjobb agent kiválasztása'''
        val_max = max(self.agent_fitnesses)
        max_index = self.agent_fitnesses.index(val_max)
        self.current_agent = max_index #az aktuális agent beállítása legjobbra

    def set_best_five(self):
        '''az öt legjobb agent megkeresése'''
        dummy_fitnesses = self.agent_fitnesses.copy()
        for i in range(5):
            val_max = max(dummy_fitnesses) #ahol a legjobb a fitnesz
            max_index = dummy_fitnesses.index(val_max) # alegjobb fitnesz indexe
            self.best_five[i] = max_index #a legjobb fitnesz indexének hozzásadása az index listához
            dummy_fitnesses[max_index] = -10000# alegjobb fitnesz kicsire állítása, hogy a következő iterációban ne legyen figyelmbe véve
        
    
    