import numpy as np
import json
import random
import pickle

class RemotePlayerStrategy:
    def __init__(self, **kwargs):
        self.nextAction = "0"
        self.sendData = kwargs["sender"]
        self.getData = kwargs["getter"]
        self.name = kwargs["name"]

    def setObservations(self, ownObject, fieldDict):
        self.nextAction = "0"
        self.sendData(json.dumps({"type":"gameData", "payload":fieldDict}), ownObject.name)

    def getNextAction(self):
        newaction = self.getData(self.name)
        if newaction is None:
            return self.nextAction
        else:
            return newaction

    def reset(self):
        
        self.nextAction = "0"
        #data = "something"
        #while data is not None:
        #    data = self.getData(self.name)

class DummyStrategy:
    def __init__(self, **kwargs):
        self.nextAction = "0"

    def setObservations(self, ownObject, fieldDict):
        ownObject.active=False

    def getNextAction(self):
        return "0"

    def reset(self):
        pass

class RandBotStrategy:
    def __init__(self, **kwargs):
        self.nextAction = 0

    def setObservations(self, ownObject, fieldDict):
        pass

    def getNextAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action

    def reset(self):
        self.nextAction = "0"

class NaiveStrategy:
    def __init__(self, **kwargs):
        self.nextAction = "0"
        self.oldpos = None
        self.oldcounter = 0

    def getRandomAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action

    def setObservations(self, ownObject, fieldDict):
        if self.oldpos is not None:
            if tuple(self.oldpos) == tuple(ownObject.pos):
                self.oldcounter += 1

        self.oldpos = ownObject.pos.copy()

        values = np.array([field["value"] for field in fieldDict["vision"]])
        values[values > 3] = 0
        values[values < 0] = 0
        if np.max(values) == 0 or self.oldcounter >= 3:
            self.nextAction = self.getRandomAction()
            self.oldcounter = 0
        else:
            idx = np.argmax(values)
            actstring = ""
            for i in range(2):
                if fieldDict["vision"][idx]["relative_coord"][i] == 0:
                    actstring += "0"
                elif fieldDict["vision"][idx]["relative_coord"][i] > 0:
                    actstring += "+"
                elif fieldDict["vision"][idx]["relative_coord"][i] < 0:
                    actstring += "-"

            self.nextAction = actstring

    def getNextAction(self):
        return self.nextAction

    def reset(self):
        self.nextAction = "0"

class NaiveHunterStrategy:
    def __init__(self, **kwargs):
        self.nextAction = "0"
        self.oldpos = None
        self.oldcounter = 0
        #print('inicializálva')

    def getRandomAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action

    def setObservations(self, ownObject, fieldDict):
        if self.oldpos is not None:
            if tuple(self.oldpos) == tuple(ownObject.pos):
                self.oldcounter += 1
            else:
                self.oldcounter = 0
        if ownObject.active:
            self.oldpos = ownObject.pos.copy()

        vals = []
        for field in fieldDict["vision"]:
            if field["player"] is not None:
                if tuple(field["relative_coord"]) == (0, 0):
                    if 0 < field["value"] <= 3:
                        vals.append(field["value"])
                    elif field["value"] == 9:
                        vals.append(-1)
                    else:
                        vals.append(0)
                elif field["player"]["size"] * 1.1 < ownObject.size:
                    vals.append(field["player"]["size"])
                else:
                    vals.append(-1)
            else:
                if 0 < field["value"] <= 3:
                    vals.append(field["value"])
                elif field["value"] == 9:
                    vals.append(-1)
                else:
                    vals.append(0)

        values = np.array(vals)
        # print(values, fieldDict["vision"][np.argmax(values)]["relative_coord"], values.max())
        if np.max(values) <= 0 or self.oldcounter >= 3:
            self.nextAction = self.getRandomAction()
            self.oldcounter = 0
        else:
            idx = np.argmax(values)
            actstring = ""
            for i in range(2):
                if fieldDict["vision"][idx]["relative_coord"][i] == 0:
                    actstring += "0"
                elif fieldDict["vision"][idx]["relative_coord"][i] > 0:
                    actstring += "+"
                elif fieldDict["vision"][idx]["relative_coord"][i] < 0:
                    actstring += "-"

            self.nextAction = actstring

    def getNextAction(self):
        #print('inicializálva')
        return self.nextAction

    def reset(self):
        self.nextAction = "0"

class RemotePlayerCopy: #az eredet class másolata 4 játékos módhoz
    def __init__(self, **kwargs):
        #belső változók ~ ugyanaz mint az eredeti osztályban
        self.nextAction = "0"
        self.agent_matrices = []
        self.vision = np.zeros((81,4))
        self.input = 0
        self.current_agent = 0
        self.act_string_dict = {
            0: '00',
            1: '0+',
            2: '++',
            3: '+0',
            4: '+-',
            5: '0-',
            6: '--',
            7: '-0',
            8: '-+',
            9: '00'
        }
        #print('inicializálva')
        self.load_model('dummy_model.pkl')# ebben a mentésben vannak a szükséges mátrixok, ezt a fő remote player minden játék után updateli

        #fitnesz számításhoz a változók
        self.previous_location = [0,0]
        self.previous_dynamic_location = [0,0]
        self.location = [0,0]
        self.static_location_counter = 0
        self.dynamic_location_counter = 0

        self.previous_size = 5
        self.size = 5
        self.expected_growth = 0
        self.fitness = 5

        self.tick = 0
        self.file_name = "teszt" # iylen névvel fogja majd a player elmenteni a számolt fitneszét amit majd a remote player visszatölt

    def sigmoid(self, x): #sigmoid függvény
        z = np.exp(-x)
        sig = 1 / (1 + z)

        return sig

    def softmax(self, x): #softmax függvény
        f_x = np.exp(x) / np.sum(np.exp(x))
        return f_x

    def setObservations(self, ownObject, fieldDict): #bemenet generálása ~ ugyanaz mint a remote playernél
        for i in range(len(fieldDict["vision"])):
            if fieldDict["vision"][i]["player"] == None:
                self.vision[i,:] = np.array([   (fieldDict["vision"][i]["relative_coord"][0] + 4)/8,
                                                        (fieldDict["vision"][i]["relative_coord"][1] + 4)/8,
                                                        fieldDict["vision"][i]["value"] / 9,
                                                        0
                                                        ])
            else:                                            
                self.vision[i,:] = np.array([   (fieldDict["vision"][i]["relative_coord"][0] + 4)/8,
                                                    (fieldDict["vision"][i]["relative_coord"][1] + 4)/8,
                                                    fieldDict["vision"][i]["value"] / 9,
                                                    fieldDict["vision"][i]["player"]["size"] / 50
                                                    ])
                                                
        self.input = self.vision.flatten(order = 'C')

        #fitnesz számítás:, ugyanaz mint az eredeti agentnél
        self.size = ownObject.size
        self.pos = ownObject.pos

        plus = (self.size - self.previous_size)*1.5
        minus = 0
        if plus < self.expected_growth:
            minus += self.expected_growth - plus

        self.location = ownObject.pos
        if self.location[0] == self.previous_location[0] and self.location[1] == self.previous_location[1]:
            self.static_location_counter += 1
        else:
            self.static_location_counter = 0
        
        if self.static_location_counter > 3:
            minus += 1

        if  self.location[0] > self.previous_dynamic_location[0] - 3 and self.location[0] < self.previous_dynamic_location[0] + 3 and \
            self.location[1] > self.previous_dynamic_location[1] - 3 and self.location[1] < self.previous_dynamic_location[1] + 3:
            self.dynamic_location_counter += 1
        else:
            self.dynamic_location_counter = 0
            self.previous_dynamic_location = self.location

        if self.dynamic_location_counter > 20:
            minus += 1

        self.fitness = self.fitness + plus - minus

        self.previous_location = self.location
        self.previous_size = self.size
        num = 0
        values = 0
        for tile in fieldDict["vision"]:
            if tile["relative_coord"][0] > -2 and tile["relative_coord"][0] < 2 and \
                tile["relative_coord"][1] > -2 and tile["relative_coord"][1] < 2:
                if tile["value"] == 1:
                    num += 1
                    values += 1
                elif tile["value"] == 2:
                    num += 1
                    values += 2
                elif tile["value"] == 3:
                    num += 1
                    values += 3
        if num != 0:                                   
            self.expected_growth = values / num
        else:
            self.expected_growth = 0
        #fitness számítás vége 
        self.tick = fieldDict['tick'] # tick elmentése

    def forward_step(self, input):#bemenet feldolgozása
        output = np.dot(self.agent_matrices[self.current_agent][0], input)
        output = self.sigmoid(output)
        output = np.dot(self.agent_matrices[self.current_agent][1] , output)
        output = self.softmax(output)
        return output


    def action_define(self, output): #százalékok alapján eldönteni, hogy mi legyen a lépés
        if np.max(output) > 0.5:
            max = np.argmax(output, axis=0)
            return int(max)
        else:
            rand = random.random()
            #max= np.argmax(output, axis=0)
            for i in range(9):
                max = np.argmax(output, axis=0)
                rand = rand - output[max]
                if rand <= 0:
                    return int(max)
                else:
                    output[max] = -1

            return 0

    def getNextAction(self):
        self.nextAction = self.act_string_dict[self.action_define(self.forward_step(self.input))]
        if self.tick > 398: #ha meghalad egy adott értékek a tick a fitnesz értékek ki kell menteni
            self.save_fitness()
        return self.nextAction
    
    def reset(self):
        
        #változók egy részének újra inicializálása
        # -> fitnesz változók
        self.previous_location = [0,0]
        self.previous_dynamic_location = [0,0]
        self.location = [0,0]
        self.static_location_counter = 0
        self.dynamic_location_counter = 0

        self.previous_size = 5
        self.size = 5
        self.expected_growth = 0
        self.fitness = 5

        self.tick = 0

        #agent léptetése
        self.nextAction = "0"
        self.current_agent += 4 #1
        if self.current_agent > 8:
            self.current_agent = self.current_agent - 8 #0
            self.load_model('dummy_model.pkl')#mátrixok betöltése ha az agent száma nagyobb egy adott értéknél
            print('loaded')


    def load_model(self, path):
        #modell betöltése
        if path != '':
            with open(path, 'rb') as file:
                myvar = pickle.load(file)

            self.agent_matrices = myvar["mutated_matrices"]
            

    def save_fitness(self):
        #fitnesz elmentése, hogy a főprogram be tudja tölteni
        save_var = {'fitness' : self.fitness}
        file_name = self.file_name
        with open(file_name, 'wb') as file:
            pickle.dump(save_var, file)
        

class Player:
    strategies = {"randombot": RandBotStrategy, "naivebot": NaiveStrategy, "naivehunterbot": NaiveHunterStrategy,
                  "remoteplayer": RemotePlayerStrategy, "dummy":DummyStrategy, "remotecopy": RemotePlayerCopy}

    def __init__(self, name, playerType, startingSize, **kwargs):
        self.name = name
        self.playerType = playerType
        self.pos = np.zeros((2,))
        self.size = startingSize
        kwargs["name"] = name
        self.strategy = Player.strategies[playerType](**kwargs)
        #===============================
        #az egyes stratégiáknak be kell állítani, hogy saját változóik legyenk ->agent szám, fitnesz fájl név
        if name == "Teszt2":
            self.strategy.current_agent = 1
            self.strategy.file_name = "teszt2.pkl"
        if name == "Teszt3":
            self.strategy.current_agent = 2
            self.strategy.file_name = "teszt3.pkl"
        if name == "Teszt4":
            self.strategy.current_agent = 3
            self.strategy.file_name = "teszt4.pkl"
        #===============================
        self.active = True

    def die(self):
        self.active = False
        print(self.name + " died!")

    def reset(self):
        #print('reset')
        self.active = True
        self.strategy.reset() #reset esetén legyen meghívva a playerek reset függvénye is

