# Remote players 

csapat: Pythronika <br>
tagok: Csóti Zoltán és Havriló Balázs <br>
link: [repo](https://gitlab.com/zcsti/pythronika-adapitv-rendszerek-modellezese-hazi-feladat)
tested: 

csapat: GALHB <br>
tagok: Giczi Alexandra Laura és Horváth Botond <br>
link: [repo](https://github.com/botondi99/AdaptIO)
tested: 

csapat: HoKTaM <br>
tagok: Honti Kristóf és Tatai Mihály  <br>
link: [repo](https://github.com/evgyom/adaptivHF) <br>
tested: 

csapat: Z&Z <br>
tagok: Csuja Zoltán és Dombi Zoltán  <br>
link: [repo](https://gitlab.com/dorton007/adaptivegame) <br>
tested: 

csapat: DeGeMeLeT <br>
tagok: Demeter Gergő és Menyhárt Levente Tibor  <br>
link: [repo](https://gitlab.com/microlab1/public-projects/adaptivegame/-/tree/main/remoteplayers/DeGeMeLeT) <br>
original repo: [repo](https://github.com/menyhartlevi/AdaptIO) <br>
tested: OK

csapat: CsoNa <br>
tagok: Csóti Ádám és Nagy Levente  <br>
link: [repo](https://gitlab.com/microlab1/public-projects/adaptivegame/-/tree/main/remoteplayers/CsoNa) <br>
note: ppt infoit áttenni readmebe
tested: OK

csapat: LANA <br>
tagok: László András és Németh Ákos <br>
link: [repo](https://gitlab.com/microlab1/public-projects/adaptivegame/-/tree/main/remoteplayers/LANA) <br>
tested: OK

csapat: OtOT <br>
tagok: Ottmár Ádám és Orbázi Tibor <br>
link: [repo](https://github.com/ottadam/adaptIO_projekt) <br>
tested: 

csapat:  KoRKAr <br>
tagok: Kovács Richárd és Károly Ármin <br>
link:  [repo](https://gitlab.com/microlab1/public-projects/adaptivegame/-/tree/main/remoteplayers/KorKAr) <br>
tested: OK

csapat: SziGe <br>
tagok: Geréb Ábris és Szima Zsolt <br>
link:  [repo](https://gitlab.com/microlab1/public-projects/adaptivegame/-/tree/main/remoteplayers/SziGe) <br>
tested: OK

csapat: MeMo  <br>
tagok: Meskó Patrik és Morvai Balázs <br>
link:  [repo](https://gitlab.com/microlab1/public-projects/adaptivegame/-/tree/main/remoteplayers/MeMo) <br>
note: ipython <br>
tested: OK

csapat: ViDa  <br>
tagok: Villányi Dáriusz <br>
link:  [repo](https://gitlab.com/microlab1/public-projects/adaptivegame/-/tree/main/remoteplayers/ViDa) <br>
note: Readme linkekekt frissíteni
tested: OK

csapat:  ZsomBEAST <br>
tagok: <br>
link:  [repo](https://github.com/ZsomBEAST/Adapt-v-Rendszerek-modellez-se?fbclid=IwAR0wGo5L_EaehMtksnSq5_-xjgWlJyCZxJuZ3VEFyXaxyzG22PZxt4kkrAc) <br>
note: new mapok <br>
tested: 

csapat:  NEJF <br>
tagok: Nemes Enikkő és Jakab Farkas<br>
link:  [repo]() <br>
note: Teamsen beküldve <br>
tested:

csapat:  TeamFinland <br>
tagok: <br>
link: <br>
videos: [video](https://drive.google.com/drive/folders/1HbCm7hkTj-hROEcaujQxwsJjq-AwgFEK) <br>
note: Teamsen beküldve <br>
tested: 

csapat:KAMI
tagok: Kántor Balázs és Kristófi János Mihály<br>
link:  [repo](https://gitlab.com/kantorb/827_adaptio) <br>
tested:
