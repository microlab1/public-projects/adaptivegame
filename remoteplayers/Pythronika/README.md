# Adaptív rendszerek modellezése házi feladat Pythronika csapat
Csóti Zoltán, Havriló Balázs

# Stratégia áttekintése

* A tárgyhoz elkészített játékfelületben egy agent stratégiájának előállítása.
* A feladat során a betanításhoz genetikus algoritmust használtunk.
* Az agent a változó környezetben a legnagyobb fitneszű stratégiát fogja valószínűleg választani, amit 3 körig alkalmaz.
* Felhasznált stratégiák: NaiveHunterBot, NaiveBot, saját random stratégia.

# Alapötlet

* Az agent viselkedés fagráfszerűen tagolható. A fő állapotváltozó a szándékosság. Ha ez alacsony, akkor a random stratégiához húz a rendszer. Szándékos viselkedés esetén az agent agresszivitását vizsgáljuk, ami ha nagy, akkor hunter, ha alacsony, akkor naive viselkedésre hajlamosabb.

![kép](https://user-images.githubusercontent.com/82893067/170893369-67d7bed0-5851-495f-a8cb-83a0832df494.png)

* A genetikus algoritmusban az egyedek tulajdonságai ez a két mutató lesz és adott vizsgálati körben ezek fitnesz értékei alapján lesz kiválasztva a megfelelő stratégia.
* Az algoritmus során állandó egyedszámot tartunk, a szülőket keresztezés után elvetjük.

![kép](https://user-images.githubusercontent.com/82893067/170893382-d1c2e829-e0a9-448e-9d3c-7c2597c3e2e6.png)

# Fitnesz
* Meghatároztunk a szándékossággal és az agresszivitással kapcsolatos fitneszfüggvényeket is.
* Az utóbbi ha volt megehető ellenfél az előző körökben a látótérben, akkor az aggresszivitást jutalmazta a passzív viselkedés helyett. A frissebb információ nagyobb súllyal bírt.

![kép](https://user-images.githubusercontent.com/82893067/170893414-c0e3150a-16ac-4515-a9d8-491c0b7b4433.png)

* A szándékosság meghatározásához több szempontrendszert is figyelembe vettünk a függvényeknél. Először is vizsgáltuk azt, hogy az agent mennyire ragad be az előző stratégiáknál. Ha az előző körökben volt olyan, hogy nem lépett, a szándékosság helyett a randomitást jutalmaztuk. A frissebb információk fontosabbak voltak.

![kép](https://user-images.githubusercontent.com/82893067/170893449-de0c7fd4-5926-4286-944e-33364a8208e3.png)

* Ezen felül vizsgáltuk a látómezőben lévő ételmezők számát. Nagyobb ételszám nagyobb szándékosságot jutalmazott. Végül figyeltük az ügynökkel szomszédos mezőkben elhelyezkedő falak számát. A magas falszám  véletlenszerű stratégiát részesítette előnyben.
* A véletlenszerű stratégiánk úgy lett kidolgozva, hogy csak olyan mezők között válasszon, amelyekre képes lépni, így néhány kör ebből a stratégiából nagy valószínűséggel ki tudja vezetni az ügynököt egy sarokból (falak, beragadás). Ha nem volt elég étel a látótérben, akkor jobbnak láttuk a véletlenszerű lépegetést.

# Kiválasztás, keresztezés, mutáció

* A szülők kiválasztása a fitneszek alapján rulettkerék algoritmussal történik.
* A keresztezés valós számok között van alkalmazva, így az (α,1-α) elven működő összeg algoritmust választottuk
* Mutációnak egy a konstans keretek között változó számmal való összeg lehetőségét adtuk meg.
* A szélső értékek esetén a túlcsordulást levágtuk

# Logfile kezelés

* Az adatok tickek közötti kommunikációjához logfile-ok alkalmazását választottuk.
* A fitnesz értékek számításához le kellett mentenünk az előző körökben jegyzett megehető ellenfelek számát és a helyben maradásokat, aminek maghatározásához tick-enként az abszolút pozíciót is le kellett menteni
* Egy második file-ba mentettük a mindenkori populációt és egy harmadikba az alkalmazott stratégiákat.

![kép](https://user-images.githubusercontent.com/82893067/170893530-4cb3ef0a-f197-4eb2-b202-76d1ef608fce.png)

# Összefoglalás

* Összességében elmondható, hogy a követett vegyes stratégia nem volt hajlamos beragadásra, így hosszabb távon általában jobb eredmény ért el a bonyolultabb pályákon, mint az előre megírt stratégiák.
* Nyitott és ételben szegény pályán nem feltétlenül előzi meg a felhasznált stratégiákat.
* Fejlesztési lehetőségként például kiegészíthető az algoritmus egy „menekülő” stratégiával, mely a veszélyes ellenféllel ellenkező irányba a választható mezők valamelyikére lép.
 
# Dependencies

* numpy
* random
* math
* pygame
* Verzió: Python 3.8.12.
