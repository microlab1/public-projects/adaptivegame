# AdaptIO
Készítette:	Ottmár Ádám	és	Orbázi Tibor

# Reinforcement learning

* Felügyelet nélkül, tapasztalati úton tanul a környezetből kapott visszajelzések alapján
* A jó lépésekért jutalmat kap, a rosszakért büntetést kaphat
* Később a környezet általa ismert állapotából próbálja kiválasztani azt a lépést ami a legnagyobb jutalomhoz vezet
* Az AlphaGo és a Google DeepMind mesterséges intelligenciák is ilyen módszereken alapulnak

![kép](https://user-images.githubusercontent.com/82893067/170893817-a1f52eb0-7dbe-44b5-8557-94b232a1252c.png)

![kép](https://user-images.githubusercontent.com/82893067/170893820-322293b0-d95f-4a7f-9a4d-876631c9d748.png)

# Reinforcement learning típusai

* Q-learning 
* Deep Q-learning (DQN)
* Policy Gradient
* Deep Deterministic Policy Gradient (DDPG)

![kép](https://user-images.githubusercontent.com/82893067/170893853-1cc2dbce-ac32-4e8e-b501-c3c03e26f50f.png)

![kép](https://user-images.githubusercontent.com/82893067/170893856-aa66b31c-60f9-4673-849f-0ab15d962a1e.png)

![kép](https://user-images.githubusercontent.com/82893067/170893859-f5ef9630-7d2f-444f-bc66-7b7591eb814f.png)

# Q-learning

1. Q-tábla létrehozása (nxm; n: állapotok száma, m: akciók száma)
2. Legmagasabb Q értékű akció választása a táblázatból az adott állapotban, egyenlőség esetén random választás
3. Akció végrehajtása
4. Új állapot és kapott jutalom vizsgálata, Q-tábla frissítése a Bellman egyenlet alapján
5. Ismétlés a tanulás végéig
Epslilon greedy algoritmussal ötvözhető így a tanulás elején több a felfedezés

![kép](https://user-images.githubusercontent.com/82893067/170893914-cbcf387b-3036-44a9-9a5d-27976a3d5749.png)

![kép](https://user-images.githubusercontent.com/82893067/170893920-b5c4565b-eb28-444f-8ed2-e1c71976ff9a.png)

# Deep Q-learning

* Ha nagy az állapotok és akciók száma a Q-tábla nehezen és lassan számítható ki
* A táblázat helyett egy neurális háló becsüli meg az adott állapot és akció páros Q értékét. A minél pontosabb becslést kell megtanulnia a hálónak
* Az összetartozó állapot-akció-jutalom csoportokat egy memóriába tároljuk később ebből válogatva tanul a háló. (Off-policy)
* A tanulás egyenletesebb, ha az adott állapotban elérhető maximális Q értéket egy másik un. target háló becsüli meg. Az alap háló súlyai bizonyos időközönként átmásolódnak a target hálóba, frissítve azt

![kép](https://user-images.githubusercontent.com/82893067/170893884-28505cba-6686-4840-bada-fb5f69f23410.png)

# Mit lát a neurális háló?

* A bemenet egy 410 elemű vektor
* [0:1] - ágens pozíciója a pályán 0-1 intervallumra normálva
* [2] - ágens mérete 0-1 közé normálva
* [3:7] - látómező első pixelének értéke egy 5 elemű One-Hot vektorban tárolva (fal, 1 kaja, 2 kaja, 3 kaja, üres)
* ….
* [408] - ellenség pozíciója a látómezőben 0-1 közé normálva
* [409] - ellenség mérete 0-1 intervallumra normálva

![kép](https://user-images.githubusercontent.com/82893067/170893966-c5995b91-92e7-4c8f-858c-5811399cf1d6.png)

# Neurális háló felépítése

![kép](https://user-images.githubusercontent.com/82893067/170893972-5b597f14-2a93-4da2-942a-a862dba0e380.png)

* Bementi réteg 410 elemű vektor
* 1. rejtett réteg 120 neuron (ReLu)
* 2. rejtett réteg 60 neuron (ReLu)
* Kimeneti réteg 9 neuron (Linear)

# Tanítás menete

* A játék közben először minden lépést, később csak egy határértéknél magasabb jutalmú lépést lement egy replay memory-ba (state, action, reward)
* 5 lépésenként a replay memory-ból véletlenszerűen kiválasztott 100 elemen tanul a neurális háló (állapot-akció-jutalom)
* 100 lépésenként frissül a target háló, megkapja az újonnan tanított súlyokat
* Közben folyamatosan fut a játék, és újabb elemek kerülnek a replay memoryba
* Epsilon Greedy -  a tanulás teljesen randomizált lépésekkel indul, majd fokozatosan egyre többször lép a neurális háló (NaiveHunterBot)
* Egyszerű térképen kezdtük a sarokban termelődő kaja mellett, majd nehezebb beállítások mellett folytattuk, a végén véletlenszerűen választott pályákon.

![kép](https://user-images.githubusercontent.com/82893067/170894006-c0053dd2-6d56-4b3f-a164-fe406e8f8af9.png)

![kép](https://user-images.githubusercontent.com/82893067/170894008-9785fd66-0269-4885-a22f-7c2598417df7.png)

# Eredmények, tapasztalatok

* Véletlenszerű pályákon futtatva, random botok ellen átlagosan 70 pontot képes elérni egy játékban, azonban  ingadozó a teljesítménye (10-250 pont)
* A tanulás hatékonyabb, ha nem minden elemet mentünk a replay memóriába, hanem csak jó elemeket
* Fontos jól meghatározni a tanítás és a target háló frissítésének gyakoriságát, a tanuló minta méretét
* Az ágens gyakran beragad egy helyben vagy ciklikus mozgásokban, egy-egy véletlenszerű lépés képes kizökkenteni, és továbbmegy
* Ha különálló lépések helyett lépések sorozatát mentenénk le, visszavezetett jutalom értékekkel és ezeken is tanítanánk, feltehetően jobb végeredményt produkálna

![kép](https://user-images.githubusercontent.com/82893067/170894030-df3dc2ed-966a-4390-8882-62d5a755185e.png)

# Dependencies

* matplotlib 3.5.1
* tensorflow 2.6.0
* keras 2.8.0
* numpy 1.21.5
* collections.deque

# Credits

* Orbázi Tibor (orbazi.tibor@edu.bme.hu)
* Ottmár Ádám (ottmar.adam2000@edu.bme.hu)
