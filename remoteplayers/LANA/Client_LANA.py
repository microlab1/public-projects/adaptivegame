#encoding: utf-8

#==============================================================
# Adds the src map to the path, so imports can work properly
#==============================================================
import pathlib
import sys

current_dir = pathlib.Path().resolve()
src_dir     = current_dir.parent.parent.joinpath("src")
path = str(src_dir).replace("/", "\\")
sys.path.append(path)
#==============================================================

import struct
import time
from Client import SocketClient
import json

import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Flatten, Input
from tensorflow.keras.optimizers import Adam
import tensorflow.keras.initializers as initializers

# Neurális háló modelljét felépítő függvény
# Neurális háló felépítése: - Bemeneti réteg
#                           - 1. Rejtett réteg: 40 neuron
#                           - 2. Rejtett réteg: 30 neuron
#                           - Kimeneti réteg
def buildModel(states, actions) -> keras.Model:
    inputs = Input(shape=(states,)) 
    hidden1 = Dense(
        40, activation="tanh", kernel_initializer=initializers.glorot_uniform()
    )(inputs)
    hidden2 = Dense(
        30, activation="tanh", kernel_initializer=initializers.glorot_uniform()
    )(hidden1)
    q_values = Dense(
        actions, kernel_initializer=initializers.Identity() , activation="linear",
    )(hidden2)

    modell = keras.Model(inputs=inputs, outputs=[q_values])

    return modell



class RemoteNaiveHunterStrategy:

    def __init__(self):
        # Dinamikus viselkedéshez szükséges változók definíciója
        #self.model = buildModel(12, 9)                             #12 bemenet, 9 kimenet
        current_dir = pathlib.Path().resolve().joinpath("model")
        print(current_dir)
        #self.model = tf.keras.models.load_model("D:/Egyetem/Msc/1.Szemeszter/Adaptív rendszerek modellezése/HF/adaptivegame-main/src/model")
        self.model  = tf.keras.models.load_model(current_dir)
        self.opt = tf.keras.optimizers.Adam(learning_rate=0.1)
        self.exploration_rate = 1
        self.gameCount = 0
        self.model.trainable = True
        self.neighbours = [0,0,0,0,0,0,0,0]
        self.xPyP= 0.0
        self.xPyN= 0.0
        self.xNyP= 0.0
        self.xNyN= 0.0
        self.prevPos = None
        self.reward = 0
        self.prevPosCtr = 0

    # Egyéb függvények...
    def getRandomAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action

    
    # A szomszédos 8 mező megvizsgálása
    def searchNeighbour(self, field):
        if field["player"] is not None:
            if field["player"]["size"] * 1.1 < self.size:
                if field["relative_coord"][0] == -1 and field["relative_coord"][1] == -1:
                                self.neighbours[0] = field["player"]["size"]
                elif field["relative_coord"][0] == -1 and field["relative_coord"][1] == 0:
                                self.neighbours[1] = field["player"]["size"]
                elif field["relative_coord"][0] == -1 and field["relative_coord"][1] == 1:
                                self.neighbours[2] = field["player"]["size"]
                elif field["relative_coord"][0] == 0 and field["relative_coord"][1] == -1:
                                self.neighbours[3] = field["player"]["size"]
                elif field["relative_coord"][0] == 0 and field["relative_coord"][1] == 1:
                                self.neighbours[4] = field["player"]["size"]
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == -1:
                                self.neighbours[5] = field["player"]["size"]
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == 0:
                                self.neighbours[6] = field["player"]["size"]
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == 1:
                                self.neighbours[7] = field["player"]["size"]
            else:
                if field["relative_coord"][0] == -1 and field["relative_coord"][1] == -1:
                                self.neighbours[0] = -100
                elif field["relative_coord"][0] == -1 and field["relative_coord"][1] == 0:
                                self.neighbours[1] = -100
                elif field["relative_coord"][0] == -1 and field["relative_coord"][1] == 1:
                                self.neighbours[2] = -100
                elif field["relative_coord"][0] == 0 and field["relative_coord"][1] == -1:
                                self.neighbours[3] = -100
                elif field["relative_coord"][0] == 0 and field["relative_coord"][1] == 1:
                                self.neighbours[4] = -100
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == -1:
                                self.neighbours[5] = -100
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == 0:
                                self.neighbours[6] = -100
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == 1:
                                self.neighbours[7] = -100
        else:
            if 0 < field["value"] <= 3:
                if field["relative_coord"][0] == -1 and field["relative_coord"][1] == -1:
                                self.neighbours[0] = field["value"]
                elif field["relative_coord"][0] == -1 and field["relative_coord"][1] == 0:
                                self.neighbours[1] = field["value"]
                elif field["relative_coord"][0] == -1 and field["relative_coord"][1] == 1:
                                self.neighbours[2] = field["value"]
                elif field["relative_coord"][0] == 0 and field["relative_coord"][1] == -1:
                                self.neighbours[3] = field["value"]
                elif field["relative_coord"][0] == 0 and field["relative_coord"][1] == 1:
                                self.neighbours[4] = field["value"]
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == -1:
                                self.neighbours[5] = field["value"]
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == 0:
                                self.neighbours[6] = field["value"]
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == 1:
                                self.neighbours[7] = field["value"]
            elif field["value"] == 9:
                if field["relative_coord"][0] == -1 and field["relative_coord"][1] == -1:
                                self.neighbours[0] = -1
                elif field["relative_coord"][0] == -1 and field["relative_coord"][1] == 0:
                                self.neighbours[1] = -1
                elif field["relative_coord"][0] == -1 and field["relative_coord"][1] == 1:
                                self.neighbours[2] = -1
                elif field["relative_coord"][0] == 0 and field["relative_coord"][1] == -1:
                                self.neighbours[3] = -1
                elif field["relative_coord"][0] == 0 and field["relative_coord"][1] == 1:
                                self.neighbours[4] = -1
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == -1:
                                self.neighbours[5] = -1
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == 0:
                                self.neighbours[6] = -1
                elif field["relative_coord"][0] == 1 and field["relative_coord"][1] == 1:
                                self.neighbours[7] = -1

    # A négy síknegyed megvizsgálása
    def searchQuarter(self,field, radian):
        if field["player"] is not None:
            if field["player"]["size"] * 1.1 < self.size:
                if field["relative_coord"][0] > 0 and field["relative_coord"][1] >= 0:
                                    self.xPyP+= field["player"]["size"] / (radian+1)
                elif field["relative_coord"][0] <= 0 and field["relative_coord"][1] > 0:
                                    self.xNyP += field["player"]["size"] / (radian+1)
                elif field["relative_coord"][0] < 0 and field["relative_coord"][1] <= 0:
                                    self.xNyN += field["player"]["size"] / (radian+1)
                elif field["relative_coord"][0] >= 0 and field["relative_coord"][1] < 0:
                                    self.xPyN += field["player"]["size"] / (radian+1)

            else:
                if field["relative_coord"][0] > 0 and field["relative_coord"][1] >= 0:
                                            self.xPyP += -100 / (radian+1)
                elif field["relative_coord"][0] <= 0 and field["relative_coord"][1] > 0:
                                            self.xNyP += -100 / (radian+1)
                elif field["relative_coord"][0] < 0 and field["relative_coord"][1] <= 0:
                                            self.xNyN += -100 / (radian+1)
                elif field["relative_coord"][0] >= 0 and field["relative_coord"][1] < 0:
                                           self.xPyN += -100 / (radian+1)
        else:
            if 0 < field["value"] <= 3:
                if field["relative_coord"][0] > 0 and field["relative_coord"][1] >= 0:
                                        self.xPyP += field["value"] / (radian+1)
                elif field["relative_coord"][0] <= 0 and field["relative_coord"][1] > 0:
                                        self.xNyP += field["value"] / (radian+1)
                elif field["relative_coord"][0] < 0 and field["relative_coord"][1] <= 0:
                                        self.xNyN += field["value"] / (radian+1)
                elif field["relative_coord"][0] >= 0 and field["relative_coord"][1] < 0:
                                        self.xPyN += field["value"] / (radian+1)
            elif field["value"] == 9:
                if field["relative_coord"][0] > 0 and field["relative_coord"][1] >= 0:
                                        self.xPyP += -0.1/ (radian+1)
                elif field["relative_coord"][0] <= 0 and field["relative_coord"][1] > 0:
                                        self.xNyP += -0.1 / (radian+1)
                elif field["relative_coord"][0] < 0 and field["relative_coord"][1] <= 0:
                                        self.xNyN += -0.1 / (radian+1)
                elif field["relative_coord"][0] >= 0 and field["relative_coord"][1] < 0:
                                        self.xPyN += -0.1 / (radian+1)   
            

            
    # Az egyetlen kötelező elem: A játékmestertől jövő információt feldolgozó és választ elküldő függvény
    def processObservation(self, fulljson, sendData):
        """
        :param fulljson: A játékmestertől érkező JSON dict-be konvertálva.
        Két kötelező kulccsal: 'type' (leaderBoard, readyToStart, started, gameData, serverClose) és 'payload' (az üzenet adatrésze).
        'leaderBoard' type a játék végét jelzi, a payload tartalma {'ticks': a játék hossza tickekben, 'players':[{'name': jáétékosnév, 'active': él-e a játékos?, 'maxSize': a legnagyobb elért méret a játék során},...]}
        'readyToStart' type esetén a szerver az indító üzenetre vár esetén, a payload üres (None)
        'started' type esetén a játék elindul, tickLength-enként kiküldés és akciófogadás várható payload {'tickLength': egy tick hossza }
        'gameData' type esetén az üzenet a játékos által elérhető információkat küldi, a payload:
                                    {"pos": abszolút pozíció a térképen, "tick": az aktuális tick sorszáma, "active": a saját életünk állapota,
                                    "size": saját méret,
                                    "leaderBoard": {'ticks': a játék hossza tickekben eddig, 'players':[{'name': jáétékosnév, 'active': él-e a játékos?, 'maxSize': a legnagyobb elért méret a játék során eddig},...]},
                                    "vision": [{"relative_coord": az adott megfigyelt mező relatív koordinátája,
                                                                    "value": az adott megfigyelt mező értéke (0-3,9),
                                                                    "player": None, ha nincs aktív játékos, vagy
                                                                            {name: a mezőn álló játékos neve, size: a mezőn álló játékos mérete}},...] }
        'serverClose' type esetén a játékmester szabályos, vagy hiba okozta bezáródásáról értesülünk, a payload üres (None)
        :param sendData: A kliens adatküldő függvénye, JSON formátumú str bemenetet vár, melyet a játékmester felé továbbít.
        Az elküldött adat struktúrája {"command": Parancs típusa, "name": A küldő azonosítója, "payload": az üzenet adatrésze}
        Elérhető parancsok:
        'SetName' A kliens felregisztrálja a saját nevét a szervernek, enélkül a nevünkhöz tartozó üzenetek nem térnek vissza.
                 Tiltott nevek: a configban megadott játékmester név és az 'all'.
        'SetAction' Ebben az esetben a payload az akció string, amely két karaktert tartalmaz az X és az Y koordináták (matematikai mátrix indexelés) menti elmozdulásra.
                a karakterek értékei '0': helybenmaradás az adott tengely mentén, '+' pozitív irányú lépés, '-' negatív irányú lépés lehet. Amennyiben egy tick ideje alatt
                nem külünk értéket az alapértelmezett '00' kerül végrehajtásra.
        'GameControl' üzeneteket csak a Config.py-ban megadott játékmester névvel lehet küldeni, ezek a játékmenetet befolyásoló üzenetek.
                A payload az üzenet típusát (type), valamint az ahhoz tartozó 'data' adatokat kell, hogy tartalmazza.
                    'start' type elindítja a játékot egy "readyToStart" üzenetet küldött játék esetén, 'data' mezője üres (None)
                    'reset' type egy játék után várakozó 'leaderBoard'-ot küldött játékot állít alaphelyzetbe. A 'data' mező
                            {'mapPath':None, vagy elérési útvonal, 'updateMapPath': None, vagy elérési útvonal} formátumú, ahol None
                            esetén az előző pálya és növekedési map kerül megtartásra, míg elérési útvonal megadása esetén új pálya kerül betöltésre annak megfelelően.
                    'interrupt' type esetén a 'data' mező üres (None), ez megszakítja a szerver futását és szabályosan leállítja azt.
        :return:
        """

        # Játék rendezéssel kapcsolatos üzenetek lekezelése
        if fulljson["type"] == "leaderBoard":
            print("Game finished after",fulljson["payload"]["ticks"],"ticks!")
            print("Leaderboard:")
            self.gameCount += 1
            if self.gameCount < 0:
                self.exploration_rate=1
            else:
                self.exploration_rate = 0.1

            if self.gameCount == 250:
                tf.keras.models.save_model(self.model,"D:/Egyetem/Msc/1.Szemeszter/Adaptív rendszerek modellezése/HF/adaptivegame-main/src/model",overwrite=True,include_optimizer=True, save_format=None, signatures=None, options=None, save_traces=True)

            print(self.gameCount)
            time.sleep(1)
            self.opt =  tf.keras.optimizers.Adam(learning_rate=0.01)


            

            for score in fulljson["payload"]["players"]:
                print(score["name"],score["active"], score["maxSize"])

            #time.sleep(50)
            mapPath =""
            mapNew = np.random.randint(3)
            if mapNew == 0:
                mapPath = "D:/Egyetem/Msc/1.Szemeszter/Adaptív rendszerek modellezése/HF/adaptivegame-main/src/maps/02_base.txt"
            elif mapNew == 1:
                mapPath = "D:/Egyetem/Msc/1.Szemeszter/Adaptív rendszerek modellezése/HF/adaptivegame-main/src/maps/03_blockade.txt"
            else:
                mapPath = "D:/Egyetem/Msc/1.Szemeszter/Adaptív rendszerek modellezése/HF/adaptivegame-main/src/maps/04_mirror.txt"

            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "reset", "data": {"mapPath": mapPath, "updateMapPath": mapPath}}}))

        if fulljson["type"] == "readyToStart":
            print("Game is ready, starting in 5")
            #time.sleep(5)
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "start", "data": None}}))

        if fulljson["type"] == "started":
            print("Startup message from server.")
            print("Ticks interval is:",fulljson["payload"]["tickLength"])



        


        # Akció előállítása bemenetek alapján
        # Bemenetek: 8 szomszédos mező értéke
        #            4 síknegyed értéke
        elif fulljson["type"] == "gameData":
            jsonData = fulljson["payload"]
            if "pos" in jsonData.keys() and "tick" in jsonData.keys() and "active" in jsonData.keys() and "size" in jsonData.keys() and "vision" in jsonData.keys():
                if self.prevPos is not None:
                    if tuple(self.prevPos) == tuple(jsonData["pos"]):
                        self.prevPosCtr += 1
                    else:
                        self.prevPosCtr = 0
                if jsonData["active"]:
                    self.prevPos = jsonData["pos"].copy()

                self.xPyP= 0.0
                self.xPyN= 0.0
                self.xNyP= 0.0
                self.xNyN= 0.0

                self.size = jsonData["size"]

                for field in jsonData["vision"]:
                    rad = max(abs(field["relative_coord"][0]), abs(field["relative_coord"][0]))-1
                    if rad == -1:
                        continue
                    if rad > 2 and field["value"] != 9 and field["value"] != 0:
                        i =0

                    self.searchNeighbour(field)
                    self.searchQuarter(field,rad)
            
            state = tf.constant([[self.xPyP, self.xNyP, self.xPyN, self.xNyN,
                                  self.neighbours[0], self.neighbours[1], self.neighbours[2], 
                                  self.neighbours[3], self.neighbours[4], self.neighbours[5], 
                                  self.neighbours[6], self.neighbours[7]]])
            state.trainable = True                        

            with tf.GradientTape() as tape:

                q_vec = self.model(state)

                epsilon = np.random.rand()
                if epsilon <= self.exploration_rate:
                    action = np.random.choice(8)
                else:
                    action = np.argmax(q_vec)                
                
                actionStr = "00"

                pos = 0
                if action == 1:
                    actionStr = "++"
                    pos = 7
                elif action == 2:
                    actionStr = "+0"
                    pos = 6
                elif action == 3:
                    actionStr = "+-"
                    pos = 5
                elif action == 4:
                    actionStr = "0+"
                    pos = 4
                elif action == 5:
                    actionStr = "0-"
                    pos = 3
                elif action == 6:
                    actionStr = "-+"
                    pos = 2
                elif action == 7:
                    actionStr = "-0"
                    pos = 1
                elif action == 0:
                    actionStr = "--"
                    pos = 0
                else:
                    actionStr = "--"
                    pos = 0

                if self.prevPosCtr > 2:
                    self.reward = -1
                else:
                    self.reward = self.neighbours[pos]

                if jsonData["active"]:
                    q = q_vec[0,action]
                    loss = (self.reward - q) ** 2
                    grads = tape.gradient(loss, self.model.trainable_variables)
                    self.opt.apply_gradients(zip(grads, self.model.trainable_variables))




                # Akció JSON előállítása és elküldése
                sendData(json.dumps({"command": "SetAction", "name": "Pink", "payload": actionStr}))

if __name__=="__main__":
    # Példányosított stratégia objektum
    hunter = RemoteNaiveHunterStrategy()

    # Socket kliens, melynek a szerver címét kell megadni (IP, port), illetve a callback függvényt, melynek szignatúrája a fenti
    # callback(fulljson, sendData)
    client = SocketClient("localhost", 42069, hunter.processObservation)

    # Kliens indítása
    client.start()
    # Kis szünet, hogy a kapcsolat felépülhessen, a start nem blockol, a kliens külső szálon fut
    time.sleep(0.1)
    # Regisztráció a megfelelő névvel
    client.sendData(json.dumps({"command": "SetName", "name": "Pink", "payload": None}))

   

    # Nincs blokkoló hívás, a főszál várakozó állapotba kerül, itt végrehajthatók egyéb műveletek a kliens automata működésétől függetlenül.