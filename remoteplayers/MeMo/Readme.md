# Adaptio reinforcement learning
Készítették:
Meskó Patrik és Morvai Balázs

A feladat egy agent létrehozása volt az adaptio játékhoz.

A reinforcement learning megoldást választottuk, melyet gyakran alkalmaznak játékok 
esetében. A modellt büntetjük, vagy jutalmazzuk az elért eredményei alapján: 
megevett kaják, játékosok, mozgékonyság és esetleges halál.

A feladathoz a tensorflow 2.8.0 verzióját használtuk és egy teljesen kapcsolt neurális 
hátót hoztunk létre. A bementei réteg 81 neuront tartalmaz a látómező értékeivel, 
a két rejtett réteg 256 neuront, a kimeneti réteg pedig 9 neuront, melyek a 
lehetséges akciók.

A Client_Memo.py file tartalmazza az agentet, 
a dqn_keras.py a modelt, 
a helper.py a plottert a tanításhoz és 
a ddqn_model.h5 az elmentett súlyokat.