# Original file name: agent_dqn_keras.py

#==============================================================
# Adds the src map to the path, so imports can work properly
#==============================================================
import pathlib
import sys

current_dir = pathlib.Path().resolve()
src_dir     = current_dir.parent.parent.joinpath("src")
path = str(src_dir).replace("/", "\\")
sys.path.append(path)
#==============================================================


from dqn_keras import DDQNAgent
import numpy as np
#from utils import plotLearning
#import matplotlib.pyplot as plt
import helper 

from Client import SocketClient
import json
import time
import Config

#az agent osztály
class Agent:

    def __init__(self):
        self.newsize = 0
        self.oldsize = 0
        self.active = True
        self.score = 0
        self.gameDone = False
        self.eps_history = []
        self.scores = []
        self.plot_scores = []
        self.plot_mean_scores = []
        self.total_score = 0
        self.record = 0
        self.reward = 0
        self.last_reward = 0
        self.state_old = []
        self.state_new = []
        self.firstRun = True
        self.oldpos = 0
        self.pos = 0 
        self.collisionCounter=0
        #self.dqn_agent = dqnAgent.load_model()
        self.dqn_agent = DDQNAgent(gamma=0.90, epsilon=1.0, alpha=0.0001, input_dims = 81, n_actions =9, mem_size=1000000, batch_size=Config.MAXTICKS, epsilon_end=0.01)
        self.action = 4
        self.oldaction = 4
        self.n_games = 0
        self.highscore = 0

        
        
    #kommunikáció
    def getInfo(self, fulljson, sendData):
        if fulljson["type"] == "leaderBoard":
            print("Game finished after",fulljson["payload"]["ticks"],"ticks!")
            print("Leaderboard:")
            for score in fulljson["payload"]["players"]:
                print(score["name"],score["active"], score["maxSize"])
            self.gameDone = True

            time.sleep(50)

            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))

        if fulljson["type"] == "readyToStart":
            print("Game is ready, starting in 5")
            time.sleep(5)
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "start", "data": None}}))

        if fulljson["type"] == "started":
            print("Startup message from server.")
            print("Ticks interval is:",fulljson["payload"]["tickLength"])

    #állapot lekérdezés
    def getState(self, fulljson, sendData):

        if fulljson["type"] == "gameData":
            jsonData = fulljson["payload"]
           
            if "pos" in jsonData.keys() and "tick" in jsonData.keys() and "active" in jsonData.keys() and "size" in jsonData.keys() and "vision" in jsonData.keys():
                vals = []
                food = []
                danger = []
                surrounding = ((-1,-1),(0,-1),(1,-1),(-1,0),(0,0),(1,0),(-1,1),(0,1),(1,-1))
                for field in jsonData["vision"]: 
                    #if tuple(field["relative_coord"]) in surrounding:
                        if field["player"] != None:
                            if field["player"]["size"]*1.1>jsonData["size"]:
                                food.append(-1)
                            else:
                                food.append(field["player"]["size"])
                        else:
                            if 0 < field["value"] <= 3:
                                food.append(field["value"])
                            elif field["value"]==9:
                                food.append(-1)
                            else:
                                food.append(0)
                            
                """ for field in jsonData["vision"]:
                    if tuple(field["relative_coord"]) in surrounding:
                        if field["value"]==9 or (field["player"] != None and field["player"]["size"]*1.1>jsonData["size"]):
                            danger.append(1)
                        else:
                            danger.append(0)              """
                """ for field in jsonData["vision"]:
                    #if tuple(field["relative_coord"]) in surrounding:
                        if field["player"] != None:
                            if field["player"]["size"]*1.1>jsonData["size"]:
                                danger.append(1)
                            else:
                                danger.append(0)
                        else:
                            if field["value"] == 9:
                                danger.append(1)
                            else:
                                danger.append(0) """
                            
                
                            
                            
            
            vals = food + danger
            
            #Új state és pos kiolvasása, de előtte régi elmentése
            if self.firstRun:
                self.state_old = np.array(vals)
                self.state_new = np.array(vals)
                
                self.oldpos = jsonData["pos"]
                self.pos = jsonData["pos"]

                self.oldsize = jsonData["size"]
                self.newsize = jsonData["size"]

                self.active = jsonData["active"]
                
                self.firstRun = False
            else: 
                self.state_old = self.state_new
                self.state_new = np.array(vals)
                
                self.oldpos = self.pos
                self.pos = jsonData["pos"]

                self.oldsize = self.newsize
                self.newsize = jsonData["size"]

                self.active = jsonData["active"]
                self.reward = 0
                if self.oldsize<self.newsize:
                    self.reward=(self.newsize-self.oldsize)*10
                elif self.pos==self.oldpos and self.oldaction!=4:
                    self.reward = 0
                else:
                    self.reward = 0
                

                if self.active ==False:
                    self.reward = -10
                
                self.score += self.reward
                
            

    def processObservation(self, fulljson, sendData):

        if fulljson["type"] != "gameData":
                self.getInfo(fulljson, sendData)

        if self.active and fulljson["type"] == "gameData":
            
            self.getState(fulljson, sendData)   
            
            self.dqn_agent.remember(self.state_old, self.oldaction, self.reward, self.state_new, not self.active)
             
            self.action = self.dqn_agent.choose_action(self.state_new)
            
            actstring = self.play_step(self.action)
            self.oldaction = self.action
            
            sendData(json.dumps({"command": "SetAction", "name": "Pink", "payload": actstring}))
            
        #játék végén tanítás
        if self.gameDone:
           
            self.n_games += 1
            self.dqn_agent.learn()
            self.eps_history.append(self.dqn_agent.epsilon)
            self.scores.append(self.score)

            avg_score = np.mean(self.scores[max(0, self.n_games-100):(self.n_games+1)])
            print("episode",self.n_games,'score %.2f' %self.score,"average score %.2f"%avg_score)

            if self.score>self.highscore and self.n_games>0:
                self.highscore=self.score
                self.dqn_agent.save_model()

            self.plot_scores.append(self.score)
            self.total_score += self.score
            self.mean_score = self.total_score / self.n_games
            self.plot_mean_scores.append(self.mean_score)
            helper.plot(self.plot_scores, self.plot_mean_scores)
            
            
            
            self.score = 0
            self.active = True
            self.firstRun = True
            self.gameDone = False
            """ if self.n_games>=50:
                filename1 = 'proba1.png'
                filename2 = 'proba2.png'
                x = [i+1 for i in range(self.n_games)]
                #plotLearning(x, self.scores, self.eps_history, filename)
                plt.plot(x, self.scores)
                plt.savefig(filename1)
                plt.plot(x, self.eps_history)
                plt.savefig(filename2) """

    #lépések
    def play_step(self, final_move):
        
        actstring = ""
        if final_move == 0:
            actstring += "--"  
            
        elif final_move == 1:
            actstring += "0-"
            
        elif final_move == 2:
            actstring += "+-"
            
        elif final_move == 3:
            actstring += "-0"
            
        elif final_move == 4:
            actstring += "00"
            
        elif final_move == 5:
            actstring += "+0"
            
        elif final_move == 6:
            actstring += "-+"
            
        elif final_move == 7:
            actstring += "0+"

        elif final_move == 8:
            actstring += "++"
        

        return actstring

if __name__ == '__main__':
    # Socket kliens, melynek a szerver címét kell megadni (IP, port), illetve a callback függvényt, melynek szignatúrája a fenti
    # callback(fulljson, sendData)
    agent = Agent()

    client = SocketClient("localhost", 42069, agent.processObservation)

    # Kliens indítása
    client.start()
    # Kis szünet, hogy a kapcsolat felépülhessen, a start nem blockol, a kliens külső szálon fut
    time.sleep(0.1)
    # Regisztráció a megfelelő névvel
    client.sendData(json.dumps({"command": "SetName", "name": "Pink", "payload": None}))
    
    # Nincs blokkoló hívás, a főszál várakozó állapotba kerül, itt végrehajthatók egyéb műveletek a kliens automata működésétől függetlenül.