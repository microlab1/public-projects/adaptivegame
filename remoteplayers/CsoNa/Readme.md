# Mély megerősítéses tanulással

Készítette: Nagy Levente, Csóti Ádám Ágoston <br>

# Áttekintés

1. Gépi tanulás
2. Mély megerősítéses tanulás
3. RL Algoritmusok
4. Implementált algoritmus
5. Implementált algoritmus kódolása
6. Eredmények bemutatása, kiértékelése

# Gépi tanulás

![kép](https://user-images.githubusercontent.com/82893067/170892168-6118a14e-9049-4803-935d-790a2e36bd6f.png)
* Összetartozó adatpárok
* Előforduló minta az adatkészletben
* Jutalmazás (Reward)

# Megerősítéses tanulás

![kép](https://user-images.githubusercontent.com/82893067/170892191-4ddb93e2-9c57-4623-bc06-7a12ceeedbb3.png)
* Tapasztalatgyűjtés, felfedezés
* Optimális stratégia (Policy)
* Jutalomfüggvény maximalizálása Trajektórián

# RL Algoritmusok

* Deep Q network - DQN
* Proximal Policy Optimization - PPO

![kép](https://user-images.githubusercontent.com/82893067/170892222-7ec7243c-949f-4813-8c1a-058b9155857e.png)

Megépített neurális háló:

![kép](https://user-images.githubusercontent.com/82893067/170892251-71de84b7-7d45-47d1-b26f-262e7e54f3fc.png)

# Implementált algoritmus elvi felépítése

![kép](https://user-images.githubusercontent.com/82893067/170892312-c8246433-cebe-4db7-aed3-a770e9f0807a.png)

Action-value:

![kép](https://user-images.githubusercontent.com/82893067/170892328-9b991ae1-e728-4a72-a4f9-84570b099962.png)

Value function:

![kép](https://user-images.githubusercontent.com/82893067/170892333-2fd22588-4501-42e5-97ac-0bcf4e4fa405.png)

A várható return értékének gradiense az alábbi képlettel írható fel: 

![kép](https://user-images.githubusercontent.com/82893067/170892343-78d0d73a-de6b-42f3-9571-f38d43cb7a92.png)

Advantage estimate(mennyivel előnyösebb egy véletlen lépéssel szemben)

![kép](https://user-images.githubusercontent.com/82893067/170892355-4c794f29-25bf-4d25-a47a-32b53db8ff5f.png)

# Implementált algoritmus kódolása

![kép](https://user-images.githubusercontent.com/82893067/170892380-6ce6b1ec-37fd-4ee6-a301-4278107fa0da.png)

# Dependencies: <br>
* numpy <br>
* torch <br>
* scipy <br>