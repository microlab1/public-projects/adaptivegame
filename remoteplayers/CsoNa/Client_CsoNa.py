#encoding: utf-8

#==============================================================
# Adds the src map to the path, so imports can work properly
#==============================================================
import pathlib
import sys

current_dir = pathlib.Path().resolve()
src_dir     = current_dir.parent.parent.joinpath("src")
path = str(src_dir).replace("/", "\\")
sys.path.append(path)
#==============================================================

from cmath import nan
from http import client
from socket import SocketKind
from GameMaster import GameMaster
import time
from Client import SocketClient
import json
import numpy as np
import torch
import torch.nn as nn
from torch.distributions.normal import Normal
from torch.distributions.categorical import Categorical
import scipy
from scipy import signal
from torch.optim import Adam
import os.path as osp, time, atexit, os




class InvalidTensor:

    def __init__(self):
        self.oldpos = None
        self.oldcounter = 0
        self.reward = 0
        self.seed = torch.manual_seed(42)
        self.epoch_ended = False
        self.sumrewards = []
        self.index = 0
        print("InvalidTensor created!")
        self.number_of_epochs = 0
        self.epochs = 1000
        self.games_played = 0
 
        self.obs_dim=np.zeros([327,1]).shape
        self.act_dim=np.zeros([9,1]).shape
        self.number_of_games = 10
        self.ac = ActorCritic(self.act_dim)
        #meglévő modell továbbtanítása
        
        pi_lr=2e-4
        vf_lr=3e-4
        self.vf_optimizer = Adam(self.ac.value.parameters(), lr=vf_lr)
        self.pi_optimizer = Adam(self.ac.pi.parameters(), lr=pi_lr)
        self.checkpoint= torch.load("./state_dict_model.pt")
        self.ac.load_state_dict(self.checkpoint['model_state_dict'])
        self.pi_optimizer.load_state_dict(self.checkpoint['optimizers_state_dict'])
        self.vf_optimizer.load_state_dict(self.checkpoint['optimizers_state_dict2'])
        
        self.buf = PPOBuffer(self.obs_dim, self.act_dim, self.number_of_games*300, gamma=0.97, lam=0.95)
        
        self.size_old = 0

    # Egyéb függvények...
    def getRandomAction(self):
        actdict = {0: "0", 1: "+", 2: "-"}
        r = np.random.randint(0, 3, 2)
        action = ""
        for act in r:
            action += actdict[act]

        return action

    def decide_action(self, action):
        index = np.argmax(action)
        if index == 0:
            actstring = "-+"
        elif index == 1:
            actstring = "0+"
        elif index == 2:
            actstring = "++"
        elif index == 3:
            actstring = "-0"
        elif index == 4:
            actstring = "00"
        elif index == 5:
            actstring = "+0"
        elif index == 6:
            actstring = "--"
        elif index == 7:
            actstring = "0-"
        elif index == 8:
            actstring = "+-"
        else:
            actstring = "00"

        return actstring

    def compute_loss_pi(self, data):
            obs, act, adv, logp_old = data['obs'], data['act'], data['adv'], data['logp']
            pi, logp = self.ac.pi(obs.view(self.number_of_games * 300, 327), act)
            clip_ratio=0.2
            # Policy loss
            ratio = torch.exp(logp - logp_old)
            clip_adv = torch.clamp(ratio, 1-clip_ratio, 1+clip_ratio) * adv
            loss_pi = -(torch.min(ratio * adv, clip_adv)).mean()


            approx_kl = (logp_old - logp).mean().item()
            ent = pi.entropy().mean().item()
            clipped = ratio.gt(1+clip_ratio) | ratio.lt(1-clip_ratio)
            clipfrac = torch.as_tensor(clipped, dtype=torch.float32).mean().item()
            pi_info = dict(kl=approx_kl, ent=ent, cf=clipfrac)

            return loss_pi, pi_info

        #Valuse loss

    def compute_loss_v(self, data):
        obs, ret = data['obs'], data['ret']

        return ((self.ac.value(obs.view(self.number_of_games* 300, 327)) - ret)**2).mean()

    def update(self):   
        pi_lr=1e-4
        vf_lr=8e-4
        train_pi_iters=40
        train_v_iters=40
        data = self.buf.get()
        target_kl=0.01

        # Start new training
        pi_optimizer = Adam(self.ac.pi.parameters(), lr=pi_lr)
        vf_optimizer = Adam(self.ac.value.parameters(), lr=vf_lr)

        
        # Policy training (gradient descent)
        for i in range(train_pi_iters):
            print('Policy training with gradient descent %d/%d'%(i+1, train_pi_iters))
            pi_optimizer.zero_grad()
            loss_pi, pi_info = self.compute_loss_pi(data)
            kl = pi_info['kl']
            if kl > 1.5 * target_kl:
                print("early stop")
                break
            loss_pi.backward()
            pi_optimizer.step()

        # Value function 
        for i in range(train_v_iters):
            print('Value function learning %d/%d'%(i+1, train_v_iters))
            vf_optimizer.zero_grad()
            loss_v = self.compute_loss_v(data)
            loss_v.backward()
            vf_optimizer.step()
    # Az egyetlen kötelező elem: A játékmestertől jövő információt feldolgozó és választ elküldő függvény
    def processObservation(self, fulljson, sendData):

        obs = np.array([])
        # Akció előállítása bemenetek alapján (egyezik a NaiveHunterBot-okéval)
        # obs:
        if fulljson["type"] == "gameData":
            jsonData = fulljson["payload"]
            tick = jsonData["tick"]
            #print(tick)
            size_new = jsonData["size"]
            if size_new > self.size_old:
                self.reward += size_new - self.size_old
                self.size_old = size_new
            else:
                self.reward -= 0.5
            if "pos" in jsonData.keys() and "tick" in jsonData.keys() and "active" in jsonData.keys() and "size" in jsonData.keys() and "vision" in jsonData.keys():
                if self.oldpos is not None:
                    if tuple(self.oldpos) == tuple(jsonData["pos"]):
                        self.reward -= 0.1
                        self.oldcounter += 1
                    else:
                        self.reward += 0.5
                        self.oldcounter = 0
                if jsonData["active"]:
                    self.oldpos = jsonData["pos"].copy()
                #pos, size
                obs = np.append(obs, jsonData["pos"][0]/38)
                obs = np.append(obs, jsonData["pos"][1]/38)
                obs = np.append(obs, np.log(jsonData["size"]))

                vals = []
                #visible fields
                for field in jsonData["vision"]:
                    if field["player"] is not None:
                        obs = np.append(obs, field["relative_coord"][0]/38)
                        obs = np.append(obs, field["relative_coord"][1]/38)
                        obs = np.append(obs, field["value"]/9)
                        obs = np.append(obs, np.log(field["player"]["size"]))
                    else:
                        obs = np.append(obs, field["relative_coord"][0]/38)
                        obs = np.append(obs, field["relative_coord"][1]/38)
                        obs = np.append(obs, field["value"]/9)
                        obs = np.append(obs, 0)
                
                action, value, logp = self.ac.step(torch.as_tensor(obs.copy(), dtype=torch.float32))
                actstring = self.decide_action(action)
                    

                if actstring != "00":
                    self.reward += 0.5
                else:
                    self.reward -= 0.3
                obs = np.reshape(obs, (327,1))
                
                if tick != 300:
                    self.buf.store(obs, action, self.reward, value[0], logp[0])
                
                sendData(json.dumps({"command": "SetAction", "name": "Pink", "payload": actstring}))

            if tick > 299 :
                self.sumrewards=np.append(self.sumrewards,self.reward)
                print("Reward received:",self.reward)

                
                self.buf.finish_path(value[0])
                self.games_played += 1
                print("Games played: ",self.games_played)
                print("Current epoch: ", self.number_of_epochs)
                self.reward = 0
                if self.games_played < self.number_of_games:
                    sendData(json.dumps({"command": "GameControl", "name": "master","payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))
                    sendData(json.dumps({"command": "GameControl", "name": "master","payload": {"type": "start", "data": None}}))
                else:
                    #mentés
                    """
                    if self.number_of_epochs>1:
                        torch.save({
                            'model_state_dict': self.ac.state_dict(),
                            'optimizers_state_dict': self.pi_optimizer.state_dict(),
                            'optimizers_state_dict2': self.vf_optimizer.state_dict(),
                            }, "./state_dict_model.pt")
                    """
                    self.update()
                    self.games_played = 0
                    self.number_of_epochs += 1
                    self.buf = PPOBuffer(self.obs_dim, self.act_dim, self.number_of_games*300, gamma=0.97, lam=0.95)
                    sendData(json.dumps({"command": "GameControl", "name": "master","payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))
                    sendData(json.dumps({"command": "GameControl", "name": "master","payload": {"type": "start", "data": None}}))

                if self.number_of_epochs == self.epochs:
                    sendData(json.dumps({"command": "GameControl", "name": "master","payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))
                    sendData(json.dumps({"command": "GameControl", "name": "master","payload": {"type": "interrupt", "data": None}}))

class PPOBuffer:
    def __init__(self, obs_dim, act_dim, size, gamma=0.99, lam=0.95):
        
        self.obs_buf = np.zeros(self.combined_shape(size, obs_dim), dtype=np.float32)
        self.act_buf = np.zeros(self.combined_shape(size, act_dim), dtype=np.float32)
        self.adv_buf = np.zeros(size, dtype=np.float32)
        self.rew_buf = np.zeros(size, dtype=np.float32)
        self.ret_buf = np.zeros(size, dtype=np.float32)
        self.val_buf = np.zeros(size, dtype=np.float32)
        self.logp_buf = np.zeros(size, dtype=np.float32)
        self.gamma, self.lam = gamma, lam
        self.ptr, self.path_start_idx, self.max_size = 0, 0, size

    def combined_shape(self, length, shape=None):
        if shape is None:
            return (length,)
        return (length, shape) if np.isscalar(shape) else (length, *shape)

    def store(self, obs, act, rew, val, logp):
        #print(self.ptr)
        assert self.ptr < (self.max_size)
        self.obs_buf[self.ptr] = obs
        self.act_buf[self.ptr] = act
        self.rew_buf[self.ptr] = rew
        self.val_buf[self.ptr] = val
        self.logp_buf[self.ptr] = logp
        self.ptr += 1

    def finish_path(self, last_val=0):
        path_slice = slice(self.path_start_idx, self.ptr)
        rews = np.append(self.rew_buf[path_slice], last_val)
        vals = np.append(self.val_buf[path_slice], last_val)
        
        # GAE-Lambda advantage estimate
        deltas = rews[:-1] + self.gamma * vals[1:] - vals[:-1]
        self.adv_buf[path_slice] = scipy.signal.lfilter([1], [1, float(-self.gamma * self.lam)], deltas[::-1], axis=0)[::-1]
        # value function tanítására
        rews2 = rews[1:]
        self.ret_buf[path_slice] = scipy.signal.lfilter([1], [1, float(-self.gamma)], rews2[::-1], axis=0)[::-1]
        self.path_start_idx = self.ptr

    def get(self):
        #print("Get Pointer erteke: ",self.ptr)
        #assert self.ptr == self.max_size
        self.ptr, self.path_start_idx = 0, 0
        #print("self.adv_buf",self.adv_buf)
        adv_buf_np = np.array(self.adv_buf, dtype=np.float32)
        adv_mean = np.sum(adv_buf_np) / len(adv_buf_np)
        adv_std = np.sum((adv_buf_np - adv_mean)**2) / len(adv_buf_np)
        self.adv_buf = (self.adv_buf - adv_mean) / adv_std
        data = dict(obs=self.obs_buf, act=self.act_buf, ret=self.ret_buf, adv=self.adv_buf, logp=self.logp_buf)
        return {k: torch.as_tensor(v, dtype=torch.float32) for k,v in data.items()}


class Actor(nn.Module):#Base class for all neural network modules.
    def _distribution(self, obs):
        raise NotImplementedError

    def _log_prob_from_distribution(self, pi, act):
        raise NotImplementedError

    def forward(self, obs, act=None):   
        pi = self._distribution(obs)
        logp_a = None
        if act is not None:
            logp_a = self._log_prob_from_distribution(pi, act)
        return pi, logp_a


    
class GaussianActor(Actor):
     
    def __init__(self, act_dim):
        super().__init__()
        log_std = -0.5 * np.ones(act_dim, dtype=np.float32)           # szoras
        self.log_std = torch.nn.Parameter(torch.as_tensor(log_std))
        
        self.mu_net = nn.Sequential(
            
            nn.Linear(327, 64),
            nn.ReLU(),
            nn.Linear(64, 32),
            nn.ReLU(),
            nn.Linear(32, 9)
        )

    def _distribution(self, obs):
        obs = obs.view(obs.shape[0],obs.shape[1])
        #print("obs: ",obs)
        mu = self.mu_net(obs).view(9,obs.shape[0])
        if torch.isnan(mu).sum() > 0:
            print("Mu elszallt", mu)
        self.log_std = torch.nn.Parameter(torch.as_tensor(-0.5 * np.ones([9,obs.shape[0]], dtype=np.float32)))
        std = torch.exp(self.log_std)	
        return Normal(mu, std)

    def _log_prob_from_distribution(self, pi, act):
        
        act = act.view(act.shape[1], act.shape[0])
        #print("act: ", act.shape)
        return pi.log_prob(act).sum(axis=0)



class Critic(nn.Module):
     
    def __init__(self):
        super().__init__()
        
        self.v_net = nn.Sequential(

            nn.Linear(327, 64),
            nn.ReLU(),
            nn.Linear(64, 32),
            nn.ReLU(),
            nn.Linear(32, 1)
        )
    def forward(self, obs):
       return torch.squeeze(self.v_net(obs), -1)


class ActorCritic(nn.Module):
    def __init__(self, act_dim=9):
        super().__init__()

        self.pi = GaussianActor(act_dim)

        self.value = Critic()
    
    def step(self, obs):
        obs = torch.unsqueeze(obs, 0)
        with torch.no_grad():
            pi = self.pi._distribution(obs)
            action = pi.sample()
            logp_action = self.pi._log_prob_from_distribution(pi, action)
            value = self.value(obs)
        return action.numpy(), value.numpy(), logp_action.numpy()

    def act(self, obs):
        return self.step(obs)[0]

if __name__ == "__main__":
    #gm = GameMaster()
    hunter = InvalidTensor()
    client = SocketClient("localhost", 42069, hunter.processObservation)
    # Kliens indítása
    client.start()
    # Kis szünet, hogy a kapcsolat felépülhessen, a start nem blockol, a kliens külső szálon fut
    time.sleep(0.1)
    # Regisztráció a megfelelő névvel
    client.sendData(json.dumps({"command": "SetName", "name": "Pink", "payload": None}))
    #gm.run()
