#encoding: utf-8

#==============================================================
# Adds the src map to the path, so imports can work properly
#==============================================================
import pathlib
import sys

current_dir = pathlib.Path().resolve()
src_dir     = current_dir.parent.parent.joinpath("src")
path = str(src_dir).replace("/", "\\")
sys.path.append(path)
#==============================================================

import time
from Client import SocketClient
import json
import numpy as np
import math
import Config
from numpy import loadtxt
import subprocess


olpos=[0,0]
mapp=np.ones([40,40])*-1#egyszer létrehozzuk a térképet

mapp=((np.array(mapp)).reshape(40,40))
mapp[0:40,0]=9
mapp[0:40,39]=9
mapp[0,0:39]=9
mapp[39,0:40]=9

# NaiveHunter stratégia implementációja távoli eléréshez.
class RemoteNaiveHunterStrategy:

    def __init__(self):
        # Dinamikus viselkedéshez szükséges változók definíciója
        self.oldpos = None
        self.oldcounter = 0


    # Az egyetlen kötelező elem: A játékmestertől jövő információt feldolgozó és választ elküldő függvény
    def processObservation(self, fulljson, sendData):
        # Játék rendezéssel kapcsolatos üzenetek lekezelése
        if fulljson["type"] == "leaderBoard":
            print("Game finished after",fulljson["payload"]["ticks"],"ticks!")
            print("Leaderboard:")
            for score in fulljson["payload"]["players"]:
                print(score["name"],score["active"], score["maxSize"])
            
            time.sleep(1)
            
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "reset", "data": {"mapPath": None, "updateMapPath": None}}}))
                                 
          

        if fulljson["type"] == "readyToStart":
            print("Game is ready, starting in 5")
            time.sleep(1)
            sendData(json.dumps({"command": "GameControl", "name": "master",
                                 "payload": {"type": "start", "data": None}}))

        if fulljson["type"] == "started":
            print("Startup message from server.")
            print("Ticks interval is:",fulljson["payload"]["tickLength"])

        elif fulljson["type"] == "gameData":
            jsonData=fulljson["payload"]
            
            if jsonData['tick']==0:
                sulyok=loadtxt("log/Egyed4.txt", comments="#", delimiter=",", unpack=False)
                global w1
                global w2
                global w3
                global w4
                global w5
                global w6
                global w7
                global w8
                global w9
                w1=int(sulyok[0])
                w2=int(sulyok[1])
                w3=int(sulyok[2])
                w4=int(sulyok[3])
                w5=int(sulyok[4])
                w6=int(sulyok[5])
                w7=int(sulyok[6])
                w8=int(sulyok[7])
                w9=int(sulyok[8])
            
            
            # Akció előállítása bemenetek alapján (egyezik a NaiveHunterBot-okéval)
            actstring=""
         
            sum_heur=0   
            menekules=0
            maxx=0
            veszely=0            
            keveskaja_hatar=w7  #suly
            veszely_hatar=w8  #suly
         
         
            pos=jsonData['pos']
            print(pos)
            d=[]    
            c=[]
            for field in jsonData['vision']:
                for valami in field['relative_coord']:
                    c.append(valami) #relativ koordinatak kinyerese
            c=np.array(c)        
            c=c.reshape(81,2) #atalakitasa 81x2-esse
            
            sizesajat=jsonData['size'];
            for field in jsonData['vision']:
                if field['relative_coord']!=(0,0):
                    if field['player']==None:
                        d.append(int(field['value']));
                    else:
                        d.append((10+int(field["player"]["size"])));
                else:
                    d.append(0);       
            
            latomez=np.column_stack([c,d]);    #latomez 81x3 tomb letrehozasa
            
            
            g=np.ones([11,11])*-2#latomezo vizualizalasahoz matrix letrehozasa
            g=np.reshape(g,(11,11))
            for k in range(81):
                g[5+latomez[k,1],5+latomez[k,0]]=latomez[k,2] #latomezoben x és y értékek az indexek a g nek és a value-val egyenlo

          
            

            for i in range(11):
                for j in range(11):
                    if g[i,j]!=-2 and (0<(i+pos[1]-5)<40) and (0<=(j+pos[0]-5)<40) :
                        mapp[i+pos[1]-5,j+pos[0]-5]=g[i,j]     #térkép frissítése a látómezővel    
                        
             #térkép mátrixos alakban
            if jsonData['tick']==0:
                global ss1
                ss1=sizesajat
                global foodstreak1
                foodstreak1=0
                global killstreak1
                killstreak1=0
            if sizesajat==ss1+1 or sizesajat==ss1+2 or sizesajat==ss1+3:
                foodstreak1=foodstreak1+1;
            if ss1+3<sizesajat:
                killstreak1=killstreak1+1;
            ss1=sizesajat            
             
             
            
             
            if jsonData['tick']==0:
                global xr1
                xr1=pos[0]+2;
                global xl1
                xl1=pos[0]-2;
                global yu1
                yu1=pos[1]-2;
                global yd1
                yd1=pos[1]+2;
                global topor1
                topor1=0;
                global ujdef1
                ujdef1=0;
                global eltol1
                eltol1=0;
            if jsonData['active']==True:
                if xl1<=pos[0]<=xr1 and yd1>=pos[1]>=yu1:
                    if eltol1<3:
                        eltol1=eltol1+1;
                    else:
                        topor1=topor1+1;
                if xl1>pos[0] or xr1<pos[0] or yd1<pos[1] or pos[1]<yu1:
                    xr1=pos[0]+2
                    xl1=pos[0]-2
                    yu1=pos[1]-2
                    yd1=pos[1]+2
                    ujdef1=ujdef1+1;
                    eltol1=0;
             
             
             
             
             
             
                
            
            heur_lm=np.ones([11,11])*-2#latomezo vizualizalasahoz matrix letrehozasa
            heur_lm=np.reshape(heur_lm,(11,11))
            g_fal=np.zeros([11,11])*-2#fal melletti mezők
            g_fal=np.reshape(g_fal,(11,11))
            
            for i in range(11):
                for j in range(11):
                    if g[i,j]>0 and g[i,j]<4:
                        if i-5!=0 and j-5==0:
                            heur_lm[i,j]=w1*g[i,j]/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                        elif i-5==0 and j-5!=0:
                            heur_lm[i,j]=w1*g[i,j]/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                        elif i-5!=0 and j-5!=0:
                            heur_lm[i,j]=w1*g[i,j]/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                    
                    if g[i,j]>14:
                        if ((g[i,j]-10)*Config.MIN_RATIO)<(sizesajat*Config.MIN_RATIO):    #üldözés súlya
                            menekules=1
                            if i-5!=0 and j-5==0:
                                heur_lm[i,j]=w2*(g[i,j]-10)/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                            elif i-5==0 and j-5!=0:
                                heur_lm[i,j]=w2*(g[i,j]-10)/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                            elif i-5!=0 and j-5!=0:
                                heur_lm[i,j]=w2*(g[i,j]-10)/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                        else:
                            menekules=0
                 
                        
                        if ((g[i,j]-10)*Config.MIN_RATIO)>=(sizesajat*Config.MIN_RATIO):#menekülés súlya
                            if i-5!=0 and j-5==0:
                                heur_lm[-i,-j]=heur_lm[-i,-j]+w3*(g[i,j]-10)/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                            elif i-5==0 and j-5!=0:
                                heur_lm[-i,-j]=heur_lm[-i,-j]+w3*(g[i,j]-10)/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                            elif i-5!=0 and j-5!=0:
                                heur_lm[-i,-j]=heur_lm[-i,-j]+w3*(g[i,j]-10)/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))







                        
                            
                    if g[i,j]==9:
                        heur_lm[i,j]=0
                    if g[i,j]==-2:
                        heur_lm[i,j]=0
                    if g[i,j]==0:
                        if i-5!=0 and j-5==0:                                  #üres mezők súlya
                            heur_lm[i,j]=w4/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                        elif i-5==0 and j-5!=0:
                            heur_lm[i,j]=w4 /math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                        elif i-5!=0 and j-5!=0:
                            heur_lm[i,j]=w4/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                            
            for i in range(11):           
                for j in range(11):       
                    sum_heur=sum_heur+heur_lm[i,j]       
                    if (g[i,j]-10)>0 and maxx<(g[i,j]-10):
                        maxx=(g[i,j]-10)
                        maxx=maxx-sizesajat
                         
                         
            k=0 #célmező
            l=0
            m=0    
            
            
            for i in range(11):
               for j in range(11):
                    if heur_lm[i,j]>m:
                        m=heur_lm[i,j]
                        k=i
                        l=j
            
            if m<w5:            #középpont priorizálásának súlya
                k=25-pos[1]
                l=25-pos[0]
            x=l-5
            y=5-k
            arctans=0
            if x<0 and y>=0:
                arctans=180
            if x<0 and y<0:
                arctans=180
            if x>=0 and y<0:
                arctans=360
            if x==0:
                x=0.00000000000000001
            arctan=math.atan(y/x)/math.pi*180
            arctan=arctan+arctans
          
            if 22.5<=arctan<=67.5:
                actstring=('+-')
            elif 67.5<arctan<112.5:
                actstring=('0-')
            elif 112.5<=arctan<=157.5:
                actstring=('--')
            elif 157.5<arctan<202.5:
                actstring=('-0')
            elif 202.5<=arctan<=247.5:
                actstring=('-+')
            elif 247.5<arctan<292.5:
                actstring=('0+')
            elif 292.5<=arctan<=337.5:
                actstring=('++')
            else:
                actstring=('+0')
          
          
          
          
          
          
          
          
          
          
          
          
            posx=5
            posy=5
            
            #láthatóság
            it=0  #iteráció
            x=k-posx
            y=posy-l
            voc=1   #vision of cél   ha 1 akkor látszik ha 0 akkor nem   
            while (((x!=0)or(y!=0))and it<5):
                x=k-posx
                y=posy-l
                
                oldposx=posx
                oldposy=posy
                
                arctans=0
                if x<0 and y>=0:
                    arctans=180
                if x<0 and y<0:
                    arctans=180
                if x>=0 and y<0:
                    arctans=360
                if x==0:
                    x=0.00000000000000001
                arctan=math.atan(y/x)/math.pi*180
                arctan=arctan+arctans
                
                posx=oldposx
                posy=oldposy
                x=k-posx
                y=posy-l
                
                if 22.5<=arctan<=67.5:
                    
                    posx=posx+1
                    posy=posy-1
                elif 67.5<arctan<112.5:
         
                    posy=posy-1
                elif 112.5<=arctan<=157.5:

                    posx=posx-1
                    posy=posy-1
                elif 157.5<arctan<202.5:

                    posx=posx-1
                elif 202.5<=arctan<=247.5:

                    posx=posx-1
                    posy=posy+1
                elif 247.5<arctan<292.5:

                    posy=posy+1
                elif 292.5<=arctan<=337.5:

                    posy=posy+1
                    posx=posx+1
                else:

                    posx=posx+1
                    
                if (g[posx,posy]==9):
                    
                    posx=oldposx
                    posy=oldposy
                it=it+1
                x=k-posx
                y=posy-l
           
            if it==5:
                voc=0  # voc= vision of cél, ha 0 akkor falölelés
             
            if menekules:    # menekülés ha 1 akkor menekülési helyzet van
                veszely=maxx*w9   # ilyenkor a veszély mértéke a méretek különbsége*valamilyen tanítható súllyal
            
            
          
            if voc==0 or (sum_heur/2)<keveskaja_hatar or veszely>veszely_hatar:  # falölelés "engedélyezése" ha veszélyes a helyzet vagy ha kevés a kaja
                
                
                for i in range(11):
                    for j in range(11):
                        g_fal[i,j]
                        if g[i,j]==9:
                            for ii in range(3):
                                for jj in range(3):
                                    h=-1
                                    n=-1
                                    if i+ii+h==0 and j+jj+n==0:
                                        g_fal[i+ii+h, j+jj+n]=0
                                    elif 0<=i+ii+h<11 and 0<=j+jj+n<11:
                                        g_fal[i+ii+h, j+jj+n]=1
                        if g[i,j]==9:
                            g_fal[i,j]==0
                       
                       
                for i in range(11):     #nem tudom ezt miért kell megint belerakni, de így jó
                    for j in range(11):
                        if g[i,j]==9:
                            g_fal[i,j]=0
                            g_fal[5,5]=0
                            
                            
                if   0<=5+olpos[1]-pos[1]<11     and  0<=5+olpos[0]-pos[0]<11 :
                    g_fal[5+olpos[1]-pos[1], 5+olpos[0]-pos[0]]=0   # ez már benne volt csak az ifes feltételek ami plussz     

                
              
                fal_heur_lm=np.ones([11,11])*-2#fal heurisztikus látómező
                fal_heur_lm=np.reshape(fal_heur_lm,(11,11))
                
                
                
                
                for i in range(11):
                    for j in range(11):
                        if g_fal[i,j]==1:
                            if math.sqrt((i-5)*(i-5)+(j-5)*(j-5))!=0:
                                fal_heur_lm[i,j]=10*g_fal[i,j]/ math.sqrt((i-5)*(i-5)+(j-5)*(j-5))
                
               
                kk=0 #célmező
                ll=0
                mm=0    
                
                for i in range(11):
                   for j in range(11):
                        if fal_heur_lm[i,j]>mm:
                            mm=fal_heur_lm[i,j]
                            kk=i
                            ll=j

                
                

                x=ll-5
                y=5-kk
                arctans=0
                if x<0 and y>=0:
                    arctans=180
                if x<0 and y<0:
                    arctans=180
                if x>=0 and y<0:
                    arctans=360
                if x==0:
                    x=0.00000000000000001
                arctan=math.atan(y/x)/math.pi*180
                arctan=arctan+arctans
                
               
                
                if 22.5<=arctan<=67.5:
                    actstring=('+-')
                elif 67.5<arctan<112.5:
                    actstring=('0-')
                elif 112.5<=arctan<=157.5:
                    actstring=('--')
                elif 157.5<arctan<202.5:
                    actstring=('-0')
                elif 202.5<=arctan<=247.5:
                    actstring=('-+')
                elif 247.5<arctan<292.5:
                    actstring=('0+')
                elif 292.5<=arctan<=337.5:
                    actstring=('++')
                else:
                    actstring=('+0')
            
        
            
            
            
            
            
            olpos[0]=pos[0]
            olpos[1]=pos[1]
            
            
                                    
                                    
                    
                
        

            # Akció JSON előállítása és elküldése
            sendData(json.dumps({"command": "SetAction", "name": "Pink", "payload": actstring}))



if __name__=="__main__":
    # Példányosított stratégia objektum
    hunter = RemoteNaiveHunterStrategy()

    # Socket kliens, melynek a szerver címét kell megadni (IP, port), illetve a callback függvényt, melynek szignatúrája a fenti
    # callback(fulljson, sendData)
    client = SocketClient("localhost", 42069, hunter.processObservation)

    # Kliens indítása
    client.start()
    # Kis szünet, hogy a kapcsolat felépülhessen, a start nem blockol, a kliens külső szálon fut
    time.sleep(0.1)
    # Regisztráció a megfelelő névvel
    client.sendData(json.dumps({"command": "SetName", "name": "Pink", "payload": None}))

    # Nincs blokkoló hívás, a főszál várakozó állapotba kerül, itt végrehajthatók egyéb műveletek a kliens automata működésétől függetlenül.