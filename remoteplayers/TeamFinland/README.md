# AdaptIO by Team Finland

Készítette: Géczi Zsombor és Palik Mátyás Balázs 
2022

Az Egyed4.py és Tanítás.py fájlok vannak kommentelve.
A tanításhoz létrehoztunk 4 darab "Example_client-et", Egyed1, Egyed2, Egyed3 és Egyed4 néven. Ezek ugyan azt az általunk megírt programot tartalmazzák. A tanítás során ezeket versenyeztettük egymás ellen. Minden Egyedhez tartozik kettő fájl, melyek a súlyokat és a fitnesz értékeket tartalmazzák. Ezek a „elog” nevű mappában vannak. A súlyokat minden kör elején beolvassa az Egyed és a kör végén visszaírja a fájlba. A fitneszértékeket pedig minden kör végén írja ki a fájlba. Összesen 9 súly és 6 fitneszértékkel dolgoztunk.
Kitüntettet szerepet kapott a 4-es Egyed. A kör végén amikor a játéktól megkapja a „leaderboardot”, meghívja a Tanitas.py nevű programunkat, ami elvégzi a súlyok módosítását és a játék csak ez után indul újra az Egyed4 által küldött „reset” üzenettel.
A tanítást az adatok beolvasásával és rendezésével kezdtük. Ezután súlyoztuk a fitneszértékeket az alapján, hogy milyen fontosnak ítéltük meg őket. A fitneszértékek sorba rendezve a súlyaikkal:
1.	Él-e vagy halott: *100
2.	Saját méret: *10
3.	Toporgás: *-10
4.	Újra definiálás: *10
5.	Mennyi kaját vett fel: *10
6.	Mennyi ellenséget evett meg: *30

Ezek pontosabban az Egyed4.py-ben vannak leírva.
A fitx.py nevű fájlba 0. helyre egy egyedet azonosító sorszám van beírva. (1,2,3,4)
A tanításhoz használt 9 súlyt átírtuk 8 bitre. Emiatt az értékük 0 és 255 között változhat. Ezek a súlyok a következőek: (kiegészítve a hozzájuk rendelt fitneszértékekkel)

1.	Étel: 2-es és 5-ös fitneszérték
2.	Ellenség üldözése: 6-os fitneszérték
3.	Ellenség elől történő menekülés: 1-es fitneszérték
4.	Üres mezők: Szummázott 
5.	A pálya középpontja: Szummázott
6.	Falkövetés: Szummázott
7.	Kevés kaja a látómezőn belül: Szummázott
8.	Veszélyhatár: Szummázott
9.	Menekülés súlya: Szummázott

Ezek pontosabban az Egyed4.py-ben vannak leírva.


A súlyokat átírtuk 9db 4x1-es mátrixba, melyek tartalmazták ugyan ahhoz a súlyhoz tartozó 4 egyed súlyait sorba rendezve. Ezek után 4 pontos keresztezést hajtottunk végre rajtuk úgy, hogy az első egyedet a 2.-al és a 3.-al kereszteztük. A 4.-et pedig a másodikkal. Az első egyedet változatlanul írtuk vissza. Majd egy 5%-os mutációt hajtottunk végre rajtuk. A súlyokat ezután visszaalakítottuk decimális formába és visszaírtuk őket a fileokba. 
A tanítás során a megfelelő generációból a megfelelő egyed kiválasztása egyelőre manuálisan történik. A játékmeneteket figyelve kiválasztunk egy megfelelőnek ítélt egyedet és kimentjük a súlyait. A betanított játékosunk futtatása úgy történik, hogy a tanítás során használt egyik egyedet regisztráljuk be a játékba és az a kimentet súlyokot olvassa be a kör elején. 
A programunkat továbbfejleszteni több pályán történő betanítással, a súlyok nagyobb felbontásával és az Egyedek pontosabb kiértékelésével lehetne. Emellett pedig szükséges lenne meghatározni egy kilépési feltételt amikor az egyedünk megfelelően betanult, így elkerülve azt, hogy egy pályára túlságosan rátanuljon. 