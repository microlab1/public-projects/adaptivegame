# Készítők:

Geréb Ábris <br>
Szima Zsolt <br>

# Szükséges csomagok:

Pytorch <br>
Pygame

# Algoritmus alapja:

## Reinforcement Learning Linear Q learning

A Q-learning egy modell nélküli megerősítő tanulási algoritmus, amivel egy adott állapotban egy cselekvés értékét lehet megtanulni. 
Nem igényel környezeti modellt (azaz „modellmentes”), és adaptáció nélkül képes kezelni a sztochasztikus átmenetekkel és jutalmakkal kapcsolatos problémákat.
Bármely véges Markov-döntési folyamathoz (FMDP) a Q-learning megtalálja az optimális politikát abban az értelemben,
hogy minden egymást követő lépésben maximalizálja a teljes jutalom várható értékét, az aktuális állapottól kezdve.
A Q-learning képes meghatározni egy optimális akciókiválasztási politikát 
bármely adott FMDP-hez a végtelen feltárási idő és egy részben véletlenszerű szabály alapján. 
A "Q" az algoritmus által kiszámított függvényre utal (Quality) – egy adott állapotban végrehajtott művelet várható jutalmára.
Viszont fontos megjegyezni, hogy csak megfeleőjutalmazási rendszer esetén tud tanulni a háló.

## Modell működése:

Először is ki kellett alakítanunk egy jutalomrendszert, mely alapján az Agent-ünk tanulni tud. A mi karakterünk az egész látómezőt kihasználja, mivel abban bíztunk,
hogy megfelelő tanítási idő után kellően jó stratégiát fog kialakítani. Kimenetnek 9 értéket adtunk meg tehát a mi karakterünk egy helyben is maradhatott, viszont ezt böntettük.
A jutalom rendszert úgy építettük ki hogy kaja esetén plusz pontokat kaj a kaja értékétől függően, az egyszerü mozgásért nem kapott semmilyen pontot, az egy helyben maradást viszont büntettük.
Fontos hogy a modellnek két állapota van, mikor explotációt és mikor explorációt hajt végre.
Az exploráció azon kezdeti szakasza, amikor még nincsenek a súlyok betanítva a hálón így többnyire rendom mozgásokat véges az Agent. 
Ez tanítás során változik ugyanis egyre jobb súlyokat állít be magának az agent és ekkor veszi át szépen lassan explorációt az explotáció. 
Ekkor már a betanított súlyok alapján mozog és az alapján tájákozódik.
Fontos az exploráció rész mivel hajlamos megrekedni az elején az Agent és olyankor nem mozdul, mert nem kap random mozgásra "felfedezésre" utasítást.
A modell tanítására Pytorch-ot használtunk így a súlyokat is ilyen típusú fileként menti majd ki 'model.pth'.
Ezt a két állását az agentünknek a "traine" parancsal lehet elindítani. 

## Agent rekordja:

A tanítások során sajnos nem a legoptimálisabb jutalomrendszert választottuk az Agentünknek, így nem tudott rátanulni exponenciálisan semmilyen stratégiára.
Az egyetlen stratégiája pont az egy helyben maradás büntetése miatt az volt, hogy nagyon fürge volt végig a pályán.
Ezekben a mozdulatokban így is egy 50% - 60%-os random mozgás volt, viszont sok esetben céltudatosan a célok sőt még a elenségehez vette az irányt.
A maximális rekordja a modellüknek 53 pont 200 tick alatt.

## További fejlesztési javaslatok:

A jutalomrendszer helyes beállításával, esetleg más algoritmusok beágyazásával esetleg sikeresebb lehet a modellünk a jövőben.

